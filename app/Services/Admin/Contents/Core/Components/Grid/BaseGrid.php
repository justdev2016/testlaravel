<?php

namespace App\Services\Admin\Contents\Core\Components\Grid;

/**
 * Базовый класс для компонентов типа `Grid`
 */
class BaseGrid
{
    /** @var string (required) */
    const TYPE = 'grid';

    /** @var string  */
    const BASE_VIEW = 'admin.components';

    /** @var array (required) */
    const VIEW_COMPONENTS = [
        'actions',
        'body',
        'create-button',
        'each_column',
        'footer',
        'header',
    ];

    public function __construct()
    {
        //
    }

    /**
     * @param  string $suffix
     * @param  string $param
     * @return string
     */
    protected function getViewComponents(string $suffix, string $param) : string
    {
        return implode('.', [
            self::BASE_VIEW, // admin.components.
            'grid', // grid
            $suffix, // dataTable.includes
            $this->ViewComponentsFormatter($param) // header
        ]);
    }

    /**
     * Форматирует имена компонентов с наличием "_"
     *
     * @param  string $param
     * @return string
     */
    private function ViewComponentsFormatter(string $param) : string
    {
        if (str_contains($param, '_')) {
            return str_replace('_', '.', $param);
        }

        return $param;
    }
}