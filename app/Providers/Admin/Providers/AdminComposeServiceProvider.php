<?php

namespace App\Providers\Admin\Providers;

use Illuminate\Support\ServiceProvider;

class AdminComposeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootViewComposer();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Регистрация View Composer
     * (глобальные данные для views)
     *
     * @return void
     */
    protected function bootViewComposer()
    {
        $this->bootViewComposerFrontEnd();
        $this->bootViewComposerGlobal();
        $this->bootViewComposerDocument();
    }

    /**
     * Регистрация View Composer `Front-End`:
     * `cssPath`,`jsPath`
     * (глобальные данные для views)
     *
     * @return void
     */
    private function bootViewComposerFrontEnd()
    {
        view()->composer('*', function ($view) {
             $view->with('cssPath', config('admin.frontEnd.css'));
             $view->with('jsPath', config('admin.frontEnd.js'));
        });
    }

    /**
     * Регистрация View Composer:
     *  `locale`,`router`,`auth`
     * (глобальные данные для views)
     *
     * @return void
     */
    private function bootViewComposerGlobal()
    {
        view()->composer('*', function ($view) {
             $view->with('locale', config('app.locale', 'en'));
             $view->with('router', app('router'));
             $view->with('auth', auth());
        });
    }

    /**
     * Регистрация View Composer `Document`
     * (глобальные данные для views)
     *
     * @return void
     */
    private function bootViewComposerDocument()
    {
        view()->composer('*', function ($view) {
             $view->with('document', app('document'));
        });
    }
}
