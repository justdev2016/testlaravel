<?php

namespace App\Services\Admin\Support\Web;

use Page;
use App\Services\Admin\Contents\Core\Action;

abstract class Decorator
{
    /** @var \App\Services\Admin\Contents\Core\Action */
    protected $action;

    public function __construct(Action $action)
    {
        $this->action = $action;

        $this->make();
    }

// <CONTENT>|<ACTION>

    /**
     * Прием и обработка параметров:
     * [
     *     `content` - тип к контента (данных), который необходимо отобразить на странице
     *     `action`  - тип действия, который необходимо выполнить с данными
     * ]
     * @param  string $name
     * @return self|object
     */
    public function __get($name)
    {
        // обработка `content`
        if (Page::has($name)) {
            $this->action->setContentType($name);

            return $this;
        }

        // обработка `action`
        $this->action = $this->action->make($name);
        return $this;
    }

// RESPONSE

    /**
     * Вернкть данные пользователю
     *
     * @return mixed
     */
    public function response()
    {
        return $this->action->response();
    }
}