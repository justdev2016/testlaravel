<?php

namespace App\Services\Admin\Contents\Core\Settings;

/**
 * Глобальные настройки источников информационных страниц
 */
class InfoSettings extends BaseSettings
{
    /** @var string */
    const DEFAULT_PREFIX = 'admin.layouts.includes.messages.';

    /** @var array */
    const INFO = [
        'error',
        'info',
        'success',
        'warning',
    ];

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct()
    {
        parent::__construct();

        $this->make();
    }

    public function make()
    {
        $this->settings->put('prefix', self::DEFAULT_PREFIX);

        $this->makeView();
    }

    /**
     * Формирует пути к информационным файлам
     *
     * @return void
     */
    private function makeView()
    {
        foreach (self::INFO as $item) {
            $items[$item] = "{$this->settings->get('prefix')}{$item}";
        }

        $this->settings = collect($items);
    }
}