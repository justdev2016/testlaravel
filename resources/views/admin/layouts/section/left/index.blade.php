<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        {{-- @if (! $auth->guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/admin-lte/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ $auth->user()->name }}</p>
                </div>
            </div>
        @endif --}}
        <!-- search form (Optional) -->
        {{-- @include('admin.layouts.includes.searchform') --}}
        {{-- [MENU] --}}
        @each('admin.layouts.section.left.item', $leftData, 'item')
    </section><!-- /.sidebar -->
</aside>