<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\AdminController;

class HomeController extends AdminController
{
	public function index()
	{
		return view('admin.home');
	}
}
