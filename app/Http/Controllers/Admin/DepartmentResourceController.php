<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Services\Admin\Contents\Core\Web;

class DepartmentResourceController extends Controller
{
    /** @var \App\Services\Admin\Contents\Core\Web */
    protected $web;

    public function __construct(Web $web)
    {
        $this->web = $web;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() : View
    {
        return $this->web->departments->index->response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() : View
    {
        return $this->web->departments->create->response();
    }

    /**
     * Display the specified resource.
     *
     * @param Department $department
     * @return Illuminate\View\View
     */
    public function show(Department $department) : View
    {
        return $this->web->departments->show->response();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Department  $department
     * @return \Illuminate\View\View
     */
    public function edit(Department $department) : View
    {
        return $this->web->departments->edit->response();
    }
}
