@extends('admin.layouts.app')

@section('contentheader_title')
    {{ $page->title('employees') }}
@endsection

@section('htmlheader_title')
    {{ $page->title('employees') }}
@endsection

@section('main-content')
	<div class="box-header with-border">
		<h3 class="box-title">{{ $page->title('employees') }}</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
        @foreach ($page->components as $component)
    		@each($page->view(), $component, 'components')
        @endforeach
	</div><!-- /.box-body -->
@endsection

@push('scripts')
    <script src="{{ asset("{$jsPath}/index-{$page->script()}") }}"></script>
@endpush