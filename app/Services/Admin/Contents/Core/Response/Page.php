<?php

namespace App\Services\Admin\Contents\Core\Response;

use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings;

/**
 * Класс для работы с данными в представлении
 */
class Page
{
    /** @var \Illuminate\Support\Collection */
    protected $data;

    /** @var object */
    protected $settings;

    /** @var array */
    const METHODS = ['components'];

    /**
     * @param \Illuminate\Support\Collection $data
     * @param object $settings
     */
    public function __construct(Collection $data, $settings)
    {
        $this->data     = $data;
        $this->settings = $settings;
    }

    public function __get($name)
    {
        if (! in_array($name, self::METHODS)) {
            throw new \Exception(trans('message.method-not-exist'), 2);
        }

        return ['components' => $this->data];
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->data, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->data, $method], $arguments);
    }

    /**
     * @param  string $name
     * @return string
     */
    public function title(string $name) : string
    {
        $prefix = $this->settings->content('locale.main');

        return trans("{$prefix}.{$name}");
    }

    /**
     * @return string
     */
    public function script() : string
    {
        return "{$this->settings->get('type')}.js";
    }

    /**
     * @return string
     */
    public function type() : string
    {
        return $this->settings->get('type');
    }

    /**
     * @param  string $name
     * @return string
     */
    public function view(string $name = null) : string
    {
        return ! $name
            ? "{$this->settings->content('views.prefix')}includes.{$this->settings->page()}"
            : $this->settings->content($name);
    }
}