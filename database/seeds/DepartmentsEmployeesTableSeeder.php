<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DepartmentsEmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $range =range(1,5);
        $array = range(1,10);
        $employees= range(1,300);

        foreach ($employees as $id) {
            $random = $array[array_rand($range)];

            for ($i = 1; $i < $random; $i++) {
                $data[] = [
                    'employee_id'  => $id,
                    'department_id' => $array[array_rand($array)],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ];
            }
        }

        \DB::table('departments_employees')->insert($data);
    }
}
