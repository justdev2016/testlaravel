<?php

namespace App\Services\Admin\Contents\Items\Employees\Filters;

use App\Contracts\Admin\FilterContract;
use Illuminate\Database\Eloquent\Builder;

class Department implements FilterContract
{
    /**
     * Apply a given search value to the builder instance.
     *
     * @param  Builder $builder
     * @param  mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        if (! $value) {
            return $builder;
        }

        return $builder->whereHas('departments', function ($q) use ($value) {
            $q->where('departments.id', $value);
        });
    }
}