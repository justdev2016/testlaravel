<?php

use App\Department;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * @property mixed $app
 */
class AdminApiDepartmentsListTest extends TestCase
{
    /**
     * Имитация авторизованного пользователя
     */
    public function login()
    {
        $user = \App\User::find(1);
        $this->be($user);
    }

    public function testDepartmenrntListAfterLoginSuccess()
    {
        $this->login();

        $data = Department::pluck('name', 'id');

        $response = $this->json('GET', 'admin/api/departments/list');

        $response->seeJson($data->toArray());
    }

    public function testDepartmenrntListApiAfterLoginSuccess()
    {
        $this->login();

        $this->mock = \Mockery::mock('Illuminate\Database\Eloquent\Model', 'App\Department');

        $this->mock
            ->shouldReceive('pluck')
            ->with('name','id')
            ->once()
            ->andReturnUsing(function () {
                return new Illuminate\Database\Eloquent\Collection(['zero', 'one', 'two']);
            });

        $this->call('GET', 'admin/api/departments/list');

        $this->assertArraySubset(['zero', 'one', 'two'], $this->mock->pluck('name','id')->toArray());
    }
}
