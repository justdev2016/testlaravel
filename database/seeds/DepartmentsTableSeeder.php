<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names= [
            'административный',
            'бухгалтерия',
            'маркетинг',
            'менеджеры',
            'охрана',
            'программисты',
            'производственный',
            'рекламный',
            'склад',
            'юридически',
        ];

        foreach ($names as $name) {
            App\Department::create(['name' => $name]);
        }
    }
}
