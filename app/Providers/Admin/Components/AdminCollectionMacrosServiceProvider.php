<?php

namespace App\Providers\Admin\Components;

use Illuminate\Support\ServiceProvider;

class AdminCollectionMacrosServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCollectionMacros();
    }

    /**
     * @return void
     */
    private function registerCollectionMacros()
    {
        collect()->macro('content', function($name, $default = null) {
            $param  = explode('.', $name);
            $result = $this->items;

            foreach ($param as $value) {
                if (! $result && is_array($result)) {
                    $result = collect($result);

                    break;
                }

                if (! $result) {
                    $result = $default;

                    break;
                }

                if (is_array($result)) {
                    $result = collect($result);
                }

                // получить содержимое как `Collection`
                if ($value === '*') {
                    if (is_string($result)) {
                        $result = collect([$result]);
                    }

                    break;
                }

                $result = $result->get($value);
            }

            return $result ?? $default;
        });

        collect()->macro('getData', function($name, $default = null) {
            $oldName = $name;

            if ($this->items instanceof \Illuminate\Support\Collection) {
                $this->items = $this->items->toArray();
            }

            if (ends_with($name, '*')) {
                $name = str_replace('.*', '', $name);
            }

            if (is_array($this->items)) {
                $result = array_get($this->items, $name, $default);
            }

            if (ends_with($oldName, '.*')) {
                if (is_string($result) && $result !== $default) {
                    $result = collect([$result]);
                }
            }

            if (is_string($result)) {
                return $result;
            }

            if (is_array($result)) {
                return collect($result);
            }

            return $result;
        });

        collect()->macro('present', function ($class) {
            return $this->map(function ($object) use ($class) {
                return present($object, $class);
            });
        });
    }
}
