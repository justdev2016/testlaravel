<div id="app">
  @if ($page->type() === 'employees')
    <div class="box">
        {!! Form::open(['route' => ['admin.api.employees.filter'], '@submit.prevent' => 'filter']) !!}
        <div class="box-body">
          <div class="form-group">
            {!! Form::label('begin', trans('message.begin')) !!}
            <div class="input-group col-md-2" id="datetimepicker1">
              {!! Form::input('text', 'begin', null, ['class' => "form-control", 'ref' => "begin"]) !!}
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('end', trans('message.end')) !!}
            <div class="input-group col-md-2" id="datetimepicker2">
              {!! Form::input('text', 'end', null, ['class' => "form-control", 'ref' => "end"]) !!}
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('fullname', trans('message.fullname')) !!}
            <div class="input-group col-md-3">
              {!! Form::input('text', 'fullname', null, ['v-model.trim'=>'form.fullname','class' => "form-control"]) !!}
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('phone', trans('message.phone')) !!}
            <div class="input-group col-md-2">
              {!! Form::input('text', 'phone', null, ['v-model'=>'form.phone','class' => "form-control"]) !!}
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('email', trans('message.email')) !!}
            <div class="input-group col-md-2">
              {!! Form::input('text', 'email', null, ['v-model.trim'=>'form.email','class' => "form-control"]) !!}
            </div>
          </div>

          <div class="form-group">
            {!! Form::label('department', trans('message.department')) !!}
            <div class="input-group col-md-2">
              <select name="department" v-model.number="form.department" class="form-control">
                <option v-for="(value, key) in departments" :value="key">@{{ value }}</option>
              </select>
            </div>
          </div>
        </div>

        <div class="box-footer">
        {!! Form::submit(trans('message.find'), ['class' => 'btn btn-info']) !!}
        {!! Form::button(trans('message.reset'), ['class' => 'btn btn-info', 'type' =>'reset', '@click' => 'reset']) !!}

        </div>
        {!! Form::close() !!}
    </div>
  @endif

  <div class="box">
  <div class="box-body">
      <div class="dataTables_wrapper form-inline dt-bootstrap">
        <div class="row">
            <table id="employees" class="table table-bordered table-striped table-hover dataTable">
              @include($component->view('header'))

              @include($component->view('body'))

              @include($component->view('footer'))
            </table>
          </div>
        </div>

        @include($page->view("views.delete-{$page->type()}-modal"))
        <app-pagination
          :source='pagination'
          @navigate="paginate"
          :show="raw.total > raw.per_page"
        ></app-pagination>
    </div>
  </div>
  <div class="box-footer">
    @if ($component->actions()->has('create.get'))
      @include($component->view('create-button'), [
        'action' => $component->actions('create.get'),
        'type'   => $component->settings('actions.type'),
      ])
    @endif
  </div>
</div>