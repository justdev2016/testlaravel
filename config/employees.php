<?php

return [
    /**
     * Отображать функционал в приложении
     */
    'active' => true,

    /**
     * Данные для `Menu`.`Employees`
     */
    'menu' => [
        'name' => 'employees',
        'text' => 'employees.employees',
        'url'  => 'admin.employees.index'
    ],

    'settings' => [
        'action' => App\Services\Admin\Contents\Items\Employees\Support\Entry::class,
    ],

    // Rest actions instance
    'actions' => [
        'web' => [
            'index'  => App\Services\Admin\Contents\Items\Employees\Web\Index\Index::class,
            'create' => App\Services\Admin\Contents\Items\Employees\Web\Create\Create::class,
            'edit'   => App\Services\Admin\Contents\Items\Employees\Web\Edit\Edit::class,
            'show'   => App\Services\Admin\Contents\Items\Employees\Web\Show\Show::class,
        ],
        'api' => [
            'index'   => App\Services\Admin\Contents\Items\Employees\Api\Index\Index::class,
            'destroy' => App\Services\Admin\Contents\Items\Employees\Api\Destroy\Destroy::class,
            'store'   => App\Services\Admin\Contents\Items\Employees\Api\Store\Store::class,
            'update'  => App\Services\Admin\Contents\Items\Employees\Api\Update\Update::class,
            'reorder' => App\Services\Admin\Contents\Items\Employees\Api\Custom\UpdateReOrder::class,
        ],
    ],

    /**
     * Page component[driver] + componentConfig[] (optional)
     * [
     *     '<type>' => [
     *         'driver' => '<driver>',
     *         'config' => [
     *             'data' => false || 'load@all' || 'load@limit',
     *             // optional - config.data != false && config.data == 'load@all'
     *             'sync' => 'sync@all' || 'sync@limit' || 'sync#all' || 'sync#limit'
     *         ]
     *     ]
     * ], где
     *     config.data - передавать ли данные в представление, если да, то все сразу или порциями;
     *     config.sync - последующая синхронизация данных или в случай, если admin.spa = true, то в каком объеме получить данные;
     */
    'components' => [
        'index' => [
            'grid' => [
                'driver' => 'dataTable',
                'config' => [
                    'data' => false,
                    'sync' => 'sync@limit'
                ]
            ],
        ],
        'show'  => [
            'info' => [
                'driver' => 'adminLTE',
                'config' => [
                    'data' => 'load@all',
                ]
            ],
        ],
        'create' => [
            'form' => [
                'driver' => 'adminLTE',
                'config' => [
                    'data' => 'load@all',
                ]
            ],
        ],
        'edit' => [
            'form' => [
                'driver' => 'adminLTE',
                'config' => [
                    'data' => 'load@all',
                ]
            ],
        ]
    ],
];