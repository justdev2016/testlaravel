<?php

return [
    /**
     * Base `Menu` data
     */
    'data' => [
        'home' => [
            'name' => 'home',
            'text' => 'menu.dashboard',
            'url'  => 'admin.pages.index'
        ],
        'childrens' => [
            [
                'content' => [
                    'text' => 'menu.content'
                ],
            ]
        ]
    ],

    /**
     * `Menu` settings
     */
    'settings' => [
        'menu' => [
            'web-component' => [
                // 'raw' =>,
                // 'vue' =>,
            ],
            'render' => [
                // 'web' =>
                // 'ajax' =>
            ],

        ],
    ],
];