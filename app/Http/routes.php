<?php

Route::patterns([
    'employees'    => '[0-9]+',
    'departments' => '[0-9]+',
]);

Route::get('/', function () {
    return view('welcome');
});

// Admin web
Route::group(['prefix'=>'admin', 'namespace'=>'Admin', 'middleware' => ['auth']], function() {
    Route::get('/', ['as' =>'admin.pages.index', 'uses' => 'HomeController@index']);

    // employees resource
    Route::resource('employees', 'EmployeeResourceController', ['except' => ['destroy', 'store', 'update']]);

    // departments resource
    Route::resource('departments', 'DepartmentResourceController', ['except' => ['destroy', 'store', 'update']]);
});

// Admin api
Route::group(['prefix'=>'admin/api', 'namespace'=>'Admin', 'middleware' => ['auth']], function() {
    // employees resource - api
    Route::get('employees/filter', ['as' =>'admin.api.employees.filter', 'uses' => 'EmployeeApiResourceController@filter']);
    Route::resource('employees', 'EmployeeApiResourceController', ['only' => ['index', 'destroy', 'store', 'update']]);

    // departments resource - api
    Route::get('departments/list', ['as' =>'admin.api.departments.filter', 'uses' => 'DepartmentApiResourceController@list']);
    Route::resource('departments', 'DepartmentApiResourceController', ['only' => ['index', 'destroy', 'store', 'update']]);
});

Route::auth();