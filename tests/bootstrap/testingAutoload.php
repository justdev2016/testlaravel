<?php
/*
|--------------------------------------------------------------------------
| Run migration and seeds
|--------------------------------------------------------------------------
|
*/
passthru("php artisan migrate:refresh --seed");
require __DIR__ . '/../../bootstrap/autoload.php';