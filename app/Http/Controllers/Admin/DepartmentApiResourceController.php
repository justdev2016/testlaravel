<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\Http\Controllers\Controller;
use App\Services\Admin\Contents\Core\Api;
use App\Http\Requests\Department\DepartmentRequest;
use Illuminate\Http\JsonResponse;

class DepartmentApiResourceController extends Controller
{
    /** @var \App\Services\Admin\Contents\Core\Api */
    protected $api;

    public function __construct(Api $api)
    {
        $this->middleware('check-ajax', [
            'only' => [
                'index',
                'store',
                'update',
                'destroy',
                'list'
            ]
        ]);

        $this->api = $api;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        return $this->api->departments->index->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Department\DepartmentRequest $request
     * @return  \Illuminate\Http\JsonResponse
     */
    public function store(DepartmentRequest $request) : JsonResponse
    {
        return $this->api->departments->store->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Department\DepartmentRequest  $request
     * @param  Department $department
     * @return  \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        return $this->api->departments->update->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Department $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Department $department) : JsonResponse
    {
        return $this->api->departments->destroy->response();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function list() : JsonResponse
    {
        $data = (new Department)->pluck('name', 'id');

        return response()->json($data);
    }
}
