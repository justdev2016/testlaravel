<div class="callout callout-warning @if(isset($static) && $static) static-warning @else warning @endif">
    <h4>Warning!</h4>
    @if(isset($warning))
        @if(is_array($warning))
        <ul>
        @foreach($warning as $warn)
            <li>{!! $warn !!}</li>
        @endforeach
        </ul>
        @else
            <p>{{ $warning }}</p>
        @endif
    @elseif(session()->has('warning'))
        <p>{{ session('warning') }}</p>
    @endif
</div>