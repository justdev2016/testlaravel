<?php

namespace App\Services\Admin\Managers\Files;

use App\Contracts\Managers\Files\{Commander,Options};

/**
 * Files/Folders manager
 */
class AdminFCommander implements Commander
{

    /** @var string */
    const METHOD = 'action';
    
    /** @var \ActionOptions */
    protected $action;

    /**
     * @param \App\Contracts\Managers\Files\Options $options
     */
    public function make(Options $options)
    {
        $this->action = $options;

        return $this;
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->action, self::METHOD)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->action, self::METHOD], $arguments);
    }
}