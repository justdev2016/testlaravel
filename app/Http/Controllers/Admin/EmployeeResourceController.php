<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use Illuminate\View\View;
use App\Http\Controllers\Controller;
use App\Services\Admin\Contents\Core\Web;

class EmployeeResourceController extends Controller
{
    /** @var \App\Services\Admin\Contents\Core\Web */
    protected $web;

    public function __construct(Web $web)
    {
        $this->web = $web;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() : View
    {
        return $this->web->employees->index->response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() : View
    {
        return $this->web->employees->create->response();
    }

    /**
     * Display the specified resource.
     *
     * @param Employee $employee
     * @return Illuminate\View\View
     */
    public function show(Employee $employee) : View
    {
        return $this->web->employees->show->response();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Employee  $employee
     * @return \Illuminate\View\View
     */
    public function edit(Employee $employee) : View
    {
        return $this->web->employees->edit->response();
    }
}
