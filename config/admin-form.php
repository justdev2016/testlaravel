<?php

/**
 * Component `Form`
 */

return [
    'data' => [
        'instance' => App\Services\Admin\Contents\Core\Components\Form\Form::class,
        'drivers'  => [
            'adminLTE' => [
                'active'   => true,
                'instance' => App\Services\Admin\Contents\Core\Components\Form\AdminLTE\FormManager::class,
            ],
        ]
    ],
];