<?php

namespace App\Services\Admin\Document;

use App\Services\Admin\Support\Component;

/**
 * DocumentComposite - составной объект
 * - Определяет поведение компонентов, у которых есть потомки
 * - Хранит компоненты-потомки
 * - Реализует относящиеся к управлению потомками операции и интерфейс
 */
class DocumentComposite extends Component
{
    /** @var array */
    private $children = [];

    public function add(Component $component)
    {
        $key = $component->item['name'] ?? $component->item;

        $this->children[$key] = $component->item['item'];
    }

    public function remove(Component $component)
    {
        $key = $component->item['name'];

        unset($this->children[$key]);
    }

    public function render()
    {
        foreach($this->children as $child) {
            $data[] = $child->render();
        }

        return $data ?? [];
    }
}