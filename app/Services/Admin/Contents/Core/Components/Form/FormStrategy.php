<?php

namespace App\Services\Admin\Contents\Core\Components\Form;

use App\Services\Admin\Contents\Core\Components\ComponentDependence;

interface FormStrategy
{
    public function make(ComponentDependence $data, ComponentDependence $settings);
}