<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Create\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `settings`
 */
class SettingsConfig implements ActionContract
{
    /** @var array */
    const SETTINGS = [
        'form' => [
            'open' => [
                'id' => 'create-department-form',
            ],
            // @todo проверить, может `name` уже не используется!!! + оптимизировать
            'fields' => [
                [
                    'text' => [
                        'name'     => 'name',
                        'label'    => 'departments.name',
                        'required' => true,
                    ],
                ],
            ],
            'buttons' => [
                [
                    'submit' => [
                        'type'  => 'success',
                        'label' => 'message.save',
                        'id'    => 'department-create-button'
                    ],
                ],[
                    'cancel' => [
                        'type'  => 'danger',
                        'label' => 'message.cancel',
                        'id'    => 'department-cancel'
                    ],
                ],
            ],
        ],
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->settings = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = collect(self::SETTINGS)->map(function($item) {
            return collect($item)->map(function($item) {
                return collect($item);
            });
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('settings', $this->settings);
    }
}