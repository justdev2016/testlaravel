function reset(...items) {
    const data     = items,
          all      = $('#user-locales-list, #user-languages-list, #user-themes-list'),
          locale   = $('#user-locales-list'),
          language = $('#user-languages-list'),
          theme    = $('#user-themes-list')

    if (data.length == 1 && data[0] === 'all') {
        resetAction(all)
    } else {
        $.each(data, (key, val) => {
            switch(val) {
                case 'locale':
                    resetAction(locale)
                    break
                case 'language':
                    resetAction(language)
                    break
                case 'theme':
                    resetAction(theme)
                    break
            }
        })
    }
}

function init(el, data) {
    let first = el.find('option').eq(0).text()

    el.find('option').remove()
    el.append($('<option>').text(first).attr('value', ''));

    $.each(data, (key, value) => {
        el.append($('<option>').text(value).attr('value', key))
    })
}

// private
function resetAction(el) {
    el.prop('selectedIndex', 0)
    el.attr('disabled', 'disabled')
}

export { reset, init }