<li {{ isset($item['url']) && $router->is($item['url']) ? 'class="active"' : '' }}>
    <a href="{{ $item['url'] }}">
        <i class="{{ ($item['url'] === route('admin.pages.index')) ? 'fa fa-home' : 'fa a-folder' }}"></i>
        <span>{{ $item['text'] }}</span>
    </a>
</li>