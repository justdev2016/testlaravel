<td>
    <div class="btn-group">
        @foreach($actions as $alias => $action)
            {!!
                link_to_route("admin.{$type}.{$action}", trans("actions.{$action}"), $routeParam, [
                    "class" => "btn btn-default {$action}-{$type}",
                    "role"  => "button"
                ])
            !!}
        @endforeach
    </div>
</td>