<?php

namespace App\Services\Admin\Contents\Core\Defaults;

use Illuminate\Support\Collection;

/**
 * Доступные компоненты системы
 * Основные задачи:
 * [
 *     хранить список,
 *     проверять входные данные на активность
 * ]
 * Основные данные о компонентах
 */
class Components extends Defaults
{
    /** @var array */
    const DRIVERS = [
        'vue','raw'
    ];

    /** @var \Illuminate\Support\Collection */
    protected $config;

    /** @var array */
    const METHODS = ['types','components'];

    /**
     * @param array $components
     */
    public function __construct(array $components)
    {
        parent::__construct();

        $this->config = collect($components);

        $this->make();
    }

    /**
     * @return void
     */
    protected function make()
    {
        $this->makeComponentTypes();
        $this->loadSettings();
        $components = $this->config->map(function($item, $type) {
            $item['drivers'] = $this->makeComponentDrivers($type);

            return $item;
        });

        $this->defaults->put('components', $components);
    }

    /**
     * @return void
     */
    private function makeComponentTypes()
    {
        $types = $this->config->keys();

        $this->defaults->put('types', $types);
    }

    /**
     * @return void
     */
    private function loadSettings()
    {
        $this->config = $this->config->map(function($item, $type) {
            return collect(config("{$item}.data"));
        });
    }

    /**
     * @param  string $type
     * @return \Illuminate\Support\Collection
     */
    private function makeComponentDrivers(string $type) : Collection
    {
        return $this->config->getData("{$type}.drivers")
            ->active()
            ->map(function($item) {
                return collect($item)->only(['instance']);
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function active() : Collection
    {
        return $this->defaults->get('types');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function drivers() : Collection
    {
        return $this->defaults->get('components');
    }

    /**
     * @param string  $type
     * @return string
     */
    public function instance(string $type) : string
    {
        return $this->defaults->getData("components.{$type}.instance");
    }

    /**
     * @param  string  $type
     * @param  string  $name
     * @return object
     * @todo валидировать данные перед инициализацией (name)
     */
    public function driver(string $type, string $name)
    {
        return $this->defaults->getData("components.{$type}.drivers.{$name}.instance.*")->instance()->first();
    }
}