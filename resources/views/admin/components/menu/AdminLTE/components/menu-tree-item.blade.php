<li {{ $router->is($item['url']) ? 'class="active"' : '' }}>
    <a href="{{ $item['url'] }}">
        <i class="fa fa-circle-o"></i>
        {{ $item['text'] }}
    </a>
</li>