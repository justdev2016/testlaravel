<?php

namespace App\Services\Admin\Contents\Core\Defaults;

class Defaults
{
    /** @var \Illuminate\Support\Collection */
    protected $defaults;

    public function __construct()
    {
        $this->defaults = collect();
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->defaults, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->defaults, $method], $arguments);
    }
}