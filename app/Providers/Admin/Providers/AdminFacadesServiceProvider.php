<?php

namespace App\Providers\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Contents\Core\Support\Spa;
use App\Services\Admin\Contents\Core\Support\DataLoader;

class AdminFacadesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSPAFacade();
        $this->registerDataLoaderFacade();
    }

    protected function registerSPAFacade()
    {
        $this->app->singleton('spa', function ($app) {
            $config  = $app['config']['admin.spa'];

            return new Spa($config);
        });
    }

    protected function registerDataLoaderFacade()
    {
        $this->app->singleton('dataLoader', function ($app) {
            return new DataLoader;
        });
    }
}
