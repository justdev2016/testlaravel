<?php

namespace App\Services\Admin\Managers\Info;

abstract class ProfileInfo
{
    const RULES = [
        'all', // @todo
        'web', // @todo
        'path' => ['path','real_path'],
        'file' => ['path','filename','basename','pathname'],
    ];
    
    /** @var \Illuminate\Support\Collection */
    protected $profiles;

    public function __construct()
    {
        $this->profiles = collect(self::RULES);
    }
}