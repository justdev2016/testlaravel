<?php

namespace App\Services\Admin\Contents\Core\Components\Form\AdminLTE;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\Form\BaseForm;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Валидация и подготовка данных о настройках для компонента
 *
 * @todo реализовать функционал применение настроек в зависимости от логики страницы
 * @todo использование множественных компонентов одного типа на одной странице
 * @todo DRY
 */
class FormManagerSettings extends BaseForm
{
    /** @var string */
    const DRIVER = 'adminLTE';

    /** @var array (required) */
    const SETTINGS = [
        'component' => false,
        // (optional - default `vue`)
        'driver'    => 'vue',
    ];

    /** @var array (required) */
    const VIEWS = [
        'raw' => 'index',
        'vue' => 'vue.index',
        'index2' => 'index2',
    ];

    /** @var \Illuminate\Support\Collection */
    protected $columns;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $actions;

    /** @var \Illuminate\Support\Collection */
    protected $rawSettings;

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     */
    public function __construct(ComponentDependence $settings)
    {
        $this->columns  = collect();
        $this->settings = collect();

        $this->make($settings);
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return self
     */
    private function make(ComponentDependence $settings)
    {
        $this->rawSettings = collect([
            'global' => $settings->data->get('global'),
            'local'  => $settings->data->get('local')
        ]);

        $this->makeColumns();
        $this->makeActions();
        $this->makeSettings();

        return $this;
    }

    /**
     * @return void
     */
    private function makeColumns()
    {
        $locale = $this->rawSettings->content('global.locale.main');

        $columns = $this->rawSettings->content('local.fields.*')->columns()->trans('text', $locale);

        $this->columns->put('base', $columns);
    }

    /**
     * @return void
     */
    private function makeActions()
    {
        $this->actions = $this->rawSettings->content('local.actions');
    }

    /**
     * @return void
     */
    private function makeSettings()
    {
        $this->makeViewComponentSettings();

        $this->settings->put('type', $this->rawSettings->content('global.type'));
        $this->settings->put('form', $this->makeFormSettings());
    }

    /**
     * @return void
     */
    private function makeViewComponentSettings()
    {
        $settings = self::SETTINGS;

        foreach (self::VIEWS as $type => $view) {
            $settings[$type] = $this->getView($type);
        }

        foreach (self::VIEW_COMPONENTS as $item) {
            $settings['components'][$item] = $this->getViewComponents($item);
        }

        $this->settings->put('views', collect($settings));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function makeFormSettings() : Collection
    {
        $form = $this->rawSettings->content('local.settings')
            ->only(['fields','buttons'])
            ->map(function($item) {
                if ($item instanceof Collection) {
                    $item = $item->map(function($item) {
                        if (is_array($item)) {
                            $item = collect($item);
                        }

                        if ($item->has('select') && $attr = $item->getData('select.attributes')) {
                            return $item
                                ->trans('label')
                                ->map(function($item) use ($attr) {
                                    $item['attributes'] = $attr->trans('placeholder');

                                    return $item;
                                });
                        }

                        return $item->trans('label');
                    });
                }

                return $item;
            });

        return $this->rawSettings->content('local.settings')->merge($form->all());
    }

    /**
     * @param  string|null $name
     * @return string
     */
    public function view(string $name = null) : string
    {
        if (! isset($name)) {
            return $this->settings->getData('views.raw');
        } elseif ($view = $this->settings->getData("form.view.{$name}")) {
            return $view;
        } elseif ($view = $this->settings->getData($name)) {
            return $view;
        }

        return $this->settings->getData('views.raw');

        /*if (! $name && $this->isComponent()) {
            $driver = $this->settings->get('driver');

            return $this->settings->content("views.{$driver}");
        }*/
        // @todo подумать
        // $name = $name ?? 'raw';
        // return $this->settings->content("views.{$name}");
    }

    /**
     * @param  string|null $type
     * @return \Illuminate\Support\Collection
     */
    public function columns(string $type = null) : Collection
    {
        return ! $type ? $this->columns : $this->columns->get($type);
    }

    public function actions() : Collection
    {
        return $this->actions;
    }

    /**
     * @param  string|null $param
     * @return mixed
     */
    public function settings(string $param = null)
    {
        return $param ? $this->settings->get($param) : $this->settings;
    }

    /*public function isComponent() : bool
    {
        return $this->settings->get('component', false);
    }*/

    /**
     * @param  string $param
     * @return string
     */
    private function getView(string $param) : string
    {
        return implode('.', [
           self::BASE_VIEW, // admin.components
           self::TYPE, // form
           self::DRIVER, // formUpdater
           self::VIEWS[$param] // index
        ]);
    }

    /**
     * Получение драйвера компонента
     *
     * @return string
     */
    private function getComponentDriver() : string
    {
        return $this->settings->get('driver', 'vue');
    }
}