<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPagesEmployeeTest extends TestCase
{
// INDEX

    public function login()
    {
        $user = \App\User::find(1);
        $this->be($user);
    }

    public function testEmployeeIndexSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.employees.index'));
        $this->assertResponseOk();
    }

// CREATE

    public function testEmployeeCreateSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.employees.create'));
        $this->assertResponseOk();
    }

// UPDATE

    public function testEmployeeUpdateSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.employees.edit', ['employees' => 1]));
        $this->assertResponseOk();
    }

// READ

    public function testEmployeeReadSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.employees.show', ['employees' => 1]));
        $this->assertResponseOk();
    }

// 404

    public function testEmployeeUpdate404()
    {
        $this->login();
        $this->call('GET', route('admin.employees.edit', ['employees' => 999]));
        $this->assertResponseStatus(404);
    }
}