<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Update;

use Actions;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Выполнение инициализации настроек для `Web`.`Employees`.`Update`
 * @todo DRY - support methods
 */
class UpdateConfigAction implements ActionContract
{
    /** @var array */
    const REPOSITORIES = [
        'main' => [
            'get' => [
                'all' => [
                    'method' => 'allCourseCountriesSelect',
                ],
                'departments-primary' => [
                    'method' =>'courseCountriesPrimary',
                    'params' => 'model'
                ]
            ],
            'set' => [
                'update' => [
                    'method' => 'updateEmployee',
                    'params' => 'requests'
                ],
                'departments-clean' => [
                    'method' => 'cleanEmployeeDepartments',
                    'params' => 'model',
                ]
            ],
        ],
        'dependencies' => [
            'departments' => [
                'get' => [
                    'primary-by' => [
                        'method' => 'getPrimaryById',
                        'params' => 'id'
                    ]
                ],
                'set' => [
                    'sync' => [
                        'method' => 'sync',
                        'params' => 'options',
                    ],
                ],
            ],
        ],
    ];

    /** @var array */
    const RELATIONS = ['departments'];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $response->put('repository', self::REPOSITORIES);
        $response->put('relations', self::RELATIONS);

        return $response;
    }
}