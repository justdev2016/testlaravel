<?php

return [
    'id'                     => 'Идентификатор',
    'name'                   => 'Имя',
    'departments'            => 'Отделы',
    'department'             => 'Отдел',
    // 'select-message'      => '-- Выберите отдел --',
    'created_at'             => 'Создан',
    'updated_at'             => 'Изменен',
    'delete-header-message'  => 'Удаление Отдела!',
    'create-header-message'  => 'Создание Отдела!',
    'delete-body-message'    => 'Вы действительно хотите удалить отдел!',
    'delete-error-message'   => 'Нет возможности удалить отдел!',
    'delete-success-message' => 'Отдел успешно удален!',
    'update-success-message' => 'Отдел успешно обновлен!',
    'create-success-message' => 'Отдел успешно создан!',
    'update-title'           => 'Редактирование отдела',
    'create-title'           => 'Создание отдела',
    'not-found'              => 'Не удается найти заданный отдел!',
];