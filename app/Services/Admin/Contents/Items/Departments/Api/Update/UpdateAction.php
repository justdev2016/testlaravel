<?php

namespace App\Services\Admin\Contents\Items\Departments\Api\Update;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Getter,Setter,Content};
use App\Services\Admin\Managers\Notify\SuccessOptions;

/**
 * Выполнение действия для `Ajax`.`Update`
 */
class UpdateAction implements ActionContract
{
    use Getter, Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $this->settings = $this->object->action();

        $request    = $this->settings->get('requests')->request();
        $mainSetter = $this->setter('main','update');

        // обновить данные модели
        $employee = $this->content($mainSetter, ['requests' => $this->settings->get('requests')]);

        $success = ['message' => trans('Departments.update-success-message'), 'data' => $employee];
        $notify  = Notify::success($success);

        $notify->withSession($notify->except('data')->toArray());

        return $notify;
    }
}