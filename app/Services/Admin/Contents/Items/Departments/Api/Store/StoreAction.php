<?php

namespace App\Services\Admin\Contents\Items\Departments\Api\Store;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Getter,Setter,Content};

/**
 * Выполнение действия для `Ajax`.`Store`
 */
class StoreAction implements ActionContract
{
    use Getter, Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $this->settings = $this->object->action();
        $request    = collect($this->settings->get('requests')->request()->all());
        $mainSetter = $this->setter('main','store');

        // внести данные модели
        $department = $this->content($mainSetter, ['request' => $request]);

        $success = ['message' => trans('departments.create-success-message'), 'data' => $department];
        $notify  = Notify::success($success);

        $notify->withSession($notify->except('data')->toArray());

        return $notify;
    }
}