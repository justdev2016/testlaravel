<?php

namespace App\Repositories;

use DB;
use App\Employee;
use App\Traits\Admin\ACLTrait;
use App\Traits\Admin\Contents\Presentable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collect;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Exceptions\Admin\SaveEmployeeException;
use App\Exceptions\Admin\DeleteEmployeeException;
use App\Services\Admin\Contents\Core\Support\Input;

class EmployeeRepository extends BaseRepository
{
    use ACLTrait, Presentable;

    /**
     * @return mixed
     */
    public function model()
    {
        return Employee::class;
    }

    /**
     * @param  Employee $employee
     * @return self
     */
    public function setModel(Employee $employee)
    {
        $this->model = $employee;

        return $this;
    }

// RESOURCE

    /**
     * @return mixed
     */
    public function index(Collect $request)
    {
        if ($request->get('request')->has('limit')) {
            $this->setModel(new Employee);

            $data = $this->model->paginate((int)$request->getData('request.limit'));

            $newCollection = $data->getCollection()->present($request->get('present'));

            return $data->setCollection($newCollection);
        }
    }

    /**
     * @param  string $presenter
     * @return \Illuminate\Support\Collection
     */
    public function show(string $presenter) : Collect
    {
        return $this->model->present($presenter)->get();
    }

    /**
     * @param  string $presenter
     * @return \Illuminate\Support\Collection
     */
    public function edit(string $presenter) : Collect
    {
        return $this->model->present($presenter)->edit();
    }

    /**
     * @param Input|Collect $request
     * @throws DeleteEmployeeException
     */
    public function destroy(Input $request)
    {
        if (! $request->model('employees')->delete()) {
            throw new DeleteEmployeeException(trans('employees.delete-error-message'), 1);
        }
    }

    /**
     * @param  \Illuminate\Support\Collection $request
     * @throws \App\Exceptions\Admin\SaveEmployeeException
     * @return Employee
     */
    public function store(Collect $request)
    {
        $employee  = $this->makeModel();

        $employee->last_name     = $request->get('last_name');
        $employee->first_name    = $request->get('first_name');
        $employee->surname       = $request->get('surname');
        $employee->phone         = $request->get('phone');
        $employee->email         = $request->get('email');
        $employee->position      = $request->get('position');
        $employee->birthday      = $request->get('birthday');
        $employee->employee_date = $request->get('employee_date');

        if (! $employee->save()) {
            throw new SaveEmployeeException(trans('message.save-error-message'), 1);
        }

        return $employee;
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Support\Input $request
     * @throws \App\Exceptions\Admin\SaveEmployeeException
     * @return Employee
     */
    public function updateEmployee(Input $request)
    {
        $employee  = $request->model('employees');
        $request = $request->request();

        $employee->last_name     = $request->get('last_name');
        $employee->first_name    = $request->get('first_name');
        $employee->surname       = $request->get('surname');
        $employee->phone         = $request->get('phone');
        $employee->email         = $request->get('email');
        $employee->position      = $request->get('position');
        $employee->birthday      = $request->get('birthday');
        $employee->employee_date = $request->get('employee_date');

        if (! $employee->save()) {
            throw new SaveEmployeeException(trans('message.save-error-message'), 1);
        }

        return $employee;
    }

    /**
     * @param  array $params
     * @return void
     */
    public function syncData(array $params)
    {
        // $model, $relation, $ids
        extract($params);

        $model->{$relation}()->sync($ids);
    }

    /**
     * @param  array $params
     * @return void
     */
    public function saveMany(array $params)
    {
        // $model, $relation, $models
        extract($params);

        $model->{$relation}()->saveMany($models);
    }

    /**
     * @param  Employee  $employee
     * @return void
     */
    public function cleanEmployeeDepartments(Employee $employee)
    {
        $ids = $this->employeeDepartmentsPrimary($employee)->toArray();

        $employee->departments()->detach($ids);
    }

    /**
     * @param  Employee  $employee
     * @return \Illuminate\Support\Collection
     */
    public function employeeDepartmentsPrimary(Employee $employee) : Collect
    {
        return collect($employee->departments()->get()->modelKeys());
    }
}