<div class="callout callout-danger @if(isset($static) && $static) static-error @else error @endif">
    <h4>Errors!</h4>
    @if(!empty($errors->all()))
        <ul>
        @foreach($errors->all() as $error)
            <li>{!! $error !!}</li>
        @endforeach
        </ul>
    @else
        <p>{{ session('notify')['message'] }}</p>
    @endif
</div>