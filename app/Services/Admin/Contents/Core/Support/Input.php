<?php

namespace App\Services\Admin\Contents\Core\Support;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс входных параметров `Web`||`Ajax`||`Api`
 */
class Input
{
    private $request;

    /**
     * @param \Illuminate\Http\Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Получение модели из запроса
     *
     * @param  string|null $name
     * @return mixed
     */
    public function model(string $name = null)
    {
        if (! isset($name)) {
            return $this->bindings()->parameters();
        }

        if($this->hasModel($name)) {
            return $this->bindings()->parameter($name);
        }
    }

    /**
     * @return \Illuminate\Http\Request
     */
    public function request()
    {
        return $this->request;
    }

    private function bindings() : Route
    {
        return $this->request->route();
    }

    /**
     * Проверка наличия параметра вмаршруте по его имени
     *
     * @param  string $name
     * @return bool
     */
    private function hasModel(string $name) : bool
    {
        return $this->bindings()->hasParameter($name);
    }
}