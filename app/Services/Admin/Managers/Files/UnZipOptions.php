<?php

namespace App\Services\Admin\Managers\Files;

use Illuminate\Support\Collection;
use Illuminate\Filesystem\Filesystem;
use App\Contracts\Managers\Files\Options;
use App\Validators\AdminFCommanderValidator;
use App\Services\Admin\Managers\Files\ArchiveInfo;
use App\Contracts\Managers\Files\Options as UnZipOptionsContract;

/**
 * Archive options
 */
class UnZipOptions extends BaseOptions implements UnZipOptionsContract
{
    /** @var string|array */
    protected $src;

    /** @var string */
    protected $dst;

    /** @var string|array|null */
    protected $extract;

    /** @var \ZipArchive */
    protected $manager;

    /** @var \Illuminate\Filesystem\Filesystem */
    protected $filesystem;

    /** @var \Symfony\Component\HttpFoundation\File\File */
    protected $complete;

    /**
     * Допустимый формат данных
     * Допустимые значения и их типы:
     * [
     *     src    => array|object   => required,
     *     dst    => string         => required,              
     *     random => inteder        => optional              
     * ]
     * 
     * @param \Illuminate\Support\Collection $collection
     */
    public function __construct(Collection $collection = null)
    {
        parent::__construct();

        $this->complete   = collect();
        $this->manager    = new \ZipArchive;
        $this->filesystem = app(Filesystem::class);
        
        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    /**
     * Инициализация входных данных
     * 
     * @param  array $collection
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->validate($collection->toArray(), AdminFCommanderValidator::RULE_ARCHIVE_ANY);

        $this->src     = $collection->get('src');
        $this->dst     = $collection->get('dst', $this->filesystem->dirname($this->src));
        $this->extract = $collection->get('extract', null);

        return $this;
    }
    
    /**
     * Получение информации о действии над файлами/папками
     * 
     * @param  \App\Contracts\Managers\Files\Options $options
     * @return App\Services\Admin\Managers\Info\Info
     */
    protected function info(Options $options)
    {
        return $this->info->make(new ArchiveInfo($options->complete));
    }

    /**
     * Извлекает все/избранные файл(-ы) из архива
     *
     * @param boolean $info
     * @throws \Exception
     * @return void|\App\Services\Admin\Managers\Files\Info
     */
    public function action(bool $info = true)
    {
        $unZip = $this->manager;

        if ($unZip->open($this->src) === TRUE) {

            if (! $this->extractTo($unZip)) {
                throw new \Exception(trans('messages.no-unzip-error'), 1);
            }

            $unZip->close();
        } else {
            throw new \Exception(trans('messages.no-unzip-error'), 1);
        }

        $this->complete->put('path', $this->dst);
        $this->complete->put('data', [$this->extract]);

        if ($info) {
            return $this->info($this);
        }
    }

    /**
     * @return bool
     */
    protected function isExtractFiles() : bool
    {
        return ! empty($this->extract);
    }

    /**
     * @param \ZipArchive  $manager
     * @return bool
     */
    protected function extractTo(\ZipArchive $manager) : bool
    {
        return $this->isExtractFiles() ? $manager->extractTo($this->dst, $this->extract) :  $manager->extractTo($this->dst);
    }
}