<?php

namespace App\Services\Admin\Managers\Files;

use Illuminate\Support\Collection;
use Illuminate\Filesystem\Filesystem;
use App\Services\Admin\Managers\Info\{Info,ProfileInfo};
use App\Contracts\Managers\Info\{Info as InfoContract,PathProfile,FileProfile};

/**
 * Предоставление информации о файлах/папках
 * и действия пользователя над файлами/папками
 */
class ArchiveInfo extends ProfileInfo implements InfoContract, PathProfile, FileProfile
{
    /** @var \Illuminate\Support\Collection */
    protected $data;

    /** @var \Illuminate\Filesystem\Filesystem */
    protected $filesystem;

    /**
     * @param \Illuminate\Support\Collection $options
     */
    public function __construct(Collection $options)
    {
        parent::__construct();

        $this->filesystem = app(Filesystem::class);

        $this->make($options);

        $this->initMacros();
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this, $method], $arguments);
    }

    /**
     * @param  \Illuminate\Support\Collection $options
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->data = $collection;

        return $this;
    }

    protected function initMacros()
    {
        $this->macroPath();
        $this->macroFile();
    }

    private function macroPath()
    {
        $that = $this;

        collect()->macro('path', function($path) use($that) {
            return collect([$this->items])->map(function($item) use($path, $that) {

                foreach ($path as $rule) {
                    $studly = 'raw' . studly_case($rule);

                    $result[$rule] = $that->{$studly}();
                }

                return $result;
            });
        });
    }

    private function macroFile()
    {
        $that = $this;

        collect()->macro('file', function($file) use($that) {
            return collect([$this->items])->map(function($item) use($file, $that) {

                foreach ($file as $rule) {
                    $studly = 'raw' . studly_case($rule);

                    $result[$rule] = $that->{$studly}();
                }

                return $result;
            });
        });
    }

// RAW
    /**
     * @return string
     */
    protected function rawPath() : string
    {
        return $this->data->get('path');
    }

    /**
     * @return array
     */
    protected function rawRealPath() : array
    {
        $data = (array) $this->data->get('data');

        foreach ($data as $item) {
            $result[] = str_finish($this->data->get('path'), '/') . $item;
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function rawFilename() : array
    {
        return (array) $this->data->get('data');
    }

    /**
     * @return array
     */
    protected function rawBasename() : array
    {
        return (array) $this->data->get('data');
    }

    /**
     * @return array
     */
    protected function rawPathname() : array
    {
        return $this->rawRealPath();
    }

// PROFILES
    /**
     * Вернуть всю информацию
     *
     * @return array
     */
    public function all() : array
    {
        return [];
    }

    /**
     * Вернуть информацию для web
     *
     * @return array
     */
    public function web() : array
    {
        return [
            'count' => $this->data->count(),
            // 'size' => ,
        ];
    }

    /**
     * Вернуть информацию о путях
     *
     * @return array
     */
    public function path() : array
    {
        return $this->data->path($path = $this->profiles->get('path'))->all();
    }

    /**
     * Вернуть информацию о файле
     *
     * @return array
     */
    public function file() : array
    {
        return $this->data->file($file = $this->profiles->get('file'))->all();
    }
}