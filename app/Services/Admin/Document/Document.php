<?php

namespace App\Services\Admin\Document;

use Illuminate\Support\Collection;
use App\Services\Admin\Support\Component;
use App\Contracts\Admin\DocumentComponentContract;

/**
 * Service for Facade `Document`
 */
class Document implements DocumentComponentContract
{
    /** @var string */
    const VIEW_PATH = 'admin.layouts.';

    /** @var string */
    const VIEW_INDEX = 'index';

    const VIEW_INCLUDES = [
        'contentheader',
        'htmlheader',
        'mainheader',
        'mainheaderwithoutleft',
        'scripts',
    ];

    /** @var DocumentComposite */
    protected $root;

    /** @var \Illuminate\Support\Collection */
    protected $section;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $config;

    /**
     * @param  \App\Services\Admin\Support\Component $root
     * @param  array $config
     * @param  array $section
     */
    public function __construct(Component $root, array $config, array $section)
    {
        $this->root    = $root;
        $this->config  = collect($config);
        $this->section = collect($section);

        $this->settings = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $views = [
            'includes' => $this->makeViewIncludes(),
            'section'  => $this->makeViewSection()
        ];

        $this->settings->put('views', $views);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function makeViewIncludes() : Collection
    {
        $folder   = self::VIEW_PATH;
        $keys     = collect(self::VIEW_INCLUDES);
        $includes = $keys->map(function($item, $key) use ($folder) {
            return $item = "{$folder}includes.{$item}";
        });

        return $keys->combine($includes->all());
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function makeViewSection() : Collection
    {
        $folder = self::VIEW_PATH;
        $index  = self::VIEW_INDEX;

        return $this->config
            ->getData('section.*')
            ->map(function($value, $section) use ($folder, $index) {
                return "{$folder}section.{$section}.{$index}";
            });
    }

    /**
     * @return DocumentComposite
     */
    public function root() : DocumentComposite
    {
        return $this->root;
    }

    /**
     * @param  \App\Services\Admin\Support\Component $item
     * @return void
     */
    public function add(Component $item)
    {
        $this->root->add($item);
    }

    /**
     * @param  string $name
     * @param  \App\Services\Admin\Support\Component $item
     * @return void
     */
    public function addToSection(string $name, Component $item)
    {
        $this->section($name)->add($item);
    }

    /**
     * @param  string $name
     * @return bool
     */
    public function isActive(string $name) : bool
    {
        return $this->config->getData('section')->active()->has($name);
    }

    /**
     * @param  string $name
     * @return DocumentComposite
     */
    public function section(string $name) : DocumentComposite
    {
        return $this->section->get($name);
    }

    /**
     * @param  array $data
     * @return Item
     */
    public function addItem(array $data) : Item
    {
        return new Item($data);
    }

    /**
     * @param  string $section
     * @return string
     */
    private function viewSection(string $section) : string
    {
        return $this->settings->getData("views.section.{$section}");
    }

    /**
     * @param  string $section
     * @return string
     */
    public function viewInclude(string $name) : string
    {
        return $this->settings->getData("views.includes.{$name}");
    }

    /**
     * Вернуть все данные
     *
     * @return \Illuminate\Support\Collection
     */
    public function data() : Collection
    {
        return collect($this->root->render());
    }

    /**
     * @param  string $name
     * @return string
     */
    public function render(string $name) : string
    {
        return view($this->viewSection($name), ["{$name}Data" => $this->section($name)->render()])->render();
    }
}