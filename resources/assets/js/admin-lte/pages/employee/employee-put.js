import { isSuccess } from "../functions/helpers"
import { notification, removeNotification } from "../functions/notification"
import { cancel } from "../functions/butonsActions"

$(() => {
    $('#birthday, #employee_date').datetimepicker({
        format: 'YYYY-MM-DD'
    })

    // save click !код повторяется!
    $('#employee-update-button').on('click', (e) => {
        e.preventDefault()

        $('.department-block').children('.input-group').each((index, el) => {
            $(el).parseDepartments()
        })

        let form = $('#update-employee-form'),
            href = form.attr('action'),
            data = {},
            selects = $('.employee-department').not(":hidden")

        data.last_name     = form.find('input#last_name').val()
        data.first_name    = form.find('input#first_name').val()
        data.surname       = form.find('input#surname').val()
        data.email         = form.find('input#email').val()
        data.phone         = form.find('input#phone').val()
        data.position      = form.find('input#position').val()
        data.birthday      = form.find('input#birthday').val()
        data.employee_date = form.find('input#employee_date').val()

        if (selects.length > 0) {
            let array = [],
                name

            selects.each((index, el) => {
                name = $(el).attr('name')

                array.push($(el).val())
            })

            data[name] = array
        }

        $.ajax({
            url: href,
            type: 'PUT',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(data)
        })
        .done((response) => {
            if (isSuccess(response)) {
                removeNotification()

                window.location.href = '/admin/employees'
            }
        })
        .fail((errors) => {
            notification('error', $.parseJSON(errors.responseText))
        })
        .always(() => {})

    })

    // cancel click
    cancel($('#employee-cancel'), '/admin/employees')

    require('./include/delete-department')
    require('./include/create-department')
    require('./include/parse-departments')
})