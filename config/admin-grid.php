<?php

return [
    // Grid Settings
    'data' => [
        'instance' => App\Services\Admin\Contents\Core\Components\Grid\Grid::class,
        'drivers' => [
            'dataTable' => [
                'active' => true,
                'instance' => App\Services\Admin\Contents\Core\Components\Grid\DataTable\DataTable::class,
                'renders' => [
                    // 'raw' =>
                ]
            ]
        ]
    ],
];