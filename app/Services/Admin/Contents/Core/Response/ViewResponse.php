<?php

namespace App\Services\Admin\Contents\Core\Response;

use Contents;

/**
 * @todo корректная реакция на ошибки!
 */
class ViewResponse
{
    /** @var \Illuminate\Support\Collection */
    protected $data;

    /** @var object */
    protected $settings;

    /**
     * @param array $data
     * @param mixed $settings
     */
    public function __construct(array $data, $settings)
    {
        $this->data     = collect($data);
        $this->settings = $settings;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        Contents::add(new Page($this->data, $this->settings));
    }

    /**
     * @param  string  $name
     * @return \Illuminate\View\View
     */
    public function response(string $name)
    {
        return $this
            ->settings
            ->get('views')
            ->response($name);
    }
}