export var inputmaskOptions = {
    digitsOptional: false,
    min: 0,
    max: 1000000,
    autoGroup: true,
    groupSeparator: " ",
    removeMaskOnSubmit: true,
    showMaskOnFocus: true,
    autoUnmask: true
}