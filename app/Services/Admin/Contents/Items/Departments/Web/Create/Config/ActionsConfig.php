<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Create\Config;

use Actions;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `actions`
 */
class ActionsConfig implements ActionContract
{
    /** @var array */
    const ACTIONS = [
        'form' => ['create.post'],
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $actions;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->actions = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        foreach (self::ACTIONS as $type => $data) {
            $this->actions->put($type, Actions::sync($data));
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('actions', $this->actions);
    }

}