{{-- Select --}}
@if ($value)
    @foreach ($value->get($settings->get('value', 'id')) as $item)
        @php
            $label = [
                'name'  => $data->getData("{$settings->get('value', 'id')}.field"),
                'value' => $settings->get('label')
            ];

            $select = [
                'name'     => $settings['name'],
                'list'     => $list->toArray(),
                'selected' => $item,
                'options'  => $settings->get('attributes')->addClass('form-control')->toArray()
            ];
        @endphp

        <div class="input-group">
            @include($component->viewComponent('label'), $label)

            @if($settings->get('required', false))
                @include($component->viewComponent('required')):
            @endif

            <button type="button" class="btn btn-danger delete-item">
                <i class="fa fa-minus" aria-hidden="true"></i>
            </button>

            @include($component->viewComponent('select'), $select)
        </div><!-- /.input-group -->
    @endforeach
@endif