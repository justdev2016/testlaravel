import { isSuccess } from "../../functions/helpers"
import { notification, removeNotification } from "../../functions/notification"

// Modal Create Course
$('#course-plus-button').on('click', (e) => {
    e.preventDefault()
    
    $('.create-course').modal('show')
    $('#create-course-form')[0].reset()
    $('.country-block').find('.input-group').remove()
})

// Create and Push
$('#course-create-button').on('click', (e) => {
    e.preventDefault()

    $('.country-block').children('.input-group').each((index, el) => {
        $(el).parseCountries()
    })
    
    let form = $('#create-course-form'),
        href = form.attr('action'),
        data = new FormData()

    data.append('title', form.find('input#title').val())
    data.append('slug', form.find('input#slug').val())
    if ($('.country-block select').length > 0) {
        $('.country-block select').each((index, el) => {
            let name = $(el).attr('name')

            data.append(name, $(el).val())
        })
    }
    data.append('modal', true)

    $.ajax({
        url: href,
        type: 'POST',
        processData: false,
        contentType: false,
        dataType: 'json',
        data: data
    })
    .done((response) => {
        if (isSuccess(response)) {
            let option = $('<option></option>')
                    .attr('selected', 'selected')
                    .val(response.data.id)
                    .text(response.data.title),
                output = [],
                p      = $('<p></p>')
            
            $('.create-course').modal('hide')
            removeNotification()
            
            if(response.message != undefined && response.message.length != 0) {
                output.push(response.message)
            }

            p.html(output.join(''))

            $('.content').prepend('<div id="success" class="callout callout-success">' +
                '<h4>Success!</h4></div>')
            $('#success').append(p)

            if ($('form').has('select#lesson-courses-list').length) {
                $('#lesson-courses-list').append(option)
            } else if ($('form').has('select#pack-courses-list').length) {
                $('#pack-courses-list').append(option)
            }
        }
        
    })
    .fail((errors) => {
        notification('error', errors.responseText, $('.course-modal-dialog'))
    })
    .always(() => {})
})

// Cancel
$('#course-cancel').on('click', (e) => {
    e.preventDefault()

    $('.create-course').modal('hide')
    removeNotification()
})

require('../include/parse-countries')