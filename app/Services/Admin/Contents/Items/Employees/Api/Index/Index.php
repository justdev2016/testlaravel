<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Index;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Core\Settings\WebConfigAction;
use App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings;

/**
 * Логика формирования данных для действия `Index`
 *
 * @todo доделать multi-компонентность на странице
 * @todo избавится от traits -> перенести их в декораторы
 */
class Index implements ActionContract
{
    /** @var \Illuminate\Support\Collection */
    protected $config;

    /**
     * @param \App\Services\Admin\Contents\Core\Support\Input $params
     */
    public function __construct(Input $params)
    {
        $this->config = [
            'settings' => new ContentSettings('api'),
            'requests' => $params,
        ];
    }

    /**
     * @return self
     */
    public function init()
    {
        $this->object = new WebConfigAction($this->config);
        $this->object = new IndexConfigAction($this->object);
        $this->object = new IndexAction($this->object);

        return $this;
    }

    /**
     * @return mixed
     */
    public function action()
    {
        return $this->object->action();
    }
}