@if(! array_key_exists('contents', $item))
    @include($menu->view('menu-item'), ['item' => $item])
@else
    <li class="treeview">
        <a href="#">
            <i class="fa fa-link"></i>
            <span>{{ $menu->getSubMenuName(key($item)) }}</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @each($menu->view('menu-tree-item'), $item[key($item)], 'item')
        </ul>
    </li>
@endif