<?php

namespace App\Services\Admin\Contents\Core\Support;

use Illuminate\View\View;
use Illuminate\Routing\Router;
use Illuminate\Support\Collection;
use App\Contracts\Admin\RegistryContract;

/**
 * Реестр карты `Widgets` для `Document`
 */
class WidgetMapperRegistry implements RegistryContract
{
    /** @var string */
    const GLOBAL = 'global';

    /** @var string */
    const LOCAL = 'local';

    /** @var array */
    private static $allowedKeys = [
        self::GLOBAL,
        self::LOCAL,
    ];

    /** @var array */
    private static $storedValues = [];

    /** @var \Illuminate\Support\Collection */
    protected $config;

    /** @var \Illuminate\View\View */
    protected $view;

    /** @var \Illuminate\Routing\Router */
    protected $router;

    /**
     * @param  array $config
     * @param  \Illuminate\Routing\Router $router
     */
    public function __construct(array $config, Router $router)
    {
        $this->config = collect($config);
        $this->router = $router;
    }

    /**
     * @see \App\Contracts\Admin\RegistryContract@set
     */
    public function set(string $key, $value)
    {
        if (! in_array($key, self::$allowedKeys)) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        self::$storedValues[$key] = $value;
    }

    /**
     * @see \App\Contracts\Admin\RegistryContract@get
     */
    public function get(string $key)
    {
        if (! in_array($key, self::$allowedKeys) || ! isset(self::$storedValues[$key])) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        return self::$storedValues[$key];
    }

    /**
     * @param \Illuminate\View\View  $view
     * @return self
     */
    public function make(View $view)
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @param string $type
     * @param string $name
     * @return \Illuminate\Support\Collection
     */
    public function section(string $type, string $name) : Collection
    {
        return $this->config->getData("{$type}")->filter(function($item) use ($name) {
            return in_array($name, $item);
        });
    }

    /**
     * @param  string $name
     * @return bool
     * @todo уникальные имена для `global`|`local`!?!
     */
    public function has(string $name) : bool
    {
        $global = $this->section('global', $name);
        $local  = $this->section('local', $name);

        return ! $global->isEmpty() || ! $local->isEmpty();
    }

    /**
     * @param  string $name
     * @param  mixed $value
     * @return \Illuminate\View\View
     */
    public function compose(string $name, $value) : View
    {
        return $this->view->with($name, $value);
    }
}