<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Create\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `settings`
 */
class SettingsConfig implements ActionContract
{
    /** @var array */
    const SETTINGS = [
        'form' => [
            'open' => [
                'id' => 'create-employee-form',
            ],
            // @todo проверить, может `name` уже не используется!!! + оптимизировать
            'fields' => [
                [
                    'text' => [
                        'name'  => 'last_name',
                        'label' => 'employees.last_name',
                        'required' => true
                    ]
                ],[
                    'text' => [
                        'name'     => 'first_name',
                        'label'    => 'employees.first_name',
                        'required' => true
                    ]
                ],[
                    'text' => [
                        'name'     => 'surname',
                        'label'    => 'employees.surname',
                        'required' => true
                    ]
                ],[
                    'text' => [
                        'name'     => 'email',
                        'label'    => 'employees.email',
                    ]
                ],[
                    'text' => [
                        'name'     => 'phone',
                        'label'    => 'employees.phone',
                    ]
                ],[
                    'text' => [
                        'name'     => 'position',
                        'label'    => 'employees.position',
                        'required' => true
                    ]
                ],[
                    'text' => [
                        'name'     => 'birthday',
                        'label'    => 'employees.birthday',
                        'required' => true

                    ]
                ],[
                    'text' => [
                        'name'     => 'employee_date',
                        'label'    => 'employees.employee_date',
                        'required' => true
                    ]
                ],[
                    'select' => [
                        'name'  => 'departments',
                        'label' => 'departments.department',
                        'attributes' => [
                            'class'       => 'employee-department',
                            'placeholder' => 'employees.select-department',
                        ],
                        'required' => true,
                        'settings' => [
                            'method' => 'add',
                            // не отображать данные
                            'show'  => false,
                            'multi' => true
                        ],
                    ]
                ],
            ],
            'buttons' => [
                [
                    'submit' => [
                        'type'  => 'success',
                        'label' => 'message.save',
                        'id'    => 'employee-create-button'
                    ],
                ],[
                    'cancel' => [
                        'type'  => 'danger',
                        'label' => 'message.cancel',
                        'id'    => 'employee-cancel'
                    ],
                ],
            ],
        ],
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->settings = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = collect(self::SETTINGS)->map(function($item) {
            return collect($item)->map(function($item) {
                return collect($item);
            });
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('settings', $this->settings);
    }
}