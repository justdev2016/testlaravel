<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Edit\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `settings`.`view`
 */
class FormViewConfig implements ActionContract
{
    /** @var string */
    const VIEW_PREFIX = 'admin.employees.resources.includes.form.';

    const VIEW_COMPONENTS = [
        'text',
        'select'
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    public function __construct(ActionContract $object)
    {
        $this->object = $object->action();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $settings = $this->object->getData('settings')->map(function($item) {
            $item['view'] = collect($this->makeView());

            return $item;
        });

        $this->object->put('settings', $settings);
    }

    /**
     * @return array
     */
    private function makeView() : array
    {
        foreach (self::VIEW_COMPONENTS as $component) {
            $view[$component] = self::VIEW_PREFIX . $component;
        }

        return $view;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        return $this->object;
    }
}