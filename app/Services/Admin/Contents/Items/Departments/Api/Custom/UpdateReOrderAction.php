<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Custom;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Setter,Content};

/**
 * Выполнение действия для `Ajax`.`UpdateReOrder`
 */
class UpdateReOrderAction implements ActionContract
{
    use Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Notify
     */
    public function action()
    {
        $this->settings = $this->object->action();

        $request    = $this->settings->get('requests')->request();
        $mainSetter = $this->setter('main','update');

        $request = [
            'data' => $request->all(),
            'ids'  => $request->input('*.id'),
        ];

        // обновить данные модели
        $employee = $this->content($mainSetter, ['request' => collect($request)]);

        $notify = Notify::success(['message' => trans('message.order-updated-success')]);

        return $notify;
    }
}