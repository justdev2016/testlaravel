@foreach($table['head'] as $column)
    <th>@lang('message.' . $column)</th>
@endforeach

@if(isset($table['actions']))
    <th>@lang('actions.' . $table['manage'])</th>
@endif