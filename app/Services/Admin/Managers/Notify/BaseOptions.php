<?php

namespace App\Services\Admin\Managers\Notify;

use App\Validators\AdminNotifyValidator;

class BaseOptions
{
    /**
     * @var \App\Validators\AdminFCommanderValidator
     */
    protected $validator;

    public function __construct()
    {
        $this->validator = app(AdminNotifyValidator::class);
    }

    /**
     * Валидация данных действия
     * 
     * @param  array $data
     * @param  string $rules
     * @return void
     */
    protected function validate(array $data, string $rules)
    {
        $this->validator->passesOrFail($data, $rules);
    }
}