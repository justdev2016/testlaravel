import { getLocale } from "../functions/helpers"
import _ from "lodash"

/**
 * dataTableOptions
 * @todo подумать над режимом ручного reOrdering (по кнопке)
 */
let dataTableOptions = {}

if (typeof(dataTableSettings) !== "undefined") {
    _.merge(dataTableOptions, dataTableSettings)
}

if (getLocale() != 'en') {
    dataTableOptions.language = {
        url: "/admin-lte/js/plugins/datatables/lang/" + getLocale() +".json"
    }
}

/**
 * dataTableOptionsDefault
 * @todo подумать над режимом ручного reOrdering (по кнопке)
 */
let dataTableOptionsDefault = {
    scrollY: "550px",
    scrollCollapse: true,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
    paging: true
}

// Global `locale` (app.blade.php)
if (getLocale() != 'en') {
    dataTableOptionsDefault.language = {
        url: "/admin-lte/js/plugins/datatables/lang/" + getLocale() +".json"
    }
}

export { dataTableOptions, dataTableOptionsDefault }