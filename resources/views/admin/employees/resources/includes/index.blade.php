@foreach ($components as $component)
    @include($component->view())

    @if ($component->settings('views')->has('javascript'))
        @include($component->settings('views.javascript'), ['settings' => $component->settings()])
    @endif
@endforeach