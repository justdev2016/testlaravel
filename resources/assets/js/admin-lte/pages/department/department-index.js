import Vue from 'vue'
import VueResource from 'vue-resource'
import { unset, merge } from 'lodash'
import AppPagination from '../../components/app-pagination.vue'
import { notification } from '../functions/notification'

Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content

new Vue({
    el: "#app",
    components: {
        AppPagination
    },
    data: {
        raw: {},
        url: '',
        index: '',
        fields: ['id', 'name'],
    },
    computed: {
        pagination () {
            let data = this.raw

            unset(this.data, 'data')

            return data
        },
        contents () {
            return this.raw.data
        }
    },
    methods: {
        del (index, e) {
            this.url = $(e.currentTarget).attr('href')

            this.index = index

            $('.delete-department-modal').modal('show')
        },
        destroy (e) {
            this.$http.delete(this.url)
                .then(({data}) => {
                    $('.delete-department-modal').modal('hide')

                    this.raw.data.splice(this.index, 1)

                    notification('success', data.message)
                }, (error) => {})
        },
        paginate (page) {
            let params = {}

            params.params = this.getForm()
            params.params.page = page

            this.search(params)
        },
        filter () {
            let params = {}

            params.params = this.getForm()

            this.search(params)
        },
        init () {
            this.$http.get(sync)
            .then(({data}) => {
                this.raw = data.data
            }, (error) => {})
        }
    },
    mounted () {
        this.init()
    }
})
