@include('admin.layouts.includes.form.open', ['params' => $form['header-course']])

  {{-- Title --}}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        {!! Form::label('title', trans('courses.title')) !!}
        {!! Form::text('title', old('title'), ['id' => 'title', 'class' => 'form-control']) !!}
      </div><!-- /.form-group -->
    </div>
  </div><!-- /.row -->

  {{-- Slug --}}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="slug">@lang('courses.slug')<strong class="required">*</strong></label>
        {!! Form::text('slug', old('slug'), ['id' => 'slug', 'class' => 'form-control']) !!}
      </div><!-- /.form-group -->
    </div>
  </div><!-- /.row -->

  {{-- Countries --}}
  <div class="row">
    <div class="form-group">
      <div class="col-md-7 country-block">
        <h2>
          @lang('departments.departments')
          <button type="button" class="btn btn-success" id="create-country">
            <i class="fa fa-plus" aria-hidden="true"></i>
          </button>
        </h2>
      </div><!-- /.col-md -->
    </div><!-- /.form-group -->
  </div><!-- /.row -->

  {{-- Buttons --}}
  <div class="form-group buttons">
    <p>
    {!! Form::submit(trans('message.save'), ['class' => 'btn btn-success', 'id' => 'course-create-button']) !!}
    {!! Form::button(trans('message.cancel'), ['class' => 'btn btn-danger', 'id' => 'course-cancel']) !!}
    </p>
  </div>
{!! Form::close() !!}