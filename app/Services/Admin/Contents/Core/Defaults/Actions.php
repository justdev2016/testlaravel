<?php

namespace App\Services\Admin\Contents\Core\Defaults;

use Illuminate\Support\Collection;

/**
 * Действия по-умолчанию
 * Основные задачи:
 * [
 *     хранить список,
 *     проверять входные данные на наличие и разрешения использования (ACL)
 * ]
 * Основные данные о компонентах
 */
class Actions extends Defaults
{
    /** @var array [CRUD => REST] */
    const ACTIONS = [
        'create' => [
            'get'  => 'create',
            'post' => 'store',
        ],
        'read'   => 'show',
        'update' => [
            'get' => 'edit',
            'put' => 'update',
        ],
        'delete' => 'destroy'
    ];

    public function __construct()
    {
        $this->make();
    }

    /**
     * @return void
     */
    protected function make()
    {
        $this->defaults = collect(self::ACTIONS);
    }

    /**
     * Синхронизация данных в `Content`
     *
     * @param  array  $data
     * @return \Illuminate\Support\Collection
     */
    public function sync(array $data) : Collection
    {
        return $this->dot()->only($data);
    }

    /**
     * Возвращает коллекцию, где ключами выступают
     * действия CRUD объеденены с методами REST через "."
     *
     * @return \Illuminate\Support\Collection
     */
    public function dot() : Collection
    {
        return collect(array_dot($this->defaults->all()));
    }


}