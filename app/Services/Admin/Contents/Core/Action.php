<?php

namespace App\Services\Admin\Contents\Core;

use SPA;
use Page;
use Notify;
use App\Services\Admin\Contents\Core\Response\{JsonResponse,ViewResponse};

/**
 * Класс управления действиями для `content`
 */
class Action
{
    /** @var \Illuminate\Support\Collection */
    protected $params;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

	/** @var array */
	protected $data;

    /** @var string */
    protected $type;

    /** @var array */
    protected $messages;

    public function __construct()
    {
        $this->params = collect();
    }

    /**
     * @param  string $type
     * @return self
     */
    public function init(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Записывает тип `content` для внутренних потребностий
     *
     * @param string $contentType
     * @return void
     */
    public function setContentType(string $contentType)
    {
        $this->params->put('content', $contentType);
    }

    /**
     * Принятие решения о типе выполняемого действия
     * в зависимости от источника запроса
     *
     * @param  string $action
     * @return self
     */
    public function make(string $action)
    {
        $settings = Page::instance($this->params->get('content'), 'action');

        if ($this->type === 'web') {
            $this->webAction($settings, $action);
        }

        if ($this->type === 'api') {
            $this->apiAction($settings, $action);
        }

        return $this;
    }

    /**
     * Получение данных для `web`
     *
     * @param  object $settings
     * @param  string $action
     * @return void
     */
    private function webAction($settings, string $action)
    {
        // $data, $settings
        extract($settings->make($action, $this->type)->init()->action());

        $this->data     = $data;
        $this->settings = $settings;
    }

    /**
     * Получение данных для `api`
     *
     * @param  object $settings
     * @param  string $action
     * @return void
     */
    private function apiAction($settings, string $action)
    {
        \DB::beginTransaction();
        try {
            $this->messages = ['messages' => $settings->make($action, $this->type)->init()->action()];

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            dd($e);
            $this->messages = ['messages' => Notify::error($e), 'code' => 422];
        }
    }

    /**
     * Принятие решения о формате фозвращаемых данных
     * в зависимости от источника запроса
     *
     * @return mixed
     */
    public function response()
    {
        if ($this->type === 'web') {
            return (new ViewResponse($this->data, $this->settings))->response($this->settings->get('action'));
        }

        if ($this->type === 'api') {
            return (new JsonResponse($this->messages))->response();
        }
    }
}