<div class="modal modal-danger delete-departments-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">@lang('departments.delete-header-message')</h4>
      </div>
      <div class="modal-body">
        <p>@lang('departments.delete-body-message')</p>
      </div>
      <div class="modal-footer">
        {!!
          Form::button('Cancel',
            ['class' => "btn btn-outline pull-left", 'data-dismiss' => "modal"])
        !!}
        {!!
          Form::button(trans('actions.destroy'),
            ['class' => "btn btn-outline", 'id' => "delete-button", 'role' => "button", "@click" => "destroy"])
        !!}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->