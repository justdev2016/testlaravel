<?php

return [
    /**
     * Single Page Application (SPA)
     *
     * @var bool Active?
     */
    'spa' => false,

    /**
     * Режим загрузки данных, если spa = true:
     * `local`  - каждый комполент обращается за своими данными по своим url;
     * `global` - данные для всех компонентов грузятся один раз;
     *
     * @var string
     */
    'mode' => 'local',

    /**
     * All components => config files
     */
    'components' => [
        'grid' => 'admin-grid',
        'info' => 'admin-info',
        'form' => 'admin-form',
    ],

    /**
     * Formatting Date/Time/DateTime for countries
     */
    'date_time' => [
        'default' => [
            'full'  => 'm.d.Y h:m:s',
            'small' => 'm.d.Y h:m',
        ],
        'ru' => [
            'full'  => 'd.m.Y h:m:s',
            'small' => 'd.m.Y h:m',
        ],
    ],
    'date' => [
        'default' => [
            'alpha_num' => 'd, F Y',
            'num'       => 'm.d.Y',
        ],
        'ru' => [
            'alpha_num' => 'd F Y',
            'num'       => 'd.m.Y',
        ],
    ],

    /**
     * `Document`.`sections` state
     */
    'document' => [
        'section' => [
            'content' => true,
            'footer'  => false,
            'header'  => true,
            'left'    => true,
        ],
    ],

    /**
     * Front-End Path
     */
    'frontEnd' => [
        'css' => '/admin-lte/css/',
        'js'  => '/admin-lte/js/',
    ],

    /**
     * Page Widget Map
     * ВАЖНО! Имена виджетов должны быть уникальными!
     */
    'map' => [
        'global' => [
            'content' => [
                'content'
            ],
            'footer'  => [
                //
            ],
            'header'  => [
                //
            ],
            'left'    => [
                'menu'
            ],
        ],
        'local'  => [
            'content' => [
                //
            ],
            'footer'  => [
                //
            ],
            'header'  => [
                //
            ],
            'left'    => [
                //
            ],
        ],
    ],
];