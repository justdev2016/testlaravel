// create country
$('#create-country').on('click', (e) => {
    e.preventDefault()

    let clone = $('.hidden-country').clone(true)
    // select2                
    clone
        .appendTo('.country-block')
        .removeClass('hidden-country')
})
