<?php

namespace App\Services\Admin\Contents\Core\Components\Info\AdminLTE;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\Info\BaseInfo;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;
use App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings;

/**
 * Валидация и подготовка данных
 *
 * @todo реализовать функционал применение настроек в зависимости от логики страницы
 * @todo использование множественных компонентов одного типа на одной странице
 */
class AdminLTESettings extends BaseInfo
{
    /** @var string */
    const DRIVER = 'adminLTE';

    /** @var array (required) */
    const SETTINGS = [
        'component' => false,
        // (optional - default `vue`)
        'driver'    => 'vue',
    ];

    /** @var array (required) */
    const VIEWS = [
        'raw'    => 'show',
        'vue'    => 'vue.show',
        'suffix' => 'adminLTE.includes',
    ];

    /** @var \Illuminate\Support\Collection */
    protected $columns;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $actions;

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     */
    public function __construct(ComponentDependence $settings)
    {
        parent::__construct();

        $this->columns  = collect();
        $this->settings = collect();

        $this->makeSettings();

        $this->make($settings);
    }

    // INIT

    /**
     * Global Settings
     *
     * @return void
     */
    private function makeSettings()
    {
        $this->makeViewSettings();
    }

    /**
     * @return void
     */
    protected function makeViewSettings()
    {
        $settings = self::SETTINGS;

        foreach (self::VIEWS as $type => $view) {
            $settings[$type] = $this->getView($type);
        }

        foreach (self::VIEW_COMPONENTS as $item) {
            $settings['components'][$item] = $this->getViewComponents(self::VIEWS['suffix'], $item);
        }

        $this->settings->put('views', collect($settings));
    }

    /**
     * @param  \App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings $settings
     * @param  \Illuminate\Support\Collection $pageSettings
     * @return self
     */
    public function make(ComponentDependence $settings)
    {
        $global = $settings->data->get('global');
        $local  = $settings->data->get('local');

        $locale   = $global->content('locale.main');

        $columns  = $local->getData('fields')->columns()->trans('text', $locale);

        $this->columns->put('base', $columns);

        $this->actions = $local->get('actions');

        $this->settings->put('actions', $global->only(['type']));

        $this->settings->put('routes', $local->getData('routes.info'));

        return $this;
    }

    public function view(string $param = null) : string
    {
        if (! $param && $this->isComponent()) {
            $driver = $this->settings->get('driver');

            return $this->settings->content("views.{$driver}");
        }

        if ($param) {
            return $this->settings->getData("views.components.{$param}");
        }

        return $this->settings->getData('views.raw');
    }

    /**
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    public function columns(string $name = null) : Collection
    {
        return ! isset($name)
            ? $this->columns
            : $this->columns->getData($name);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function actions() : Collection
    {
        return $this->actions;
    }

    /**
     * @param  string|null $name
     * @return mixed
     */
    public function routes(string $name = null)
    {
        return ! $name
            ? $this->settings->get('routes')
            : $this->settings->getData("routes.{$name}");
    }

    /**
     * @param  string|null $param
     * @return mixed
     */
    public function settings(string $param = null)
    {
        return $param ? $this->settings->get($param) : $this->settings;
    }

    public function isComponent() : bool
    {
        return $this->settings->get('component', false);
    }

    // PRIVATE

    /**
     * @param  string $param
     * @return string
     */
    private function getView(string $param) : string
    {
        return implode('.', [
           self::BASE_VIEW, // admin.components
           self::TYPE, // grid
           self::DRIVER, // dataTable
           self::VIEWS[$param] // index
        ]);
    }

    /**
     * Получение драйвера компонента
     *
     * @return string
     */
    private function getComponentDriver() : string
    {
        return $this->settings->get('driver', 'vue');
    }
}