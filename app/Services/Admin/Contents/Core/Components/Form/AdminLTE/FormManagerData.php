<?php

namespace App\Services\Admin\Contents\Core\Components\Form\AdminLTE;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Формирование данных
 */
class FormManagerData
{
    /** @var string */
    const TYPE = 'form';

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /** @var array */
    const METHODS = ['content', 'getData'];

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     */
    public function __construct(ComponentDependence $data)
    {
        $this->make($data);
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this->data, $method) || in_array($method, self::METHODS) ) {
            return call_user_func_array([$this->data, $method], $arguments);
        }

        throw new \Exception(trans('message.method-not-exist'), 1);
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @return void
     * @todo проверка на отсутствие `data`
     */
    private function make(ComponentDependence $data)
    {
        $this->data = $data->data->map(function($item, $key) {
            if ($key === 'actions') {
                $item = $item
                    ->except(['create.get'])
                    ->map(function($item) {
                        return trans("actions.{$item}");
                    });
            }

            return $item;
        });
    }
}