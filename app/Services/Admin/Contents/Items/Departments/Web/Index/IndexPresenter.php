<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Index;

use App\Services\Admin\Contents\Core\Support\Presenter;

class IndexPresenter extends Presenter
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name'];
}