<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Edit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collect;
use App\Services\Admin\Contents\Core\Support\Presenter;

class EditPresenter extends Presenter
{
    public function edit() : Collect
    {
        return collect([
            'id'            => $this->model->id,
            'last_name'     => $this->model->last_name,
            'first_name'    => $this->model->first_name,
            'surname'       => $this->model->surname,
            'email'         => $this->model->email,
            'phone'         => $this->model->phone,
            'position'      => $this->model->position,
            'birthday'      => $this->model->birthday,
            'employee_date' => $this->model->employee_date,
            'departments' => [
                'id' => $this->model->departments->pluck('id')
            ]
        ]);
    }
}