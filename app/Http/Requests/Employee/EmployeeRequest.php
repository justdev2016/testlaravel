<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\Request;
use App\Validators\AdminEmployeeValidator;

class EmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(AdminEmployeeValidator $validator)
    {
        return $validator->getRules($validator::RULE_ANY);
    }
}