<?php

namespace App\Contracts\Admin;

use Illuminate\Support\Collection;
use App\Services\Admin\Support\Component;


interface DocumentComponentContract
{
    public function add(Component $component);

    // public function remove(Component $component);

    // public function display();
}