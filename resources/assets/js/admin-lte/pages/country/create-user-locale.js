import { isSuccess } from "../functions/helpers"
import { notification, removeNotification } from "../functions/notification"

// Modal Create Country User Locale
$('#user-locale-plus-button').on('click', (e) => {
    e.preventDefault()
    
    $('.create-country-user-locale').modal('show')
});

// Valid and Push
$('#country-user-locale-create-button').on('click', (e) => {
    e.preventDefault()

    let form = $('#create-course-form'),
        href = form.attr('action'),
        data = new FormData()

    data.append('slug', form.find('input#slug').val())

    $.ajax({
        url: href,
        type: 'POST',
        processData: false,
        contentType: false,
        dataType: 'json',
        data: data
    })
    .done((response) => {
        if (isSuccess(response)) {
            let option = $('<option></option>')
                    .attr('selected', 'selected')
                    .val(response.data.slug)
                    .text(response.data.value),
                output = []
            
            $('.create-country-user-locale').modal('hide')
            removeNotification()
            
            if(response.message != undefined && response.message.length != 0) {
                output.push(response.message)
            }

            $('#user-locales-list').append(option)
        }
        
    })
    .fail((errors) => {
        notification('error', errors.responseText, $('.create-country-user-locale-modal-dialog'))
    })
    .always(() => {})
})

// Cancel
$('#country-user-locale-cancel').on('click', (e) => {
    e.preventDefault()

    $('.create-country-user-locale').modal('hide')
    removeNotification()
});