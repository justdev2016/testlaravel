<?php

namespace App\Services\Admin\Contents\Core\Components\Info\AdminLTE;

use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\Info\InfoStrategy;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Создание пользовательского интерфейса для построения Info
 */
class AdminLTE implements InfoStrategy
{
    /** @var string */
    const TYPE = 'info';

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return self
     * @todo валидация входных параметров!
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $this->data     = new AdminLTEData($data);
        $this->settings = new AdminLTESettings($settings);

        return $this;
    }

    /**
     * @param  string|null $param
     * @return string
     */
    public function view(string $param = null) : string
    {
        return $this->settings->view($param);
    }

    /**
     * @param  string|null $param
     * @return mixed
     */
    public function routes(string $param = null)
    {
        return $this->settings->routes($param);
    }

    /**
     * Вернуть данные для формирования столбцов
     *
     * @return \Illuminate\Support\Collection
     */
    public function columns(string $name = null) : Collection
    {
        return $this->settings->columns($name);
    }

    /**
     * Вернуть данные для формирования столбцов
     *
     * @return \Illuminate\Support\Collection
     */
    public function actions() : Collection
    {
        return $this->settings->actions();
    }

    /**
     * Вернуть данные для заполнения таблицы данными
     *
     * @return mixed
     */
    public function data(string $name = null)
    {
        return $this->data->data($name);
    }

    /**
     * Вернуть настройки компоненты
     *
     * @param  string $param
     * @return mixed
     */
    /*public function settings(string $param = null)
    {
        return $this->settings->settings($param);
    }*/
}