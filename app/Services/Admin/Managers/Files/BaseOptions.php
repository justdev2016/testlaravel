<?php

namespace App\Services\Admin\Managers\Files;

use App\Services\Admin\Managers\Info\Info;
use App\Validators\AdminFCommanderValidator;

class BaseOptions
{
    /**
     * @var \App\Validators\AdminFCommanderValidator
     */
    protected $validator;

    /**
     * @var \Services\Admin\Managers\Files\Info
     */
    protected $info;
    
    public function __construct()
    {
        $this->validator = app(AdminFCommanderValidator::class);
        $this->info      = app(Info::class);
    }

    /**
     * Валидация данных действия
     * 
     * @param  array $data
     * @param  string $rules
     * @return void
     */
    protected function validate(array $data, string $rules)
    {
        $this->validator->passesOrFail($data, $rules);
    }
}