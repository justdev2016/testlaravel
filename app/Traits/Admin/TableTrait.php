<?php

namespace App\Traits\Admin;

use Illuminate\Support\Collection;

/**
 * Формирование данных 
 * для построения таблицы
 */
trait TableTrait
{
    /**
     * Возвращает данные необходимые
     * для построения таблицы
     *
     * @return array
     */
    public function tableConfig() : array
    {
        return [
            'head'    => $this->getColumns(),
            'manage'  => self::DASHBOARD_COLUMNS_ACTIONS,
            'actions' => $this->getActions(),
        ];
    }

    /**
     * Возвращает поля для заголовка таблицы
     *
     * @return array
     */
    public function getColumns() : array
    {
        return self::DASHBOARD_COLUMNS;
    }

    /**
     * Возвращает допустимые действий для таблицы
     *
     * @return array
     */
    public function getActions() : array
    {
        return self::DASHBOARD_TABLE_ACTIONS;
    }

    // RESOURCES

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collectionActions() : Collection
    {
        return collect(self::DASHBOARD_TABLE_ACTIONS);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collectionColumns() : Collection
    {
        return collect(self::DASHBOARD_COLUMNS);
    }

    /**
     * @return array
     */
    public function gridConfig() : array
    {
        return [
            'columns' => $this->collectionColumns(),
            'actions' => [
                self::DASHBOARD_COLUMNS_ACTIONS => $this->collectionActions()
            ]
        ];
    }
}