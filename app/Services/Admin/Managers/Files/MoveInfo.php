<?php

namespace App\Services\Admin\Managers\Files;

use Illuminate\Support\Collection;
use App\Services\Admin\Managers\Info\{Info,ProfileInfo};
use App\Contracts\Managers\Info\{Info as InfoContract,PathProfile,FileProfile};

/**
 * Предоставление информации о файлах/папках
 * и действия пользователя над файлами/папками
 */
class MoveInfo extends ProfileInfo implements InfoContract, PathProfile, FileProfile
{
    /** @var \Illuminate\Support\Collection */
    protected $data;

    public function __construct(Collection $options)
    {
        parent::__construct();

        $this->make($options);

        $this->initMacros();
    }

    /**
     * @param  \Illuminate\Support\Collection $options
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->data = $collection;

        return $this;
    }

    protected function initMacros()
    {
        $this->macroPath();
        $this->macroFile();
    }

    private function macroPath()
    {
        collect()->macro('path', function($path) {
            return $this->map(function($file) use($path) {

                foreach ($path as $rule) {
                    $studly = 'get' . studly_case($rule);

                    $result[$rule] = $file->{$studly}();
                }

                return $result;
            });
        });
    }

    private function macroFile()
    {
        collect()->macro('file', function($file) {
            return $this->map(function($item) use($file) {

                foreach ($file as $rule) {
                    $studly = 'get' . studly_case($rule);

                    $result[$rule] = $item->{$studly}();
                }

                return $result;
            });
        });
    }

// PROFILES
    /**
     * Вернуть всю информацию
     *
     * @return array
     */
    public function all() : array
    {
        return [];
    }

    /**
     * Вернуть информацию для web
     *
     * @return array
     */
    public function web() : array
    {
        return [
            'count' => $this->data->count(),
            // 'size' => ,
        ];
    }

    /**
     * Вернуть информацию о путях
     *
     * @return array
     */
    public function path() : array
    {
        return $this->data->path($path = $this->profiles->get('path'))->all();
    }

    /**
     * Вернуть информацию о файле
     *
     * @return array
     */
    public function file() : array
    {
        return $this->data->file($file = $this->profiles->get('file'))->all();
    }
}