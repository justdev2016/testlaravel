<div class="box box-solid">
  <div class="box-header with-border">
    <i class="fa fa-text-width"></i>
    <h3 class="box-title">@lang('courses.description.learn')</h3>
  </div><!-- /.box-header -->
  <div class="box-body">
    <dl class="dl-horizontal">
    @foreach($data['fields'] as $field)
      <dt>@lang('courses.' . $field)</dt>
      <dd>
        @if($field == 'created_at' || $field == 'updated_at')
          {{ formatDateTime($data['course'][$field], 'small') }}
        @else
          {{ $data['course'][$field] }}
        @endif
      </dd>
    @endforeach
    </dl>
  </div><!-- /.box-body -->
</div>

@if (isset($data['buttons']))
  @include('admin.layouts.includes.links-as-buttons', ['parameters' => $data['buttons']])
@endif

@include('admin.courses.includes.modal.delete-course')