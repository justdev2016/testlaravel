<?php

namespace App\Services\Admin\Managers\Notify;

use Illuminate\Support\Collection;

/**
 * Сервис формирования сообщений для пользователя
 */
class AdminNotificationManager
{
    /** @const Класс статуса с сообщением о редиректе */
    const STATUS_WARNING = 'warning';

    /** @const Класс статуса с сообщением об успехе */
    const STATUS_SUCCESS = 'success';

    /** @const Класс статуса с сообщением об ошибке */
    const STATUS_ERROR = 'error';

    /** @const Класс статуса с информационным сообщением */
    const STATUS_INFO = 'info';

    /** @var \Illuminate\Support\Collection */
    protected $notify;

    public function __construct(Collection $collection = null)
    {
        $this->notify = collect();

        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->notify, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->get(), $method], $arguments);
    }

    /**
     * @param  \App\Services\Admin\Managers\Notify\BaseOptions $options
     * @return self
     */
    public function make(BaseOptions $options)
    {
        $this->notify = $options->notify;

        return $this;
    }

    /**
     * @param  array $data
     * @return self
     */
    public function success(array $data)
    {
        $this->notify = (new SuccessOptions($data))->notify;

        return $this;
    }

    /**
     * @param  mixed $data
     * @return self
     */
    public function error($data)
    {
        $this->notify = (new ErrorOptions($data))->notify;

        return $this;
    }

    /**
     * @param  array $data
     * @return void
     */
    public function withSession(array $data)
    {
        session()->flash('notify', $data);
    }

    /**
     * Возвращает отфильтрованую коллекцию
     *
     * @return \Illuminate\Support\Collection
     */
    public function get() : Collection
    {
        return $this->notify->filter();
    }

    /**
     * Формирует массив сообщения об ошибке для пользователя
     *
     * @param  \Exception $e
     * @return array
     */
    public function createErrorMessage(\Exception $e) : array
    {
        return $this
            ->init(self::STATUS_ERROR)
            ->setMessage($this->validErrors($e))
            ->get();
    }

    // SUPPORT

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllStatus() : Collection
    {
        return collect([
            self::STATUS_ERROR,
            self::STATUS_INFO,
            self::STATUS_SUCCESS,
            self::STATUS_WARNING,
        ]);
    }
}