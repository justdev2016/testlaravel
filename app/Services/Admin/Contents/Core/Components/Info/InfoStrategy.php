<?php

namespace App\Services\Admin\Contents\Core\Components\Info;

use App\Services\Admin\Contents\Core\Components\ComponentDependence;

interface InfoStrategy
{
    public function make(ComponentDependence $data, ComponentDependence $settings);
}