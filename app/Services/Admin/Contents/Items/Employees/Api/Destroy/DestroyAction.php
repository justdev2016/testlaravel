<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Destroy;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Setter,Content};

/**
 * Выполнение действия для `Ajax`.`Destroy`
 */
class DestroyAction implements ActionContract
{
    use Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $this->settings = $this->object->action();

        $mainSetter = $this->setter('main','destroy');

        $this->content($mainSetter, ['request' => $this->settings->get('requests')]);

        $notify = Notify::success(['message' => trans('employees.delete-success-message')]);

        return $notify;
    }
}