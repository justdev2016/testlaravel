<?php

namespace App\Services\Admin\Contents\Core\Support;

use App\Contracts\Admin\RegistryContract;

/**
 * Реестр всех `actions` для `Content`
 */
class ContentRegistry implements RegistryContract
{
    /** @var string */
    const SETTINGS = 'settings';

    /** @var array */
    private static $allowedKeys = [
        self::SETTINGS,
    ];

    /** @var array */
    private static $storedValues = [];

    /**
     * @see \App\Contracts\Admin\RegistryContract@set
     */
    public function set(string $key, $value)
    {
        if (! in_array($key, self::$allowedKeys)) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        if (empty(self::$storedValues[$key])) {
            self::$storedValues[$key] = $value;
        } else {
            self::$storedValues[$key] = array_merge(self::$storedValues[$key], $value);
        }

    }

    /**
     * @see \App\Contracts\Admin\RegistryContract@get
     */
    public function get(string $key)
    {
        if (! in_array($key, self::$allowedKeys) || ! isset(self::$storedValues[$key])) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        return self::$storedValues[$key];
    }
}