<?php

namespace App\Contracts\Admin;

interface RegistryContract
{
    /**
     * @param  string $key
     * @param  mixed  $value
     * @throws \InvalidArgumentException
     * @return void
     */
    public function set(string $key, $value);

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key);
}