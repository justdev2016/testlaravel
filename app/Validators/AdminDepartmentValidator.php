<?php

namespace App\Validators;

class AdminDepartmentValidator extends BaseValidator
{
    /**
     * @var string
     */
    const RULE_ANY = 'any';

    /**
     * @var array Правила валидации
     */
    protected static $rules;

    /**
     * @var string
     */
    protected $countries;

    public function __construct()
    {
        self::$rules = [
            self::RULE_ANY => $this->ruleAny()
        ];
    }

    /**
     * Возвращает правило валидации по его имени
     *
     * @param  string $name
     * @return array
     */
    public function getRules(string $name) : array
    {
        return self::$rules[$name];
    }

    /**
     * @return array
     */
    protected function ruleAny() : array
    {
        return [
            'name' => 'required|unique:departments|regex:#[а-яa-z0-9 -_\.,]+#i',
        ];
    }
}