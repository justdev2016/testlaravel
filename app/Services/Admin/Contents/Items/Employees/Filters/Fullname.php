<?php

namespace App\Services\Admin\Contents\Items\Employees\Filters;

use App\Contracts\Admin\FilterContract;
use Illuminate\Database\Eloquent\Builder;

class Fullname implements FilterContract
{

    /**
     * Apply a given search value to the builder instance.
     *
     * @param Builder $builder
     * @param mixed $value
     * @return Builder $builder
     */
    public static function apply(Builder $builder, $value)
    {
        return $builder
            ->where(function($q) use ($value) {
                $q->orWhere('last_name', 'LIKE', '%' . $value . '%')
                    ->orWhere('first_name', 'LIKE', '%' . $value . '%')
                    ->orWhere('surname', 'LIKE', '%' . $value . '%');
            });
    }
}