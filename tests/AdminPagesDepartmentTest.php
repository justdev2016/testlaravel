<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPagesDepartmentTest extends TestCase
{
// INDEX

    public function login()
    {
        $user = \App\User::find(1);
        $this->be($user);
    }

    public function testDepartmentIndexSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.departments.index'));
        $this->assertResponseOk();
    }

// CREATE

    public function testDepartmentCreateSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.departments.create'));
        $this->assertResponseOk();
    }

// UPDATE

    public function testDepartmentUpdateSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.departments.edit', ['departments' => 1]));
        $this->assertResponseOk();
    }

// READ

    public function testDepartmentReadSuccess()
    {
        $this->login();
        $this->call('GET', route('admin.departments.show', ['departments' => 1]));
        $this->assertResponseOk();
    }

// 404

    public function testDepartmentUpdate404()
    {
        $this->login();
        $this->call('GET', route('admin.departments.edit', ['departments' => 999]));
        $this->assertResponseStatus(404);
    }
}