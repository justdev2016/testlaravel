@javascript(['dataTableSettings' => $settings->get('javascript')])
@javascript(['sortable' => $settings->get('sortable')])
@javascript(['sync' => $settings->get('sync')])