<?php

namespace App\Services\Admin\Contents\Core\Components\Form;

/**
 * Базовый класс для компонентов типа `Form`
 */
class BaseForm
{
    /** @var string (required) */
    const TYPE = 'form';

    /** @var string */
    const BASE_VIEW = 'admin.components';

    /** @var array (required) */
    const VIEW_COMPONENTS = [
        'button',
        'cancel',
        'checkbox',
        'file',
        'label',
        'open',
        'radio',
        'required',
        'select',
        'submit',
        'text',
    ];

    public function __construct()
    {
        //
    }

    /**
     * @param  string $param
     * @return string
     */
    protected function getViewComponents(string $param) : string
    {
        return implode('.', [
            self::BASE_VIEW, // admin.components.
            'form.components', // form.components
            $param // submit
        ]);
    }
}