<?php

namespace App\Providers\Admin\Contents\Employees;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Support\Menu\Item;
use App\Validators\AdminEmployeeValidator;
use App\Services\Admin\AdminEmployeeService;
use App\Contracts\Admin\ContentEntryContract;
use App\Services\Admin\Contents\Core\Support\ContentRegistry;
use App\Services\Admin\Contents\Items\Employees\Support\Entry;

class AdminEmployeeServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('employees', \App\Employee::class);
        /** `Menu`.[`Contents`].`Employee` register */
        if ($this->app['config']->get('employees.active', false)) {
            $this->registerEmployeeMenu();
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();
        $this->registerValidator();
        $this->registerSettings();
    }

    /**
     * @return void
     */
    protected function registerEmployeeMenu()
    {
        if ($this->app->resolved('components.menu')) {
            $contents = $this->app['components.menu.childrens.contents'];

            $employee = $this->app['menu']->item($this->app['config']['employees.menu']);

            $contents->add($employee);

            $this->app['menu']->add($contents);
        }
    }

    /**
     * @return void
     */
    private function registerService()
    {
        $this->app->singleton(AdminEmployeeService::class, function ($app) {
            return new AdminEmployeeService($app);
        });

        $this->app->singleton(Entry::class, function($app) {
            return new Entry($app['config']['employees.actions']);
        });

        $this->app->bind(ContentEntryContract::class, Entry::class);
    }

    /**
     * @return void
     */
    private function registerValidator()
    {
        $this->app->bind(AdminEmployeeValidator::class, function ($app) {
            return new AdminEmployeeValidator;
        });
    }

    /**
     * @return void
     */
    private function registerSettings()
    {
        $registry = $this->app[ContentRegistry::class];

        $registry->set(ContentRegistry::SETTINGS, ['employees' => $this->app['config']['employees.settings']]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            AdminEmployeeService::class,
            AdminEmployeeValidator::class
        ];
    }
}