import { isSuccess } from "../../functions/helpers"
import { notification } from "../../functions/notification"

let href

// Modal Delete
$('.destroy-item, .delete').on('click', (e) => {
    e.preventDefault()

    href = $(e.currentTarget).attr('href')

    $('.delete-employees-modal').modal('show')
})

$('#delete-button').on('click', (e) => {
    e.preventDefault()

    $.ajax({
        url: href,
        type: 'DELETE',
        dataType: 'json'
    })
    .done((response) => {
        if (isSuccess(response)) {
            window.location.href = '/admin/employees'
        }
    })
    .fail((errors) => {
        notification('error', errors.responseText)
    })
    .always(() => {
        $('.delete-employees-modal').modal('hide')
    })
})
