@php
    $name = "admin.api.{$type}.{$action}";

    $settings = $settings->merge(
        [
            'role'            => 'form',
            'ref'             => 'form',
            '@submit.prevent' => 'save',
            'autocomplete'    => 'off',
            'route'           => isset($routeParam) ? [$name, $routeParam] : $name,
            'data-item'       => isset($data_item) ? $data_item : false,
            'data-dep'        => isset($data_dep) ? $data_dep : false,
        ]
    );
@endphp

{!!
    Form::open($settings->all())
!!}