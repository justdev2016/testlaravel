<div class="box box-solid">
    <div class="box-header with-border">
        <i class="fa fa-text-width"></i>
        <h3 class="box-title">
            {{-- {{ $page->title('description.learn') }} --}}
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <dl class="dl-horizontal">
            @foreach($component->columns('base') as $column)
                <dt>{{ $column['text'] }}</dt>
                <dd>
                    {{ $component->data('base')[$column['field']] }}
                </dd>
            @endforeach
        </dl>
    </div><!-- /.box-body -->
</div>

@if (! $component->actions()->isEmpty())
    @include($component->view('links-as-buttons'), [
        'routeParam' => $component->data('base.id'),
        'actions'    => $component->actions(),
        'data'       => $component->data('actions'),
        'routes'     => $component->routes()
    ])
@endif

@if ($component->actions()->has('delete'))
    {{-- @todo оптимизировать view --}}
    @include($page->view("views.delete-{$page->type()}-modal"))
@endif