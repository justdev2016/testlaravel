<?php

namespace App\Providers\Admin\Contents;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Support\Content\Contents;
use App\Services\Admin\Contents\Core\Defaults\Page;
use App\Services\Admin\Contents\Core\Support\{ComponentRegistry,ContentRegistry};
use App\Providers\Admin\Contents\{
    Employees\AdminEmployeeServiceProvider,
    Departments\AdminDepartmentServiceProvider
};

class AdminContentsServiceProvider extends ServiceProvider
{
    /** @var array */
    const PROVIDERS = [
        AdminEmployeeServiceProvider::class,
        AdminDepartmentServiceProvider::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @param  \RIlluminate\Routing\Router$router
     * @return void
     */
    public function boot()
    {
        /** `Global`.`Widget`.`Content` register */
        $this->registerGlobalWidget();
        $this->bootViewComposerContent();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBaseLogic();
        $this->registerProviders();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            ComponentRegistry::class, ContentRegistry::class,
            'contents','page',
        ];
    }

    /**
     * @return void
     */
    private function registerBaseLogic()
    {
        $this->app->singleton(ContentRegistry::class, function ($app) {
            return new ContentRegistry;
        });

        $this->app->singleton(ComponentRegistry::class, function ($app) {
            return new ComponentRegistry;
        });

        $this->app->singleton('contents', function ($app) {
            return new Contents($app[ComponentRegistry::class], $app['document.section']['content']);
        });

        $this->app->singleton('page', function ($app) {
            return new Page($app[ContentRegistry::class]);
        });
    }

    /**
     * @return void
     */
    private function registerProviders()
    {
        foreach (self::PROVIDERS as $provider) {
            $this->app->register($provider);
        }
    }

    private function registerGlobalWidget()
    {
        $mapper = $this->app['widgetMapper'];

        if ($mapper->has('content')) {
            $section = $mapper->section('global', 'content')->keys()->first();

            $content = [
                'name' => 'content',
                'item' => $this->app['contents']
            ];

            $contentItem = $this->app['document']->addItem($content);

            $this->app['document']->addToSection($section, $contentItem);
        }
    }

    /**
     * Регистрация View Composer `Content`
     * (глобальные данные для views)
     *
     * @return void
     */
    private function bootViewComposerContent()
    {
        view()->composer('*', function ($view) {
            $mapper = $this->app['widgetMapper'];

            if ($mapper->has('content')) {
                $mapper->make($view)->compose('content', $this->app['contents']);
                $mapper->make($view)->compose('page', $this->app['contents']->all());
            }
        });
    }
}
