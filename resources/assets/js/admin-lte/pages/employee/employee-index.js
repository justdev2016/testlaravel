import Vue from 'vue'
import VueResource from 'vue-resource'
import { unset, merge } from 'lodash'
import AppPagination from '../../components/app-pagination.vue'
import { notification } from '../functions/notification'

Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content

new Vue({
    el: "#app",
    components: {
        AppPagination
    },
    data: {
        raw: {},
        url: '',
        index: '',
        fields: ['id', 'full_name', 'phone', 'email', 'departments_list'],
        form: {
            department: 0,
            email: '',
            fullname: '',
            phone: '',
        },
        departments: { 0: '' }
    },
    computed: {
        pagination () {
            let data = this.raw

            unset(this.data, 'data')

            return data
        },
        contents () {
            return this.raw.data
        }
    },
    methods: {
        del (index, e) {
            this.url = $(e.currentTarget).attr('href')

            this.index = index

            $('.delete-employee-modal').modal('show')
        },
        destroy (e) {
            this.$http.delete(this.url)
                .then(({data}) => {
                    $('.delete-employee-modal').modal('hide')

                    console.log(this.raw)

                    this.raw.data.splice(this.index, 1)

                    notification('success', data.message)
                }, (error) => {})
        },
        paginate (page) {
            let params = {}

            params.params = this.getForm()
            params.params.page = page

            this.search(params)
        },
        filter () {
            let params = {}

            params.params = this.getForm()

            this.search(params)
        },
        search (params) {
            this.$http.get('/admin/api/employees/filter', params)
                .then(({data}) => {
                    this.raw = data.data
                }, (error) => {})
        },
        reset () {
            this.form = {
                department: 0,
                email: '',
                fullname: '',
                phone: '',
            }

            this.page = null

            this.init()
        },
        init () {
            this.$http.get(sync)
            .then(({data}) => {
                this.raw = data.data
            }, (error) => {})
        },
        getForm () {
            let request = this.form

            request.begin = $(this.$refs.begin).val()
            request.end   = $(this.$refs.end).val()

            return request
        }
    },
    mounted () {
        this.init()

        this.$http.get('/admin/api/departments/list')
            .then(({data}) => {
                this.departments = merge(this.departments, data)
            }, (error) => {})

        $('#datetimepicker1, #datetimepicker2').datetimepicker({
            format: 'YYYY-MM-DD'
        })
    }
})
