<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Managers\Notify\AdminNotificationManager;

class AdminNotificationServiceProvider extends ServiceProvider
{
     /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;
    
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('notify', function ($app) {
            return new AdminNotificationManager;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['notify'];
    }
}
