<?php

namespace App\Services\Admin\Contents\Core\Support;

/**
 * Logic for SPA Facade
 */
class Spa
{
    /** @var \Illuminate\Support\Collection */
    private $config;

    /**
     * @param bool $config
     */
    public function __construct(bool $config)
    {
        $this->config = collect(['spa' => $config]);
    }

    /**
     * @return bool
     */
    public function has() : bool
    {
        return $this->config->get('spa');
    }
}