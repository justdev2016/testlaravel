import { isSuccess } from "./helpers"

// ajax get course list
export function courseList() {
    $.ajax({
        url: '/admin/courses/ajax/list',
        dataType: 'json',
        cache: false
    })
    .done((response) => {
        if (isSuccess(response)) {
            // Select2
            let select2Options = {
                data: response.data
            }

            // Global `locale` (app.blade.php)
            if (locale != 'en') {
                select2Options.language = locale
            }

            $('#pack-courses-list').select2(select2Options)
        }
    })
    .fail((errors) => {})
    .always(() => {})
}