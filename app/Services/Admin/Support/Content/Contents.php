<?php

namespace App\Services\Admin\Support\Content;

use App\Contracts\Admin\RegistryContract;
use App\Services\Admin\Support\Component;

/**
 * @todo [описание]
 */
class Contents
{
    /** @var string */
    const VIEW_PATH = 'admin.layouts.section.content.';

    /** @var string */
    const VIEW_INDEX = 'index';

    /** @var string */
    const VIEW_PREFIX = 'admin.layouts.includes.';

    /** @var array */
    const VIEW_COMPONENTS = [
        'messages' => 'index',
    ];

    /** @var \App\Contracts\Admin\RegistryContract */
    private $registry;

    /** @var \App\Services\Admin\Document\DocumentComposite */
    private $section;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    /**
     * @param \App\Contracts\Admin\RegistryContract $registry
     * @param \App\Services\Admin\Support\Component $section
     */
    public function __construct(RegistryContract $registry, Component $section)
    {
        $this->registry = $registry;
        $this->section  = $section;
        $this->settings = collect();

        $this->make();
    }

    private function make()
    {
        $this->makeView();
    }

    /**
     * @return void
     */
    private function makeView()
    {
        $folder = self::VIEW_PATH;
        $prefix = self::VIEW_PREFIX;

        foreach (self::VIEW_COMPONENTS as $component => $index) {
            $path = is_string($component) ? "{$component}.{$index}" : $index;

            $views['components'][$component] = "{$prefix}{$path}";
        }

        $views['index'] = $folder . self::VIEW_INDEX;

        $this->settings->put('views', $views);
    }

    /**
     * @param  string|null $name
     * @return string
     */
    public function view(string $component = null)
    {
        return ! $component
            ? $this->settings->getData('views.index')
            : $this->settings->getData("views.components.{$component}");
    }

    /**
     * @param  mixed $components
     * @return void
     */
    public function add($components)
    {
        $this->registry->set($this->registry::GLOBAL, $components);
    }

    /**
     * Вернуть все данные
     *
     * @return mixed
     */
    public function all()
    {
        return ! $this->isEmpty() ? $this->registry->get($this->registry::GLOBAL) : collect();
    }

    /**
     * @return string
     */
    public function render() : string
    {
        return view($this->settings->getData('views.index'))->render();
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool
    {
        return $this->registry->isEmpty();
    }

    /**
     * @param  string $name
     * @return bool
     * @todo уникальные имена для `global`|`local`!?!
     */
    public function has(string $name) : bool
    {
        $global = $this->section('global', $name);
        $local  = $this->section('local', $name);

        return ! $global->isEmpty() || ! $local->isEmpty();
    }

    /**
     * @param  string $name
     * @param  mixed $value
     * @return \Illuminate\View\View
     */
    public function compose(string $name, $value) : View
    {
        return $this->view->with($name, $value);
    }
}