<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Services\Admin\Contents\Core\Api;
use App\Http\Requests\Employee\EmployeeRequest;
use App\Services\Admin\Contents\Items\Employees\EmployeesSearch;
use App\Services\Admin\Contents\Items\Employees\Web\Index\IndexPresenter;

class EmployeeApiResourceController extends Controller
{
    /** @var \App\Services\Admin\Contents\Core\Api */
    protected $api;

    public function __construct(Api $api)
    {
        $this->middleware('check-ajax', [
            'only' => [
                'destroy',
                'filter',
                'index',
                'store',
                'update',
            ]
        ]);

        $this->api = $api;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        return $this->api->employees->index->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Employee\EmployeeRequest $request
     * @return  \Illuminate\Http\JsonResponse
     */
    public function store(EmployeeRequest $request) : JsonResponse
    {
        return $this->api->employees->store->response();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Employee\EmployeeRequest  $request
     * @param  Employee $employee
     * @return  \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        return $this->api->employees->update->response();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Employee $employee
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Employee $employee) : JsonResponse
    {
        return $this->api->employees->destroy->response();
    }

    /**
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filter(Request $request) : JsonResponse
    {
        $result = EmployeesSearch::apply($request);

        $newCollection = $result->getCollection()->present(IndexPresenter::class);

        $result->setCollection($newCollection);

        return response()->json(['data' => $result]);
    }
}
