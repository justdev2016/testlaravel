@php
    $multi = $settings['multi'] ?? false;
@endphp

<div class="row">
    <div class="form-group">
        <div class="col-md-7 country-block">
            <h2>
                @lang('countries.countries')
                @if ($multi)
                    <button type="button" class="btn btn-success" id="create-country">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                @endif
            </h2>
        </div><!-- /.col-md -->
    </div><!-- /.form-group -->
</div><!-- /.row -->