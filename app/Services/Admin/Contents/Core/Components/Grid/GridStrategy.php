<?php

namespace App\Services\Admin\Contents\Core\Components\Grid;

use App\Services\Admin\Contents\Core\Components\ComponentDependence;

interface GridStrategy
{
    public function make(ComponentDependence $data, ComponentDependence $settings);
}