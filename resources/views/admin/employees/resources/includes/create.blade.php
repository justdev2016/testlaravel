@foreach($components as $component)
    @if ($component->hasInfo())
        @include($page->view('info.warning'), $component->info('warning'))
    @endif

    @include($component->view('views.index2'))
@endforeach