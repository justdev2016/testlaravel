<?php

namespace App\Services\Admin\Contents\Core\Defaults;

use App\Contracts\Admin\RegistryContract;

/**
 * Logic for Page Facade
 *
 * Интерфейс управления загрузкой
 */
class Page extends Defaults
{
    /** @var \App\Contracts\Admin\RegistryContract */
    private $registry;

    /**
     * @param \App\Contracts\Admin\RegistryContract $registry
     */
    public function __construct(RegistryContract $registry)
    {
        $this->registry = $registry;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->defaults = collect($this->registry->get($this->registry::SETTINGS));
    }

    /**
     * Получение экземпляра класса `Content`
     *
     * @param  string $content
     * @param  string $action
     * @return object
     */
    public function instance(string $content, string $action)
    {
        $class = $this->defaults->getData("{$content}.{$action}");

        return app($class);
    }
}