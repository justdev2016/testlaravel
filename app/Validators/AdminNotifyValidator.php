<?php

namespace App\Validators;

use Notify;

/**
 * Валидатор для работы с файлами
 */
class AdminNotifyValidator extends BaseValidator
{
    /** @var string */
    const RULE_ANY = 'any';

    /** @var string */
    const RULE_ERROR = 'error';

    /** @var string */
    const RULE_RAW_ANY = 'rawAny';

    /**
     * @var array Правила валидации
     */
    protected static $rules;

    /** @var array */
    protected $status;

    public function __construct()
    {
        $this->status = Notify::getAllStatus()->implode(',');

        self::$rules = [
            self::RULE_ANY => $this->ruleAny(),
            self::RULE_RAW_ANY => $this->ruleRawAny(),
            self::RULE_ERROR => $this->ruleError(),
        ];
    }

    /**
     * Возвращает правило валидации по его имени
     *
     * @param  string $name
     * @return array
     */
    public function getRules(string $name) : array
    {
        return self::$rules[$name];
    }

    /**
     * @return array
     */
    protected function ruleAny() : array
    {
        return [
            'message' => 'string',
            'status'  => "string|in:{$this->status}",
        ];
    }

    /**
     * @return array
     */
    protected function ruleRawAny() : array
    {
        return [
            'message' => 'string',
            'status'  => "string|in:{$this->status}",
        ];
    }

    /**
     * @return array
     */
    protected function ruleError() : array
    {
        return [
            'raw' => 'required|is_exception',
        ];
    }
}