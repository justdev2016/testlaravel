<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Edit;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Items\Departments\Settings\ContentSettings;
use App\Services\Admin\Contents\Core\Settings\{WebAction,WebConfigAction};

/**
 * Логика формирования данных для страницы `EDIT`
 *
 * @todo доделать multi-компонентность на странице
 * @todo DRY
 */
class Edit implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var array */
    protected $config;

    /**
     * @param \App\Services\Admin\Contents\Core\Support\Input $params
     */
    public function __construct(Input $params)
    {
        $this->config = [
            'settings'  => new ContentSettings('web'),
            'namespace' => __NAMESPACE__,
            'config'    => collect(config('departments.components.edit')),
            'requests'  => $params,
        ];
    }

    /**
     * @return self
     */
    public function init()
    {
        $this->object = new WebConfigAction($this->config);
        $this->object = new EditConfigAction($this->object);
        $this->object = new WebAction($this->object);

        return $this;
    }

    /**
     * @return array
     */
    public function action() : array
    {
        return $this->object->action();
    }
}