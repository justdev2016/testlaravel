{{-- Text --}}
@php
    $attributes = array_merge(
        ['class' => 'form-control'],
        ['id' => $data['field']]
    );

    $label = [
        'name'  => $data['field'],
        'value' => $data['text']
    ];

    $text = [
        'name'       => $data['field'],
        'value'      => $value ?? null,
        'attributes' => $attributes
    ];
@endphp

<div class="form-group">
    @include($component->viewComponent('label'), $label)

    @if($settings->get('required', false))
        @include($component->viewComponent('required'))
    @endif

    @include($component->viewComponent('text'), $text)
</div><!-- /.form-group -->