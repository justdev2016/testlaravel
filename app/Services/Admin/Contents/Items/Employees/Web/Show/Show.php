<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Show;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;
use App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings;
use App\Services\Admin\Contents\Core\Settings\{WebAction,WebConfigAction};

/**
 * Логика формирования данных для страницы `SHOW`
 *
 * @todo доделать multi-компонентность на странице
 */
class Show implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var array */
    protected $config;

    public function __construct(Input $params)
    {
        $this->config = [
            'settings'  => new ContentSettings('web'),
            'namespace' => __NAMESPACE__,
            'config'    => collect(config('employees.components.show')),
            'requests'  => $params,
        ];
    }

    /**
     * @return self
     */
    public function init()
    {
        $this->object = new WebConfigAction($this->config);
        $this->object = new ShowConfigAction($this->object);
        $this->object = new WebAction($this->object);

        return $this;
    }

    /**
     * @return array
     */
    public function action() : array
    {
        return $this->object->action();
    }
}