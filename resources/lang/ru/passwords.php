<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть не менее 6 символов в длину и совпадать с подтверждением',
    'reset'    => 'Ваш пароль был успешно изменён!',
    'sent'     => 'На вашу почту отправлено письмо с данными для сброса пароля!',
    'token'    => 'Токен сброса пароля устарел, или неверный!',
    'user'     => 'Пользователь с указанным e-mail не найден',
];
