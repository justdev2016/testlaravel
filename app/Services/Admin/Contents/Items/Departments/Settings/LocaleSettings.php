<?php

namespace App\Services\Admin\Contents\Items\Departments\Settings;

use App\Services\Admin\Contents\Core\Settings\BaseSettings;

/**
 * Настройки источников локализированных данных
 */
class LocaleSettings extends BaseSettings
{
    /** @var string */
    const LOCALE = 'departments';

    /** @var string */
    const DEFAULT = 'message';

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var array */
    const METHODS = ['settings'];

    public function __construct()
    {
        $settings = [
            'main'    => self::LOCALE,
            'default' => self::DEFAULT,
        ];

        $this->settings = collect($settings);
    }

    public function __get($name)
    {
        if (! in_array($name, self::METHODS)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return $this->{$name};
    }
}