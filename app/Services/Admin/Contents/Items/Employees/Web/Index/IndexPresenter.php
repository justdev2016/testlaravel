<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Index;

use App\Services\Admin\Contents\Core\Support\Presenter;

class IndexPresenter extends Presenter
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'full_name', 'phone', 'email', 'departments_list'];

    public function getFullNameAttribute()
    {
        return trim($this->model->last_name . ' ' . $this->model->first_name . ' ' . $this->model->surname);
    }

    public function getDepartmentsListAttribute()
    {
        return $this->model->departments()->lists('name')->implode(', ');
    }
}