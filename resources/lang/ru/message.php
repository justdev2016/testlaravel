<?php

return [
    // documents
    'search'        => 'Поиск',// @todo удалить или отображать для admin
    'level'         => 'Уровень',// breadcrumb ??
    'here'          => 'Here',// breadcrumb ??
    'homepage'      => 'Главная страница',
    'home'          => 'Главная',
    'createdby'     => 'Сделано',
    'seecode'       => 'Смотри код на ',
    'tabmessages'   => 'У Вас :count сообщения(й)',
    'supteam'       => 'Команда поддержки',
    'notifications' => 'У Вас есть :count уведомление(й)',
    'viewall'       => 'Посмотреть все',
    'profile'       => 'Профиль',
    'signout'       => 'Выход',
    'created_at'    => 'Создан',
    'updated_at'    => 'Изменен',
    'begin'         => 'Дата приема (От)',
    'end'           => 'Дата приема (До)',
    'find'          => 'Найти',
    'fullname'      => 'ФИО',
    'phone'         => 'Телефон',
    'department'    => 'Отдел',
    'email'         => 'Email',
    'reset'         => 'Сбросить',
    // buttons
    'save'   => 'Сохранить',
    'cancel' => 'Отменить',
    // messages-success
    'order-updated-success' => 'Порядок сортировкви у данных обновлен!',
    // messages-error
    'no-such-file-error'             => 'Не удается найти файл :fileName!',
    'server-error'                   => 'Ошибка сервера!',
    'save-error-message'             => 'Невозможно сохранить документ!',
    'not-valid-image-criteria-error' => 'Фото не удовлетворяет необходимым размерам!',
    'files-upload-error-message'     => 'Не все файлы полностью загрузились на сервер! Повторите попытку!',
    'incorrect-parameters-request'   => 'Параметры запроса неверные!',
    'no-zip-error'                   => 'Невозможно создать архив!',
    'no-unzip-error'                 => 'Невозможно распаковать архив!',
    'no-profile-info'                => 'Профиль отчета с таким именем не найден!',
    'not-found-attribute'            => 'Атрибут не найден!',
    'page-not-found'                 => 'Страница не найден!',
    'action-not-found'               => 'Действие не найдено!',
    'no-delete-file-or-directory'    => 'Невозможно удалить файл(-ы)/каталог(-и)!',
    'method-not-exist'               => 'Вызываемый метод не поддерживает данное действие!',
    'view-not-found'                 => 'Файл представления не найден в системе!',
    'not-found-page-instance'        => 'Не найден класс реализации логигки страницы!', // проконтролировать, возможно придется удалить!!
    // messages-warn
    'required-message'                 => 'Обязательно для заполнения!',
    'access-or-operation-denied'       => 'Доступ или операция запрещены!',
    // messages-info
    'no-data'           => 'Для данной страницы нет данных!',
    // table head
    'id'          => 'Идентификатор',
    // messages
];
