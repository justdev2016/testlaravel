<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Update;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Getter,Setter,Content};
use App\Services\Admin\Managers\Notify\SuccessOptions;

/**
 * Выполнение действия для `Ajax`.`Update`
 */
class UpdateAction implements ActionContract
{
    use Getter, Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $this->settings = $this->object->action();

        $request    = $this->settings->get('requests')->request();
        $mainSetter = $this->setter('main','update');

        // обновить данные модели
        $employee = $this->content($mainSetter, ['requests' => $this->settings->get('requests')]);

        // обновление зависимостей
        foreach ($this->settings->get('relations', []) as $relation) {
            if ($request->has($relation)) {
                // @todo что, если не будет `getter` для текущей связи, просто создать запись?
                if (! $this->getter("dependencies.{$relation}")->isEmpty()) {
                    // получить `models` зависимостей по `id`, если нужно (только для `countries`)
                    $_getter = $this->getter("dependencies.{$relation}",'primary-by');
                    $ids     = $this->content($_getter, ['id' => $request->get($relation)], $relation);

                    // сохранить новый данные
                    $params = ['ids' => $ids->toArray(), 'relation' => $relation, 'model' => $employee];
                    $_setter = $this->setter("dependencies.{$relation}",'sync');
                    $this->content($_setter, ['options' => $params]);
                }
            } else {
                // удалить все данные в связанной таблице
                $_getter = $this->setter('main',"{$relation}-clean");
                $this->content($_getter, ['model' => $employee]);
            }
        }

        $success = ['message' => trans('employees.update-success-message'), 'data' => $employee];
        $notify  = Notify::success($success);

        $notify->withSession($notify->except('data')->toArray());

        return $notify;
    }
}