<?php

use App\Employee;
use Illuminate\Support\Collection;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CollectionMacrosTest extends TestCase
{
    const DATA_ARRAY = [
        'test' => [
            'testKey' => 'testValue',
        ],
        'test_test' => 'string',
    ];

    const DATA_R_SEARCH_TRUE_MANY = [
        'data' => [
            'required' => true,
        ],
        'required' => true,
    ];

    const DATA_R_SEARCH_TRUE_ONE = [
        'data' => [
            'required' => true,
        ],
    ];

    const DATA_R_SEARCH_FALSE = [
        'data' => [
            'key' => 'value',
        ],
    ];

    const DATA_ACTIVE_TRUE = [
        'first'  => true,
        'second' => false,
    ];

    const DATA_ACTIVE_FALSE = [
        'first'  => false,
        'second' => false,
    ];

    const DATA_MENU = [
        'url'  => 'admin.pages.index',
        'text' => 'menu.dashboard',
    ];

    const DATA_ATTRIBUTES = [
        'id'    => 'test',
        'class' => 'test',
    ];

    const DATA_FIELD = [
        'text' => [
            'name'  => 'title',
            'label' => 'test'
        ]
    ];

    /**
     * \App\Employee
     */
    protected $employee;

    /**
     * onlyWithPivot() -> УДАЛИТЬ!
     * hasContent() ->NOT USED!
     * ArchiveInfo:
     *     path()
     *     file()
     * MoveInfo:
     *    path()
     *    file()
     */

// getData()

    /**
     * Имитация Model Find
     */
    protected function mockEmployee()
    {
        $datetime = '1970-01-01 12:00:00';

        $employee = Mockery::mock(Employee::class);
        $employee->shouldReceive('find')->with(1)->once()->andReturn(['updated_at' => $datetime]);

        $this->employee = $employee;
    }

    /**
     * Имитация поиска по модели `Employee`
     *
     * @return array
     */
    protected function findAsArray()
    {
        $this->mockEmployee();

        return $this->employee->find(1);
    }

    public function testCollectionGetDataString()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->getData('test.testKey');

        $this->assertEquals('testValue', $result);
    }

    public function testCollectionGetDataDefault()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->getData('test.test', 'default');

        $this->assertEquals('default', $result);
    }

    public function testCollectionGetDataStringAsCollect()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->getData('test.testKey.*');

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertEquals(['testValue'], $result->all());
    }

    public function testCollectionGetDataStringAsCollectDefault()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->getData('test.test.*', 'default');

        $this->assertEquals('default', $result);
    }

    public function testCollectionGetDataAsCollect()
    {
        $collect = collect(self::DATA_ARRAY);

        $result_1 = $collect->getData('test');
        $result_2 = $collect->getData('test.*');

        $this->assertInstanceOf(Collection::class, $result_1);
        $this->assertInstanceOf(Collection::class, $result_2);
        $this->assertEquals(['testKey' => 'testValue'], $result_1->all());
        $this->assertEquals(['testKey' => 'testValue'], $result_2->all());
    }

// content()

    public function testCollectionContentDefault()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->content('test.test', 'default');

        $this->assertEquals('default', $result);
    }

    public function testCollectionContentString()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->content('test.testKey');

        $this->assertEquals('testValue', $result);
    }

    public function testCollectionContentStringAsCollect()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->content('test.testKey.*');

        $this->assertInstanceOf(Collection::class, $result);
        $this->assertEquals(['testValue'], $result->all());
    }

    public function testCollectionContentStringAsCollectDefault()
    {
        $collect = collect(self::DATA_ARRAY);

        $result = $collect->content('test.test.*', 'default');

        $this->assertEquals('default', $result);
    }

    public function testCollectionContentAsCollect()
    {
        $collect = collect(self::DATA_ARRAY);

        $result_1 = $collect->content('test');
        $result_2 = $collect->content('test.*');

        $this->assertInternalType('array', $result_1);
        $this->assertInstanceOf(Collection::class, $result_2);
        $this->assertEquals(['testKey' => 'testValue'], $result_1);
        $this->assertEquals(['testKey' => 'testValue'], $result_2->all());
    }

// @todo present()

// r_search()

    public function testCollectionRSearchTrue()
    {
        $one  = collect(self::DATA_R_SEARCH_TRUE_ONE);
        $many = collect(self::DATA_R_SEARCH_TRUE_MANY);

        $result_one  = $one->r_search('required');
        $result_many = $many->r_search('required');

        $this->assertNotFalse($result_one);
        $this->assertNotFalse($result_many);
    }

    public function testCollectionRSearchFalse()
    {
        $false = collect(self::DATA_R_SEARCH_FALSE);

        $result_false = $false->r_search('required');

        $this->assertFalse($result_false);
    }

// key()

    public function testCollectionKey()
    {
        $key = $raw = collect(self::DATA_ARRAY);

        $key = $key->map(function($item) {
            return collect($item);
        });

        $result_key_true = $key->key();
        $result_key_array_true = $raw->key();

        $this->assertEquals('test', $result_key_true);
        $this->assertEquals('test', $result_key_array_true);
    }

// active()

    public function testCollectionActive()
    {
        $active_true  = collect(self::DATA_ACTIVE_TRUE);
        $active_false = collect(self::DATA_ACTIVE_FALSE);

        $result_true  = $active_true->active();
        $result_false = $active_false->active();

        $this->assertInstanceOf(Collection::class, $result_true);
        $this->assertInstanceOf(Collection::class, $result_false);

        $this->assertEquals(['first' => true], $result_true->all());
        $this->assertEquals([], $result_false->all());
    }

// route()

    public function testCollectionRoute()
    {
        $route = collect(self::DATA_MENU);

        $result = $route->route('url');

        $this->assertEquals(route('admin.pages.index'), $result->get('url'));
    }

// trans()

    public function testCollectionTrans()
    {
        $trans = collect(self::DATA_MENU);

        $result = $trans->trans('text');

        $this->assertEquals(trans('menu.dashboard'), $result->get('text'));
    }

// menu()

    public function testCollectionMenu()
    {
        $menu = collect(self::DATA_MENU);

        $result = $menu->menu();

        $this->assertArraySubset(['url' => route('admin.pages.index'), 'text' => trans('menu.dashboard')], $result);
    }

// addClass()

    public function testCollectionAddClass()
    {
        $addClass_with_class    = collect(self::DATA_ATTRIBUTES);
        $addClass_without_class = collect(self::DATA_ATTRIBUTES)->forget('class');

        $result_after  = $addClass_with_class->addClass('test2');
        $result_before = $addClass_with_class->addClass('test2', false);
        $result_new    = $addClass_without_class->addClass('test3');

        $this->assertEquals('test2 test', $result_before->get('class'));
        $this->assertEquals('test test2', $result_after->get('class'));
        $this->assertEquals('test3', $result_new->get('class'));
    }

// dateFormat()

    public function testCollectionDateFormat()
    {
        $employee = collect($this->findAsArray());

        $result_small_ru = $employee->dateFormat('small');
        $result_full_ru  = $employee->dateFormat('full');

        $this->assertEquals('01.01.1970 12:01', $result_small_ru->first());
        $this->assertEquals('01.01.1970 12:01:00', $result_full_ru->first());
    }

// columns()

    public function testCollectionColumns()
    {
        $columns = collect(['id','slug']);

        $test = [
            [
                'field' => 'id',
                'text'  => 'id'
            ],
            [
                'field' => 'slug',
                'text'  => 'slug'
            ]
        ];

        $result = $columns->columns();

        $this->assertArraySubset($test, $result->all());
    }

// instance()

    public function testCollectionInstance()
    {
        $instance = collect(['stdClass']);

        $result = $instance->instance()->first();

        $this->assertInstanceOf('stdClass', $result);
    }

// field()

    public function testCollectionField()
    {
        $field = collect(self::DATA_FIELD);

        $result_with_param    = $field->field('name');
        $result_without_param = $field->field();

        $this->assertEquals('title', $result_with_param);
        $this->assertEquals('text', $result_without_param);
    }

}