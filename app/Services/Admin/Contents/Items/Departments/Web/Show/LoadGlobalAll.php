<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Show;

use Components;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;
use App\Services\Admin\Contents\Items\Departments\Settings\ContentSettings;

class LoadGlobalAll
{
    /** @var \Illuminate\Support\Collection */
    private $requests;

    /** @var \Illuminate\Support\Collection */
    private $components;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(Input $requests, Collection $components, ContentSettings $settings)
    {
        $this->requests   = $requests;
        $this->components = $components;
        $this->settings   = $settings;
    }

    /**
     * @param  string $component
     * @return \Illuminate\Support\Collection
     */
    public function action(string $component) : Collection
    {
        return $this->bindData($this->makeData($component));
    }

    /**
     * @param  string $component
     * @return \Illuminate\Support\Collection
     */
    private function makeData(string $component) : Collection
    {
        $repository = $this->settings->content('repository.main')->setModel($this->requests->model('departments'));

        return $this->components->only($component)->map(function($item) use ($repository) {
            return $repository->show(ShowPresenter::class);
        });
    }

    /**
     * @param  \Illuminate\Support\Collection $data
     * @return \Illuminate\Support\Collection
     */
    private function bindData(Collection $data) : Collection
    {
        return $data->map(function($item, $type) {
            $driver    = $this->components->getData($type);

            $class     = Components::instance($type);
            $component = new $class($driver['driver']);

            $actions   = $driver->get('actions');

            $data = new ComponentDependence([
                'base'    => $item,
                'actions' => $actions
            ]);

            $settings = new ComponentDependence([
                'global' => $this->settings,
                'local'  => $driver
            ]);

            return $component->make($data, $settings);
        });
    }
}