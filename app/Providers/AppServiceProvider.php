<?php

namespace App\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

use App\Providers\Admin\AdminServiceProvider;
use App\Providers\Admin\Contents\AdminContentsServiceProvider;

use App\Providers\Repositories\{
    DepartmentRepositoryServiceProvider,
    EmployeeRepositoryServiceProvider
};

use App\Providers\Services\AdminNotificationServiceProvider;
use App\Providers\Services\AdminContentManagerServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /** Register Admin Kernel */
        $this->app->register(AdminServiceProvider::class);
        /** Register Admin Content */
        $this->app->register(AdminContentsServiceProvider::class);

        /* Support's service providers */
        $this->app->register(AdminNotificationServiceProvider::class);
        $this->app->register(AdminContentManagerServiceProvider::class);

        /* Repositories's service providers */
        $this->app->register(EmployeeRepositoryServiceProvider::class);
        $this->app->register(DepartmentRepositoryServiceProvider::class);

        $this->registerDebugBar();
    }

    /**
     * Условия регистрации пакета DebugBar
     *
     * @return void
     */
    protected function registerDebugBar()
    {
        if ($this->app->isLocal() && env('APP_DEBUG') && env('DEBUGBAR', false)) {
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);

            $loader = AliasLoader::getInstance();
            $loader->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }
    }
}
