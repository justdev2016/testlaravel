<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Store;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Getter,Setter,Content};

/**
 * Выполнение действия для `Ajax`.`Store`
 */
class StoreAction implements ActionContract
{
    use Getter, Setter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $this->settings = $this->object->action();
        $request    = collect($this->settings->get('requests')->request()->all());
        $mainSetter = $this->setter('main','store');

        // внести данные модели
        $employee = $this->content($mainSetter, ['request' => $request]);
        // dd($employee);
        foreach ($this->settings->get('relations', []) as $relation) {
            if ($request->has($relation)) {
                // @todo что, если не будет `getter` для текущей связи, просто создать запись?
                if (! $this->getter("dependencies.{$relation}")->isEmpty()) {
                    // получить `models` зависимостей по `id`, если нужно (только для `departments`)
                    $_getter = $this->getter("dependencies.{$relation}",'model-by-id');
                    $models  = $this->content($_getter, ['ids' => $request->get($relation)], $relation);

                    // сохранить новый данные
                    $params  = ['models' => $models, 'relation' => $relation, 'model' => $employee];
                    $_setter = $this->setter('main','saveMany');

                    $this->content($_setter, ['options' => $params]);
                }
            }
        }

        $success = ['message' => trans('employees.create-success-message'), 'data' => $employee];
        $notify  = Notify::success($success);

        $notify->withSession($notify->except('data')->toArray());

        return $notify;
    }
}