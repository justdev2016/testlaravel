function cancel(el, url) {
    el.on('click', (e) => {
        e.preventDefault()

        window.location.href = url
    })
}

export { cancel }