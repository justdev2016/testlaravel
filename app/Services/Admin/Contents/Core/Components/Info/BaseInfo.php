<?php

namespace App\Services\Admin\Contents\Core\Components\Info;

use Illuminate\Support\Collection;
use App\Validators\AdminInfoValidator;

/**
 * Базовый класс для компонентов типа `Info`
 */
class BaseInfo
{
    /** @var string (required) */
    const TYPE = 'info';

    /** @var string */
    const BASE_VIEW = 'admin.components';

    /** @var array (required) */
    const VIEW_COMPONENTS = [
        'links-as-buttons'
    ];

    /** @var \App\Validators\AdminInfoValidator */
    protected $validator;

    public function __construct()
    {
        $this->validator = app(AdminInfoValidator::class);
    }

    /**
     * Валидация данных
     *
     * @param  mixed $data
     * @param  string $rules
     * @return void
     */
    protected function validate($data, string $rules)
    {
        if ($data instanceof Collection) {
            $data = $data->toArray();
        }

        $this->validator->passesOrFail($data, $rules);
    }

    /**
     * @param  string $suffix
     * @param  string $param
     * @return string
     */
    protected function getViewComponents(string $suffix, string $param) : string
    {
        return implode('.', [
            self::BASE_VIEW, // admin.components.
            'info', // info
            $suffix, // adminLTE.includes
            $param // header
        ]);
    }
}