@foreach($data['table']['head'] as $column)
    @if($column == 'slug') @continue @endif
    <th>@lang('courses.' . $column)</th>
@endforeach
<th>@lang('actions.' . $data['table']['manage'])</th>