<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Store;

use Actions;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Выполнение инициализации настроек для `Web`.`Employees`.`Store`
 * @todo DRY - support methods
 */
class StoreConfigAction implements ActionContract
{
    /** @var array */
    const REPOSITORIES = [
        'main' => [
            'set' => [
                'store' => [
                    'method' => 'store',
                    'params' => 'request'
                ],
                'saveMany' => [
                    'method' => 'saveMany',
                    'params' => 'options',
                ]
            ],
        ],
        'dependencies' => [
            'departments' => [
                'get' => [
                    'model-by-id' => [
                        'method' => 'getDataById',
                        'params' => 'ids'
                    ]
                ],
            ],
        ],
    ];

    /** @var array */
    const RELATIONS = ['departments'];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $response->put('repository', self::REPOSITORIES);
        $response->put('relations', self::RELATIONS);

        return $response;
    }
}