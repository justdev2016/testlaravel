<thead>
    <tr role="row">
        @each($component->view('each_column'), $component->columns('base'), 'column')

        @if(! $component->columns('actions')->isEmpty())
            @each($component->view('each_column'), $component->columns('actions'), 'column')
        @endif
	</tr>
</thead>