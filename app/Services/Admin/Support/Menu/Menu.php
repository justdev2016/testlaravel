<?php

namespace App\Services\Admin\Support\Menu;

use Illuminate\Support\Collection;
use App\Services\Admin\Support\Component;
use App\Contracts\Admin\MenuComponentContract;

/**
 * Service for Facade `Menu`
 */
class Menu implements MenuComponentContract
{
    /** @var string */
    const VIEW_PATH = 'admin.components.menu.AdminLTE.';

    /** @var string */
    const VIEW_INDEX = 'index';

    /** @var string */
    const VIEW_SUFFIX = 'components.';

    /** @var array */
    const VIEW_COMPONENTS = [
        'menu',
        'menu-item',
        'menu-tree-item',
    ];

    /** @var MenuComposite */
    protected $root;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /**
     * @param \App\Services\Admin\Support\Component $component
     */
    public function __construct(Component $component)
    {
        $this->root     = $component;

        $this->settings = collect();

        $this->make();
    }

    private function make()
    {
        $this->makeView();
    }

    /**
     * @return void
     */
    private function makeView()
    {
        $folder = self::VIEW_PATH;
        $suffix = self::VIEW_SUFFIX;

        foreach (self::VIEW_COMPONENTS as $component) {
            $views['components'][$component] = "{$folder}{$suffix}{$component}";
        }

        $views['index'] = $folder . self::VIEW_INDEX;

        $this->settings->put('views', $views);
    }

    /**
     * @return MenuComposite
     */
    public function root() : MenuComposite
    {
        return $this->root;
    }

    public function add(Component $item)
    {
        $this->root->add($item);
    }

    /**
     * @param  array $data
     * @return Item
     */
    public function item(array $data) : Item
    {
        return new Item($data);
    }

    /**
     * @param  string $name
     * @return \App\Services\Admin\Support\Component
     */
    public function addSubMenu(string $name) : Component
    {
        return new MenuComposite($name);
    }

    /**
     * @param  string|null $name
     * @return string
     */
    public function view(string $component = null)
    {
        return ! $component
            ? $this->settings->getData('views.index')
            : $this->settings->getData("views.components.{$component}");
    }

    /**
     * Вернуть все данные
     *
     * @return \Illuminate\Support\Collection
     */
    private function all() : Collection
    {
        return collect($this->root->render());
    }

    /**
     * @return string
     */
    public function render() : string
    {
        return view($this->settings->getData('views.index'), ['menuData' => $this->all()])->render();
    }

    /**
     * @param  string $name
     * @return string
     */
    public function getSubMenuName(string $name) : string
    {
        return trans("menu.{$name}");
    }
}