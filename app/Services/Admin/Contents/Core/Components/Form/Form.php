<?php

namespace App\Services\Admin\Contents\Core\Components\Form;

use Components;
use App\Services\Admin\Contents\Core\Components\Form\FormStrategy;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Компонент `Form`
 */
class Form implements FormStrategy
{
    /** @var string */
    public $driver;

    /**
     * @param string $driver
     */
    public function __construct(string $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return object
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $class = Components::driver('form', $this->driver);

        return $class->make($data, $settings);
    }
}