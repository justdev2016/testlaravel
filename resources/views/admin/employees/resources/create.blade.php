@extends('admin.layouts.app')

@section('contentheader_title')
	{{ $page->title('employee') }}
@endsection

@section('htmlheader_title')
	{{ $page->title('employee') }}
@endsection

@section('main-content')
    <div class="box-header with-border">
		<h3 class="box-title">{{ $page->title('create-title') }}</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        @foreach ($page->components as $component)
            @each($page->view(), $component, 'components')
        @endforeach
    </div><!-- /.box-body -->
@endsection

@push('scripts')
    <script src="{{ asset("{$jsPath}/create-{$page->script()}") }}"></script>
@endpush