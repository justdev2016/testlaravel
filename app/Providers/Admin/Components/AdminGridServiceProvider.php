<?php

namespace App\Providers\Admin\Components;

use Illuminate\Support\ServiceProvider;

class AdminGridServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCollectionMacros();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerGridComponent();
    }

    /**
     * @return void
     */
    protected function registerGridComponent()
    {
        /** Register Facade `Grid` */
        $this->registerGridFacade();
    }

    /**
     * @return void
     */
    protected function registerGridFacade()
    {
        $this->app->singleton('grid', function ($app) {
            // return new Menu($app['components.menu']);
        });
    }

    /**
     * @return void
     */
    private function registerCollectionMacros()
    {
        //
    }
}
