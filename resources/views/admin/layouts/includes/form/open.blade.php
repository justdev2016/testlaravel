@php
  $route[] = $params['route'];
  if(isset($params['route-param'])) {
    $route[] = $params['route-param'];
  }
@endphp

{!!
  Form::open([
    'route' => $route,
    'autocomplete' => 'off',
    'role'  => 'form',
    'id'    => $params['id'],
    'files' => $params['files'] ?? false
  ])
!!}