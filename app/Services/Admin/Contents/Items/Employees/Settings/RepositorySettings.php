<?php

namespace App\Services\Admin\Contents\Items\Employees\Settings;

use Repository;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Settings\BaseSettings;
use App\Repositories\{
    DepartmentRepository,
    EmployeeRepository
};

/**
 * Настройки источников данных для всех страниц
 */
class RepositorySettings extends BaseSettings
{
    /** @var string */
    const MAIN = EmployeeRepository::class;

    /** @var array */
    const DEPENDENCIES = [
        'departments' => DepartmentRepository::class,
    ];

    /** @var \App\Repositories\EmployeeRepository */
    protected $settings;

    /** @var array */
    const METHODS = ['settings'];

    public function __construct()
    {
        $this->make();
    }

    private function make()
    {
        $settings = [
            'main'         => app(self::MAIN),
            'dependencies' => $this->makeDependencies()
        ];

        $this->settings = collect($settings);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function makeDependencies() : Collection
    {
        foreach (self::DEPENDENCIES as $name => $relation) {
            $settings[$name] = app($relation);
        }

        return collect($settings ?? []);
    }
}