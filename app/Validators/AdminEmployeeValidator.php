<?php

namespace App\Validators;

use App\Department;

class AdminEmployeeValidator extends BaseValidator
{
    /**
     * @var string
     */
    const RULE_ANY = 'any';

    /**
     * @var array Правила валидации
     */
    protected static $rules;

    /**
     * \App\Department
     */
    protected $departments;

    /**
     * @var string
     */
    protected $countries;

    public function __construct()
    {
        $this->departments = Department::pluck('id')->implode(',');

        self::$rules = [
            self::RULE_ANY => $this->ruleAny()
        ];
    }

    /**
     * Возвращает правило валидации по его имени
     *
     * @param  string $name
     * @return array
     */
    public function getRules(string $name) : array
    {
        return self::$rules[$name];
    }

    /**
     * @return array
     */
    protected function ruleAny() : array
    {
        return [
            'last_name'     => 'required|alpha',
            'first_name'    => 'required|alpha',
            'surname'       => 'required|alpha',
            'position'      => 'required|string',
            'birthday'      => 'required|date',
            'employee_date' => 'required|date',
            'email'         => 'email',
            'departments.*' => "filled|in:{$this->departments}",
        ];
    }
}