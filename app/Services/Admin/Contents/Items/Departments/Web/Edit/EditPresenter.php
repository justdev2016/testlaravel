<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Edit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collect;
use App\Services\Admin\Contents\Core\Support\Presenter;

class EditPresenter extends Presenter
{
    public function edit() : Collect
    {
        return collect([
            'id'   =>$this->model->id,
            'name' => $this->model->name,
        ]);
    }
}