<?php

namespace App\Services\Admin\Document;

use App\Services\Admin\Support\Component;

/**
 * Item - лист
 * - Представляет листовой узел композиции и не имеет потомков
 * - Определяет поведение примитивных объектов в композиции
 */
class Item extends Component
{

    public function add(Component $component)
    {
        throw new \Exception("Cannot add to a item", 1);
    }

    public function remove(Component $component)
    {
        throw new \Exception("Cannot remove from a item", 1);
    }

    public function render()
    {
        return $this->item;
    }
}