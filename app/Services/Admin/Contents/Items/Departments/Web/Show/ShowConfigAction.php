<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Show;

use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Settings\EmptyCollectionAction;
use App\Services\Admin\Contents\Items\Departments\Web\Show\Config\{
    ActionsConfig,
    FieldsConfig,
    RoutesConfig,
    SettingsConfig
};

/**
 * Выполнение инициализации настроек для `Web`.`Departments`.`Show`
 */
class ShowConfigAction implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object   = $object;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = new ActionsConfig(new EmptyCollectionAction);
        $this->settings = new FieldsConfig($this->settings);
        $this->settings = new SettingsConfig($this->settings);
        $this->settings = new RoutesConfig($this->settings);

        $this->settings = $this->settings->action();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $components = $response->getData('config')->map(function($item, $type) {
            $item = collect([
                'driver'  => $item['driver'],
                'config'  => $item['config'] ?? [],
                'actions' => $this->settings->getData("actions.{$type}"),
                'fields'  => $this->settings->getData("fields.{$type}"),
                'routes'  => $this->settings->getData("routes.{$type}"),
            ]);

            return $item;
        });

        $response->get('settings')->put('page', 'show');
        $response->get('settings')->put('action', 'show');

        $response->put('components', $components);

        return $response;
    }
}