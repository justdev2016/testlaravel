<?php

namespace App\Services\Admin\Managers\Files;

use File;
use SoapBox\Formatter\Formatter;
use Illuminate\Support\Collection;
use App\Validators\AdminFCommanderValidator;
use App\Services\Admin\Managers\Files\BaseOptions;
use App\Contracts\Managers\Files\Options as ReadOptions;

/**
 * Чтение XML-файлов
 * @todo описать функционал для получеиня множества данных
 */
class XmlReadOptions extends BaseOptions implements ReadOptions
{
    /** @var string */
    protected $src;

    /** @var array|null */
    protected $node;

    /** @var array|null */
    protected $attributes;

    /** @var array */
    protected $xml;

    /** @var \Illuminate\Support\Collection */
    protected $complete;

    /**
     * @param  \Illuminate\Support\Collection $collection
     */
    public function __construct(Collection $collection = null)
    {
        parent::__construct();
        
        $this->complete = collect();
        
        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    /**
     * @param  \Illuminate\Support\Collection  $collection
     */
    public function make(Collection $collection)
    {
        $this->validate($collection->toArray(), AdminFCommanderValidator::RULE_READ_XML);
        
        $this->multiple   = $collection->has('multiple');
        $this->src        = $collection->get('src');
        
        if ($this->multiple) {
            foreach ($collection->get('multiple') as $value) {
                $this->node[]       = $value['node'] ?? null;
                $this->attributes[] = $value['attributes'] ?? null;
            }
        } else {
            $this->node       = $collection->get('node', null);
            $this->attributes = $collection->get('attributes', null);
        }

        return $this;
    }

    /**
     * @return array
     * @todo реализовать функционал инфо
     */
    public function action(bool $info = true) : array
    {
        $this->xml = Formatter::make(File::get($this->src), Formatter::XML)->toArray();

        if (! empty($this->node)) {
            if ($this->multiple) {
                // @todo реализовать функционал
            } else {
                $this->complete->push($this->readNode());
            }

            return $this->complete->toArray();
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function readNode() : array
    {
        $xml = $this->xml;

        for ($i = 0; $i < count($this->node); $i++) {
            $xml = $this->getAttribute($xml, $this->node[$i]);
        }

        $xml = $this->getAttribute($xml, '@attributes');

        foreach ($this->attributes as $attribute) {
            $result[$attribute] = $this->getAttribute($xml, $attribute);
        }

        return $result;
    }

    /**
     * Получение данных
     *
     * @param array  $xml
     * @param string $attribute
     * @throws \Exception
     * @return mixed
     */
    protected function getAttribute(array $xml, string $attribute)
    {
        if (! isset($xml[$attribute])) {
            throw new \Exception('message.not-found-attribute', 1);
        }
        
        return $xml[$attribute];
    }
}