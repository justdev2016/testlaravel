<?php

namespace App\Validators;

use Validator;
use Illuminate\Validation\Validator as IlluminateValidator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Базовый валидатор для сервисов
 */
abstract class BaseValidator
{
    /** @const Сценарий создания */
    const RULE_CREATE   = ValidatorInterface::RULE_CREATE;
    /** @const Сценарий обновления */
    const RULE_UPDATE   = ValidatorInterface::RULE_UPDATE;

    /**
     * @var IlluminateValidator Объект стандартного валидатора
     */
    public $validator;

    /**
     * @var array Правила валидации
     */
    protected static $rules = [];

    /**
     * @var array Сообщения неудачной валидации модели
     */
    protected static $messages = [];

    /**
     * Валидация указанных данных
     *
     * @param array $data Проверяемые данные
     * @param string $scenario Сценарий проверки
     * @return bool
     */
    public function passes(array $data, string $scenario)
    {
        $this->validator = Validator::make($data, static::$rules[$scenario], static::$messages);
        return $this->validator->passes();
    }

    /**
     * Валидация указанных данных и вызов исключения ValidatorException в случае неудачи
     *
     * @param array $data
     * @param string $scenario
     * @throws ValidatorException
     */
    public function passesOrFail(array $data, string $scenario)
    {
        if (!$this->passes($data, $scenario)) {
            $this->fail();
        }
    }

    /**
     * Вызов исключения ValidatorException
     *
     * @throws ValidatorException
     */
    public function fail()
    {
        throw new ValidatorException($this->validator->errors());
    }

    /**
     * @return IlluminateValidator
     */
    public function errorsBag()
    {
        return $this->validator;
    }

    /**
     * @return string
     */
    public function firstMessage()
    {
        return $this->validator->messages()->first();
    }
}