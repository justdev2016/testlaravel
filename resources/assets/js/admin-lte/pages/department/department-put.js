import { notification, removeNotification } from "../functions/notification"
import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content


new Vue({
    el: "#app",
    data: {
        form: {
            name: ''
        }
    },
    methods: {
        save () {
            let url = $(this.$refs.form).attr('action'),
                params = {
                    params: this.form
                }

            this.$http.put(url, this.form)
                .then(({data}) => {
                    removeNotification()

                    window.location.href = '/admin/departments'
                }, (error) => {
                    notification('error', error.body)
                })
        },
        cancel () {
            window.location.href = '/admin/departments'
        }
    },
    mounted () {
        let data = $(this.$refs.form).data('item')

        this.form.name = data.name
    }
})