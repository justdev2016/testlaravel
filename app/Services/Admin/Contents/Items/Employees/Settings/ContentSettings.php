<?php

namespace App\Services\Admin\Contents\Items\Employees\Settings;

use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Settings\{BaseSettings,InfoSettings};
use App\Services\Admin\Contents\Items\Employees\Settings\{
    LocaleSettings,
    RepositorySettings,
    WebPageSettings
};

/**
 * Индивидуальные настройки `Content`
 */
class ContentSettings extends BaseSettings
{
    /** @var string Тип контента */
    const TYPE = 'employees';

    /** @var array Настройки для всех типов действий */
    const ALL = [
        'locale'     => LocaleSettings::class,
        'repository' => RepositorySettings::class,
    ];

    /** @var array Настройки только для `Web` */
    const WEB = [
        'views' => WebPageSettings::class,
        'info'  => InfoSettings::class,
    ];

    /**
     * @param string $type
     * @return \Illuminate\Support\Collection
     */
    public function __construct(string $type)
    {
        parent::__construct();

        $this->make();

        $instance = 'instance' . ucfirst($type);

        $this->{$instance}();

        $this->settings->put('type', self::TYPE);
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->makeWeb();
        $this->makeApi();
    }

    /**
     * @return void
     */
    private function makeWeb()
    {
        $this->settings->put('web', array_merge(self::ALL, self::WEB));
    }

    /**
     * @return void
     */
    private function makeApi()
    {
        $this->settings->put('api', self::ALL);
    }
}