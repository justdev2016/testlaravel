export function initSortable(selector, options) {
    $(selector).sortable(options).disableSelection()
}