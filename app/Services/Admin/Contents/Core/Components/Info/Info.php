<?php

namespace App\Services\Admin\Contents\Core\Components\Info;

use Components;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Компонент `Info`
 */
class Info
{
    /** @var string */
    public $driver;

    /**
     * @param string $driver
     */
    public function __construct(string $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return object
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $class = Components::driver('info', $this->driver);

        return $class->make($data, $settings);
    }
}