/**
 * dataTableOptions
 * @todo подумать над режимом ручного reOrdering (по кнопке)
 */
export function getDataTableOptions() {

    let options = {
        scrollY: "550px",
        scrollCollapse: true,
        lengthMenu: [[-1, 10, 25, 50], ["All", 10, 25, 50]],
        paging:   false,
        order: [[2, 'asc']]
    }
}