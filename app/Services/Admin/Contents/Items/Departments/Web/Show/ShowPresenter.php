<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Show;

use App\Services\Admin\Contents\Core\Support\Presenter;

class ShowPresenter extends Presenter
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name', 'created_at', 'updated_at'];
}