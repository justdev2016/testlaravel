<?php

namespace App\Services\Admin\Managers\Notify;

use App\Traits\Admin\ErrorsTrait;
use Illuminate\Support\Collection;
use App\Validators\AdminNotifyValidator;
use App\Services\Admin\Managers\Notify\BaseOptions;

/**
 * Success options
 */
class ErrorOptions extends BaseOptions
{
    use ErrorsTrait;

    /** @const успешный статус сообщения: УСПЕХ */
    const STATUS_ERROR = 'error';

    /** @var \Illuminate\Support\Collection */
    protected $notify;

    /** @var array */
    const METHODS = ['notify'];

    /**
     * @param \Exception $error
     */
    public function __construct(\Exception $error = null)
    {
        parent::__construct();

        $this->notify = collect();

        if (! empty($error)) {
            $this->make($error);
        }
    }

    public function __get($name)
    {
        if (! in_array($name, self::METHODS)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return $this->$name;
    }

    /**
     * Инициализация сообщения
     * Допустимые значения и их типы:
     * [
     *     message => string => required,
     *     status  => string => default:'success'
     * ]
     *
     * @param array $data
     * @return self
     * @todo подумать над переносом логики `validErrors` в общий валидатор
     */
    public function make(\Exception $error)
    {
        $this->validate(['raw' => $error], AdminNotifyValidator::RULE_ERROR);

        $message = $this->validErrors($error);

        $this->notify->put('message', $message);
        $this->notify->put('status', self::STATUS_ERROR);

        return $this;
    }
}