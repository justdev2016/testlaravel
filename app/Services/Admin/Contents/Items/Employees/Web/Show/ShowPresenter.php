<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Show;

use App\Services\Admin\Contents\Core\Support\Presenter;

class ShowPresenter extends Presenter
{
    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'full_name', 'birthday', 'employee_date', 'phone', 'email', 'position', 'departments_list', 'created_at','updated_at'
    ];

    public function getFullNameAttribute()
    {
        return trim($this->model->last_name . ' ' . $this->model->first_name . ' ' . $this->model->surname);
    }

    public function getDepartmentsListAttribute()
    {
        return $this->model->departments()->lists('name')->implode(', ');
    }
}