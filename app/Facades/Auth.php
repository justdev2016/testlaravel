<?php

namespace App\Facades;

use App\Models\User;
use App\Models\UserAppToken;

/**
 * @see \Illuminate\Auth\AuthManager
 * @see \Illuminate\Contracts\Auth\Factory
 * @see \Illuminate\Contracts\Auth\Guard
 * @see \Illuminate\Contracts\Auth\StatefulGuard
 */
class Auth extends \Illuminate\Support\Facades\Auth
{
    /**
     * Получение авторизованного пользователя.
     * В первую очередь из сессии, если не удастся, то в Input::get() проверяются поля
     * content_token или auth и производится попытка получить пользователя по этому токену
     *
     * @return User|null
     */
    public static function user()
    {
        if ($user = parent::user()) {
            return $user;
        }
        $token = Input::get('content_token', null) ?: Input::get('auth', null);
        if ($token === null) {
            return null;
        }
        $token = UserAppToken::find($token);
        if (!$token || $token->isExpired()) {
            return null;
        }
        $user = $token->user;
        if (!$user) {
            return null;
        }
        self::login($user, true);
        $token->touch(); // Продлеваем токен
        return $user;
    }
}