import { notification, removeNotification } from "../functions/notification"
import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').content


new Vue({
    el: "#app",
    data: {
        form: {
            name: ''
        }
    },
    methods: {
        save () {
            let url = $(this.$refs.form).attr('action'),
                params = {
                    params: this.form
                }

            this.$http.post(url, this.form)
            // this.$http.post(url, params)
                .then(({data}) => {
                    removeNotification()

                    window.location.href = '/admin/departments'
                }, (error) => {
                    notification('error', error.body)
                })
        },
        cancel () {
            window.location.href = '/admin/departments'
        }
    }
})