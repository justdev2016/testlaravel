<div class="input-group col-md-7 hidden-country">
  <h3>
    @lang('countries.country')<strong class="required">*</strong>:
    <button type="button" class="btn btn-danger delete-country">
      <i class="fa fa-minus" aria-hidden="true"></i>
    </button>
  </h3>
  {!!
    Form::select(
      "countries[]",
      $data['countries'],
      null,
      [
        'placeholder' => trans('countries.select-message'),
        'class'       => "form-control course-country"
      ])
  !!}
</div><!-- /.input group -->