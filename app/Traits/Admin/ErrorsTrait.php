<?php

namespace App\Traits\Admin;

/**
 * Трейт работы с ошибками
 */
trait ErrorsTrait
{
    /**
     * Проверка кода ошибки, по которому
     * будет принято решение об отображении текста ошибки
     * 
     * @param  \Exception $e
     * @return string
     */
    protected function validErrors(\Exception $e) : string
    {
        if (app()->isLocal() && env('APP_DEBUG')) {
            return $e->getMessage();
        }

        return ($e->getCode() > 1) ? trans('message.server-error') : $e->getMessage();
    }
}