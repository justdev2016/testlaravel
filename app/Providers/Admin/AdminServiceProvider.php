<?php

namespace App\Providers\Admin;

use Illuminate\Support\ServiceProvider;
use App\Providers\Admin\Document\DocumentServiceProfider;
use App\Providers\Admin\Providers\{AdminComposeServiceProvider,AdminFacadesServiceProvider};
use App\Providers\Admin\Components\{AdminCollectionMacrosServiceProvider,AdminMenuServiceProvider};

class AdminServiceProvider extends ServiceProvider
{
    /** @var array */
    const PROVIDERS = [
        // Kernel
        AdminCollectionMacrosServiceProvider::class,
        DocumentServiceProfider::class,
        AdminComposeServiceProvider::class,
        AdminFacadesServiceProvider::class,
        // Kernel Components
        AdminMenuServiceProvider::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerProviders();
    }

    /**
     * @return void
     */
    protected function registerProviders()
    {
        foreach (self::PROVIDERS as $provider) {
            $this->app->register($provider);
        }
    }
}
