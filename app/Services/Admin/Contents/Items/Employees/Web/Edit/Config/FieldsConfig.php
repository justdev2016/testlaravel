<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Edit\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `actions`
 */
class FieldsConfig implements ActionContract
{
    /** @var array */
    const FIELDS = [
        'form' => [
            'last_name', 'first_name', 'surname', 'email', 'phone', 'position', 'birthday', 'employee_date', 'departments' => ['id']
        ],
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $fields;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->fields = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        foreach (self::FIELDS as $type => $data) {
            $this->fields->put($type, $data);
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('fields', $this->fields);
    }
}