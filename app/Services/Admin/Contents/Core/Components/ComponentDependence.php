<?php

namespace App\Services\Admin\Contents\Core\Components;

use Illuminate\Support\Collection;

class ComponentDependence
{
    /** @var \Illuminate\Support\Collection */
    protected $item;

    public function __construct(array $item)
    {
        $this->item = collect($item);
    }

    public function __get($name)
    {
        if (method_exists($this, $name) || method_exists($this->item, $name)) {
            return $this->__call($name, []);
        }

        throw new \Exception(trans('message.method-not-exist'), 2);
    }

    public function __call($method, $arguments)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array([$this, $method], $arguments);
        }

        if (method_exists($this->item, $method)) {
            return call_user_func_array([$this->item, $method], $arguments);
        }

        throw new \Exception(trans('message.method-not-exist'), 1);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function data() : Collection
    {
        return $this->item;
    }
}