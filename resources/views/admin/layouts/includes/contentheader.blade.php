<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', trans('message.homepage'))
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            {!!
                HTML::decode(link_to('#', "<i class='fa fa-dashboard'></i>" . trans('message.level')))
            !!}
        </li>
        <li class="active">@lang('message.here')</li>
    </ol>
</section>