<?php

namespace App\Services\Admin\Contents\Core\Support;

use App\Contracts\Admin\RegistryContract;

/**
 * Реестр всех `Components` для `Contents`
 */
class ComponentRegistry implements RegistryContract
{
    /** @var string */
    const GLOBAL = 'global';

    /** @var string */
    const LOCAL  = 'local';

    /** @var array */
    private static $allowedKeys = [
        self::GLOBAL,
        self::LOCAL,
    ];

    /** @var array */
    private static $storedValues = [];

    /**
     * @see \App\Contracts\Admin\RegistryContract@set
     */
    public function set(string $key, $value)
    {
        if (! in_array($key, self::$allowedKeys)) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        self::$storedValues[$key] = $value;
    }

    /**
     * @see \App\Contracts\Admin\RegistryContract@get
     */
    public function get(string $key)
    {
        if (! in_array($key, self::$allowedKeys) || ! isset(self::$storedValues[$key])) {
            throw new \InvalidArgumentException('Invalid key given!');
        }

        return self::$storedValues[$key];
    }

    /**
     * @return bool
     */
    public function isEmpty() : bool
    {
        return empty(self::$storedValues);
    }
}