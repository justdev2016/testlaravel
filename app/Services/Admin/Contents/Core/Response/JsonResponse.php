<?php

namespace App\Services\Admin\Contents\Core\Response;

use Illuminate\Support\Collection;

class JsonResponse
{
    /** @var \Illuminate\Support\Collection */
    protected $messages;

    /**
     * @param  array $messages
     */
    public function __construct(array $messages)
    {
        $this->messages = collect($messages);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function response()
    {
        return response()->json($this->messages->get('messages')->toArray(), $this->messages->get('code', 200));
    }
}