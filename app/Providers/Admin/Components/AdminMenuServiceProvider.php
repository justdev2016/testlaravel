<?php

namespace App\Providers\Admin\Components;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Support\Menu\{
    Item,
    Menu,
    MenuComposite
};

class AdminMenuServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCollectionMacros();
        /** `Global`.`Widget`.`Menu` register */
        $this->registerGlobalWidget();
        $this->bootViewComposerMenu();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMenuComponent();
    }

    /**
     * @return void
     */
    protected function registerMenuComponent()
    {
        $mapper = $this->app['widgetMapper'];

        if ($mapper->has('menu')) {
            $section = $mapper->section('global', 'menu')->keys()->first();
            /** `Menu`.`Home` register */
            $this->registerGlobalMenu();
            /** Register Facade `Menu` */
            $this->registerMenuFacade();
            /** `Menu`.[`Contents`] register */
            $this->registerSubMenu();
        }
    }

    /**
     * @return void
     */
    protected function registerMenuFacade()
    {
        $this->app->singleton('menu', function ($app) {
            return new Menu($app['components.menu']);
        });
    }

    /**
     * @return void
     */
    protected function registerGlobalMenu()
    {
        $root = $this->app['components.menu'] = new MenuComposite('menu');

        $home = new Item($this->app['config']['admin-menu.data.home']);

        $root->add($home);
    }

    /**
     * @return void
     */
    protected function registerSubMenu()
    {
        $this->app['components.menu.childrens.contents'] = $this->app['menu']->addSubMenu('contents');
    }

    /**
     * @return void
     */
    private function registerCollectionMacros()
    {
        collect()->macro('menu', function() {
            return $this->trans('text')->route('url')->all();
        });
    }

    /**
     * @return void
     */
    protected function registerGlobalWidget()
    {
        $mapper = $this->app['widgetMapper'];

        if ($mapper->has('menu')) {
            $section = $mapper->section('global', 'menu')->keys()->first();

            $section = $this->app['document.section'][$section];

            $menu = [
                'name' => 'menu',
                'item' => $this->app['menu']
            ];

            $menuItem = new \App\Services\Admin\Document\Item($menu);

            $section->add($menuItem);
        }
    }

    /**
     * Регистрация View Composer `Memu`
     * (глобальные данные для views)
     *
     * @return void
     */
    private function bootViewComposerMenu()
    {
        view()->composer('*', function ($view) {
            $mapper = $this->app['widgetMapper'];

            if ($mapper->has('menu')) {
                $mapper->make($view)->compose('menu', $this->app['menu']);
            }
        });
    }
}
