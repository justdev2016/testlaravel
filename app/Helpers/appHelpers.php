<?php

if (! function_exists('isAjax')) {
    /**
     * Проверяет запрос на тип `XMLHttpRequest`
     *
     * @return bool
     */
    function isAjax()
    {
        return (request()->wantsJson() || request()->ajax());
    }
}

if (! function_exists('checkAjaxWithError')) {
    /**
     * Проверяет запрос на тип `XMLHttpRequest`,
     * если не Ajax - вернуть ошибку
     *
     * @param string $errorMessage
     * @return mixed
     */
    function checkAjaxWithError($errorMessage = '')
    {
        if (! isAjax()) {
            abort(403, $errorMessage);
        }
    }
}

if (! function_exists('trimArray')) {
    /**
     * Очищает значения массива от лишних пробелов +
     * удаляет пустые значения
     * Возврашает новый массив, с ключами по порядку
     *
     * @param array $data
     * @return array
     */
    function trimArray(array $data)
    {
        return array_values(array_filter(array_map('trim', $data)));
    }
}

if (! function_exists('present')) {
    /**
     * Helper to Present Models
     *
     * @param  string $model
     * @param  string $presenter
     * @return object
     */
    function present($model, $presenter)
    {
        return new $presenter($model);
    }
}

if (!function_exists('formatDateTime')) {
    /**
     * Форматирование даты и времени
     * пользователя по его локализации
     *
     * @param string $date
     * @param string $pattern Шаблон формата
     * @return string
     * @todo улучщить форматирование времени + учесть `timeZone` для времени
     */
    function formatDateTime(string $date, string $pattern)
    {
        $config = config('admin.date_time');

        return (new \Date($date))->format($config[trans()->getLocale()][$pattern], $config['default'][$pattern]);
    }
}