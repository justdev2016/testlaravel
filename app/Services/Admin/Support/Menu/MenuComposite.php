<?php

namespace App\Services\Admin\Support\Menu;

use App\Services\Admin\Support\Component;

/**
 * MenuComposite - составной объект
 * - Определяет поведение компонентов, у которых есть потомки
 * - Хранит компоненты-потомки
 * - Реализует относящиеся к управлению потомками операции и интерфейс
 */
class MenuComposite extends Component
{
    /** @var array */
    private $children = [];

    public function add(Component $component)
    {
        $key = $component->item['name'] ?? $component->item;

        $this->children[$key] = $component;
    }

    public function remove(Component $component)
    {
        $key = $component->item['name'];

        unset($this->children[$key]);
    }

    public function render()
    {
        foreach($this->children as $child) {
            $data[$this->item][] = collect($child->render())->menu();
        }

        return $data;
    }
}