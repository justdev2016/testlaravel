// create department
$('#create-department').on('click', (e) => {
    e.preventDefault()

    let clone = $('.hidden-department').clone(true)
    clone
        .appendTo('.department-block')
        .removeClass('hidden-department')
})
