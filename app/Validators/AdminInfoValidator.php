<?php

namespace App\Validators;

/**
 * Валидатор для работы с файлами
 */
class AdminInfoValidator extends BaseValidator
{
    /** @var string */
    const RULE_OPTIONS = 'options';
    /** @var string */
    const RULE_DATA    = 'data';

    /** @var array Правила валидации */
    protected static $rules;

    public function __construct()
    {
        self::$rules = [
            self::RULE_OPTIONS => $this->ruleOptions(),
            self::RULE_DATA    => $this->ruleData(),
        ];
    }

    /**
     * Возвращает правило валидации по его имени
     *
     * @param  string $name
     * @return array
     */
    public function getRules(string $name) : array
    {
        return self::$rules[$name];
    }

    /**
     * @return array
     */
    protected function ruleOptions() : array
    {
        return [
            'columns' => 'required',
            'actions' => 'required',
            'locale'  => 'required|string',
        ];
    }

    /**
     * @return array
     */
    protected function ruleData() : array
    {
        return [
            'repository'  => 'required',
            'action'      => 'required|string',
            'fields'      => 'mixed:collection',
            'middlewares' => 'array',
        ];
    }
}