<?php

namespace App\Providers\Admin\Contents\Departments;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Support\Menu\Item;
use App\Validators\AdminEmployeeValidator;
use App\Services\Admin\AdminEmployeeService;
use App\Contracts\Admin\ContentEntryContract;
use App\Services\Admin\Contents\Core\Support\ContentRegistry;
use App\Services\Admin\Contents\Items\Departments\Support\Entry;

class AdminDepartmentServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('departments', \App\Department::class);
        /** `Menu`.[`Contents`].`Department` register */
        if ($this->app['config']->get('departments.active', false)) {
            $this->registerDepartmentMenu();
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerService();
        $this->registerValidator();
        $this->registerSettings();
    }

    /**
     * @return void
     */
    protected function registerDepartmentMenu()
    {
        if ($this->app->resolved('components.menu')) {
            $contents = $this->app['components.menu.childrens.contents'];

            $employee = $this->app['menu']->item($this->app['config']['departments.menu']);

            $contents->add($employee);

            $this->app['menu']->add($contents);
        }
    }

    /**
     * @return void
     */
    private function registerService()
    {
        $this->app->singleton(Entry::class, function($app) {
            return new Entry($app['config']['departments.actions']);
        });

        $this->app->bind(ContentEntryContract::class, Entry::class);
    }

    /**
     * @return void
     */
    private function registerValidator()
    {
        $this->app->bind(AdminEmployeeValidator::class, function ($app) {
            return new AdminEmployeeValidator;
        });
    }

    /**
     * @return void
     */
    private function registerSettings()
    {
        $registry = $this->app[ContentRegistry::class];

        $registry->set(ContentRegistry::SETTINGS, ['departments' => $this->app['config']['departments.settings']]);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            AdminEmployeeService::class,
            AdminEmployeeValidator::class
        ];
    }
}