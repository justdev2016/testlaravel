<!DOCTYPE html>
<html lang="en">

@section('htmlheader')
    {{-- !! временно, через `view`, когда будут компоненты - перейдем на `render` ?? --}}
    @include($document->viewInclude('htmlheader'))
@show

{{--
    BODY TAG OPTIONS:
    =================
    Apply one or more of the following classes to get the desired effect
    |---------------------------------------------------------|
    | SKINS         | skin-blue                               |
    |               | skin-black                              |
    |               | skin-purple                             |
    |               | skin-yellow                             |
    |               | skin-red                                |
    |               | skin-green                              |
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
 --}}

 {{-- When not using the left sidebar, then set another class on the body --}}
<body class="hold-transition skin-blue @if($document->isActive('left')) sidebar-mini @else layout-top-nav @endif">
    @if ($document->isActive('left') || $document->isActive('content'))
        <div class="wrapper">

            {{-- Main Header --}}
            @if ($document->isActive('header') && $document->isActive('left'))

                @include($document->viewInclude('mainheader'))

                {{-- [Left] --}}
                {{-- [MENU,UserPanel,SearchForm] --}}
                {!! $document->render('left') !!}

            @elseif ($document->isActive('header') && ! $document->isActive('left'))

                {{-- @todo реализовать логику + разметку --}}
                @include($document->viewInclude('mainheaderwithoutleft'))

            @endif

            {{-- Content Wrapper. Contains page content --}}
            <div class="content-wrapper">
                @if ($document->isActive('content'))
                    {{-- [Content] --}}
                    {!! $document->render('content') !!}
                @endif
            </div><!-- /.content-wrapper -->

            {{-- Control SideBar --}}
            {{-- [Footer] --}}
            @if ($document->isActive('footer'))
                @include('admin.layouts.includes.footer')
            @endif
        </div><!-- ./wrapper -->
    @endif

    @include($document->viewInclude('scripts'))
</body>
</html>
