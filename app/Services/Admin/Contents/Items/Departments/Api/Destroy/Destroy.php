<?php

namespace App\Services\Admin\Contents\Items\Departments\Api\Destroy;

use App\Employee;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Core\Settings\WebConfigAction;
use App\Services\Admin\Contents\Items\Departments\Settings\ContentSettings;

/**
 * Логика формирования данных для действия `DESTROY`
 *
 * @todo доделать multi-компонентность на странице
 */
class Destroy implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $config;

    /**
     * @param \App\Services\Admin\Contents\Core\Support\Input $params
     */
    public function __construct(Input $params)
    {
        $this->config = [
            'settings' => new ContentSettings('api'),
            'requests' => $params,
        ];
    }

    /**
     * @return self
     */
    public function init()
    {
        $this->object = new WebConfigAction($this->config);
        $this->object = new DestroyConfigAction($this->object);
        $this->object = new DestroyAction($this->object);

        return $this;
    }

    /**
     * @return mixed
     */
    public function action()
    {
        return $this->object->action();
    }
}