<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Admin\Contents\Presentable;

class Department extends Model
{
    use Presentable;
    /**
     * Список работников
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employees()
    {
        return $this->belongsToMany(Employee::class, 'departments_employees')->withTimestamps();
    }
}
