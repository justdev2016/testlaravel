var elixir = require('laravel-elixir');

require('laravel-elixir-browserify-official')
require('laravel-elixir-vueify-2.0')

var bowerFullPath            = './vendor/bower/';
var adminLTEDir              = './resources/assets/js/admin-lte/';
var adminLTEPublicCssPath    = 'public/admin-lte/css/';
var adminLTEPublicJsPath     = 'public/admin-lte/js/';
var adminLTEPublicImagesPath = 'public/admin-lte/img';
var adminLTEPublicFontsPath  = 'public/admin-lte/fonts';

// AdminLTE copy
elixir(function(mix) {
    // copy fonts
    mix.copy([
            bowerFullPath + 'AdminLTE/bootstrap/fonts',
            bowerFullPath + 'font-awesome/fonts',
        ], adminLTEPublicFontsPath
    );

    // ################################################

    // copy images
    mix.copy([
            bowerFullPath + 'AdminLTE/dist/img/**/*.*',
        ], adminLTEPublicImagesPath
    )
    // copy images iCheck
        .copy(
            bowerFullPath + 'iCheck/skins/square/blue*.png'
            , adminLTEPublicCssPath
        );

    // ################################################

    // datatables
    mix.copy([
        adminLTEDir + 'plugins/datatables/lang/*.json'
    ], adminLTEPublicJsPath + 'plugins/datatables/lang');

    // select2
    mix.copy([
        bowerFullPath + 'select2/dist/js/i18n/*.js'
    ], adminLTEPublicJsPath + 'plugins/select2/lang');

});

// ################################################

// AdminLTE compile less
elixir(function(mix) {
    mix.less('admin-lte/admin.less', adminLTEPublicCssPath + 'build/pages/admin.css');
});

// ################################################

// AdminLTE concat css
elixir(function(mix) {
    // common
    mix.styles([
        bowerFullPath + 'AdminLTE/bootstrap/css/bootstrap.css',
        bowerFullPath + 'font-awesome/css/font-awesome.css',
        bowerFullPath + 'Ionicons/css/ionicons.css',
        bowerFullPath + 'AdminLTE/dist/css/AdminLTE.css',
        bowerFullPath + 'AdminLTE/dist/css/skins/skin-blue.css',
        bowerFullPath + 'iCheck/skins/square/blue.css',
        bowerFullPath + 'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
    ],'public/admin-lte/css/common.css')

    // pages
        .styles([
            bowerFullPath + 'datatables/media/css/dataTables.bootstrap.css',
            bowerFullPath + 'jquery-ui/themes/base/jquery-ui.css',
            bowerFullPath + 'select2/dist/css/select2.css',
            '../../../public/admin-lte/css/build/pages/admin.css',
        ],'public/admin-lte/css/pages.css');
});

// ################################################

// AdminLTE compile js
elixir(function(mix) {
    // common
    mix.scripts([
        bowerFullPath + 'jquery/dist/jquery.js',
        bowerFullPath + 'bootstrap-sass/assets/javascripts/bootstrap.js',
        bowerFullPath + 'AdminLTE/dist/js/app.js',
        bowerFullPath + 'moment/min/moment-with-locales.js',
        bowerFullPath + 'eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js'
    ], adminLTEPublicJsPath + 'common.js')

    // libs
        .scripts([
            // bowerFullPath + 'datatables/media/js/jquery.dataTables.js',
            // bowerFullPath + 'datatables/media/js/dataTables.bootstrap.js',
            bowerFullPath + 'fastclick/lib/fastclick.js',
            bowerFullPath + 'holderjs/holder.js',
            bowerFullPath + 'jquery-ui/jquery-ui.js',
            bowerFullPath + 'jquery.inputmask/dist/jquery.inputmask.bundle.js',
            bowerFullPath + 'select2/dist/js/select2.js',
            bowerFullPath + 'iCheck/icheck.js',
        ], adminLTEPublicJsPath + 'libs.js')

        // employees
        /*.browserify([
            './resources/assets/js/admin-lte/pages/employee/employee-index.js',
            './resources/assets/js/admin-lte/pages/employee/employee-read.js',
            './resources/assets/js/admin-lte/pages/employee/employee-create.js',
            './resources/assets/js/admin-lte/pages/employee/employee-put.js',
        ], adminLTEPublicJsPath + 'employees.js')*/
        .browserify(
            './resources/assets/js/admin-lte/pages/employee/employee-index.js',
            adminLTEPublicJsPath + 'index-employees.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/employee/employee-create.js',
            adminLTEPublicJsPath + 'create-employees.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/employee/employee-put.js',
            adminLTEPublicJsPath + 'put-employees.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/employee/employee-read.js',
            adminLTEPublicJsPath + 'show-employees.js')
        // departments
        /*.browserify([
            './resources/assets/js/admin-lte/pages/department/department-index.js',
            './resources/assets/js/admin-lte/pages/department/department-create.js',
            './resources/assets/js/admin-lte/pages/department/department-read.js',
            './resources/assets/js/admin-lte/pages/department/department-update.js',
        ], adminLTEPublicJsPath + 'departments.js')*/

        .browserify(
            './resources/assets/js/admin-lte/pages/department/department-index.js',
            adminLTEPublicJsPath + 'index-departments.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/department/department-create.js',
            adminLTEPublicJsPath + 'create-departments.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/department/department-put.js',
            adminLTEPublicJsPath + 'put-departments.js')
        .browserify(
            './resources/assets/js/admin-lte/pages/department/department-read.js',
            adminLTEPublicJsPath + 'show-departments.js')
});