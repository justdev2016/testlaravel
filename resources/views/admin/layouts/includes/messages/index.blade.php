@if(!empty($errors->all()) || (session()->has('notify') && session('notify')['status'] == 'error'))
    @include('admin.layouts.includes.messages.error')
@endif
@if(session()->has('notify') && session('notify')['status'] == 'success')
    @include('admin.layouts.includes.messages.success')
@endif