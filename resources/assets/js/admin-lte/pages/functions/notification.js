// public
function notification(type, data, selector, options) {
    removeNotification()

    switch(type) {
        case 'success':
            success(data, selector, options)
            break

        case 'error':
            error(data, selector)
            break

        case 'info':
            info(data, selector)
            break

        case 'warning':
            warning(data, selector)
            break

        default:
            break
    }
}

function removeNotification() {
    $('.error, .success, .info, .warning').remove()
}

// protected
function success(data, selector, options) {
    selector = selector || $('.main-box')
    options = options || {}

    if (data) {
        selector.prepend('<div class="callout callout-success success">' + '<h4>Success!</h4><p>'+ data + '</p></div>')
    }
}

function error(data, selector) {
    let output   = [],
        ul       = $('<ul></ul>'),
        error    = data

    selector = selector || $('.content')

    if (error.status !== undefined) {
        output.push("<li>" + error.message + "</li>")
    } else {
        $.each(error, (key, value) => {
            if (typeof value == 'string') {
                output.push("<li>" + error.message + "</li>")
            } else {
                value.forEach((item, i) => {
                    output.push("<li>" + item + "</li>")
                })
            }
        })
    }

    ul.html(output.join(''))

    selector.prepend('<div class="callout callout-danger error">' + '<h4>Error!</h4></div>')
    $('.error').append(ul)
}

function info(data, selector) {}

function warning(data, selector) {}

// export
export { notification, removeNotification }