function getLocale() {
    return window.locale
}

function isSuccess(response) {
    return response.status == 'success'
}

function isError(response) {
    return response.status == 'error'
}

// export
export { getLocale, isSuccess, isError }