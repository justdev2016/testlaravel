<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Index;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Settings\EmptyCollectionAction;
use App\Services\Admin\Contents\Items\Employees\Web\Index\Config\{
    ActionsConfig,
    FieldsConfig,
    SyncConfig
};

/**
 * Инициализация настроек для `Web`.`Employees`.`Index`
 */
class IndexConfigAction implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = new ActionsConfig(new EmptyCollectionAction);
        $this->settings = new FieldsConfig($this->settings);
        $this->settings = new SyncConfig($this->settings);

        $this->settings = $this->settings->action();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        $components = $response->getData('config')->map(function($item, $type) {
            $item = collect([
                'driver'  => $item['driver'],
                'config'  => $item['config'] ?? [],
                'actions' => $this->settings->getData("actions.{$type}"),
                'fields'  => $this->settings->getData("fields.{$type}"),
                'sync'    => $this->settings->getData("sync.{$type}"),
            ]);

            return $item;
        });

        $response->get('settings')->put('page', 'index');
        $response->get('settings')->put('action', 'index');

        $response->put('components', $components);

        return $response;
    }
}