<div class="callout callout-info @if(isset($static) && $static) static-info @else info @endif">
    <h4>Info!</h4>
    @if(isset($info))
        <ul>
        @foreach($info as $information)
            <li>{!! $information !!}</li>
        @endforeach
        </ul>
    @elseif(session()->has('info'))
        <p>{{ session('warning') }}</p>
    @endif
</div>