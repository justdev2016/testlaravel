<?php

namespace App\Traits\Admin;

use Gate;
use App\Models\Language;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\{Builder, Model};

/**
 * Работа с правами доступа
 * пользователя к ресурсам
 */
trait ACLTrait
{
    /**
     * Проверка прав на доступ к действиям над ресурсом
     *
     * @param string $action
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return bool
     */
    public function checkAccess(string $action, Model $model)
    {
        return Gate::check($action, $model);
    }

    /**
     * Фильтрует и возвращает данные
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $relationship
     * @param \App\Models\Language|null $forLanguage
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getFiltredData(Model $model, string $relationship, $forLanguage = null) : Builder
    {
        $query = $model::query();

        if(! is_null($forLanguage)) {
            $query->ofLanguage($forLanguage->id);
        }
        $query->whereHas($relationship, function($query) {
                $query->whereIn('slug', auth()->user()->getSetting('countries'));
        })->has($relationship, '<', 1, 'or');

        return $query;
    }

    /**
     * @param  Builder $builder
     * @param  string  $relationship
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAllowedFiltred(Builder $builder, string $relationship) : Builder
    {
        $builder->whereHas($relationship, function($query) {
                $query->whereIn('slug', auth()->user()->getSetting('countries'));
        })->has($relationship, '<', 1, 'or');

        return $builder;
    }

    /**
     * !!!Предпочтительный язык пользователя!!!
     *
     * Возвращает данные для построения списка.
     * Присутствует возможность получить список
     * только по конкретной стране
     *
     * @param integer|null $forCountry
     * @return array
     * @todo еще подумать над реализацией (возможно с передачей параметров)
     */
    public function getFiltredUserLocaleDataForSelect() : array
    {
        $result = $this->getFiltredData(new Language, 'country')
            ->get(['slug as text'])
            ->map(function($item) {
                return array_map('getUserLanguage', $item->toArray());
            })
            ->unique('text')
            ->map(function($item) {
                 $item['locale'] = array_map('addUserLanguageText', [$item], [['text']])[0];

                return $item;
            })
            ->pluck('locale', 'text')
            ->all();

        return $result;
    }
}