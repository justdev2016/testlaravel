<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Create\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `repository`
 */
class RepositoryConfig implements ActionContract
{
    /** @var array */
    const REPOSITORY = [
        'dependencies' => []
    ];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $repository;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->repository = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        foreach (self::REPOSITORY as $type => $data) {
            $this->repository->put($type, $data);
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('repository', $this->repository);
    }
}