<?php

namespace App\Services\Admin\Contents\Items\Employees\Api\Custom;

use Actions;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Выполнение инициализации настроек для `Web`.`Employees`.`Index`
 * @todo DRY - support methods
 */
class UpdateReOrderConfigAction implements ActionContract
{
    /** @var array */
    const REPOSITORIES = [
        'main' => [
            'set' => [
                'update' => [
                    'method' => 'updateOrders',
                    'params' => 'request'
                ],
            ],
        ],
    ];

    /** @var array */
    const RELATIONS = [];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $response->put('repository', self::REPOSITORIES);
        $response->put('relations', self::RELATIONS);

        return $response;
    }
}