@foreach($actions as $alias => $action)
	@php
        $class = $alias === 'delete' ? 'danger destroy-item' : 'success';

        $attributes = [
            'class' => "btn btn-{$class}",
            "role"  => "button"
        ];
    @endphp

	{!!
		link_to_route($routes->get($action), $data->get($alias), [$routeParam], $attributes)
	!!}
@endforeach