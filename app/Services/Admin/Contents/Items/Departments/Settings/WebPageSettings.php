<?php

namespace App\Services\Admin\Contents\Items\Departments\Settings;

use Illuminate\View\View;
use App\Services\Admin\Contents\Core\Settings\BaseSettings;

/**
 * Настройки Blade-страниц для `Web`
 */
class WebPageSettings extends BaseSettings
{
    const DEFAULT = 'admin.departments.';
    /** @var string */
    const SUFFIX = 'resources.';

    public function __construct()
    {
        parent::__construct();

        $this->make();
    }

    protected function make()
    {
        $this->settings->put('prefix', self::DEFAULT . self::SUFFIX);
        $this->settings->put('suffix', self::DEFAULT . self::SUFFIX . 'includes.');
        $this->settings->put('delete-departments-modal', self::DEFAULT . 'includes.modal.delete-departments');
    }

    /**
     * Отправка данных пользователю
     *
     * @param  string $view
     * @param  array|null $data
     * @return \Illuminate\View\View
     */
    public function response(string $view, array $data = null) : View
    {
        $view = $this->makeView($view);

        return ! $data ? view($view) : view($view, $data);
    }

    /**
     * Получение имени файла представления
     *
     * @param string $page
     * @return string
     */
    private function makeView(string $page) : string
    {
        return "{$this->settings->get('prefix')}{$page}";
    }
}