<td>
  <div class="btn-group">
    @foreach($actions as $action)
      @if($action === 'create')
        {!!
            link_to_route($route . $action, trans('actions.' . $action), [], [
                "class" => "btn btn-default {$action}-{$type}",
                "role" => "button"
            ])
        !!}
      @else
        {!!
            link_to_route($route . $action, trans('actions.' . $action), $routeParam, [
                "class" => "btn btn-default {$action}-{$type}",
                "role"  => "button"
            ])
        !!}
      @endif
  @endforeach
  </div>
</td>