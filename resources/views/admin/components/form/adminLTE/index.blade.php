<div id="app">
	@include($component->viewComponent('open'), $component->form())
		{{-- @todo улучшить передачу данных + доступ к ним + дополнить данные --}}
		{{-- @todo [имена всех представлений, методы получения основных данных] --}}
		@foreach ($component->fields() as $key => $field)
			@php
				$name = $field->field('name');
			@endphp

			<div class="row">
			    <div class="col-md-6">
					@include($component->view($field->field()), [
						'data'     => $component->fields($key),
						'settings' => $component->settings($key, $field->field()),
						'value'    => $component->value("base.main.{$name}", '')
					])
			    </div>
			</div><!-- /.row -->
		@endforeach

		{{-- Buttons --}}
		<div class="form-group buttons">
			<p>
				@foreach ($component->buttons('form.buttons') as $button)
					@php
						$key = key($button->all());
					@endphp
					@include($component->viewComponent($key), ['data' => $button->getData($key)])
				@endforeach
			</p>
		</div><!-- /.buttons -->

	{!! Form::close() !!}
</div>