<?php

namespace App\Providers\Repositories;

use Illuminate\Support\ServiceProvider;

use App\Repositories\EmployeeRepository;

class EmployeeRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(EmployeeRepository::class, function ($app) {
            return new EmployeeRepository($app);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [EmployeeRepository::class];
    }

}
