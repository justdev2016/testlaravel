<?php 

namespace App\Services\Admin\Managers\Info;

use Illuminate\Support\Collection;
use App\Contracts\Managers\Info\BaseInfo;
use App\Services\Admin\Managers\Info\ProfileInfo;
use App\Contracts\Managers\Info\Info as InfoContract;

/**
 * Предоставление информации о файлах/папках
 * и действия пользователя над файлами/папками
 */
class Info extends ProfileInfo implements BaseInfo
{
    /** @var \Illuminate\Support\Collection */
    protected $info;

    /**
     * @param  \App\Contracts\Managers\Info\Info $options
     * @return $this
     */
    public function make(InfoContract $options)
    {
        $this->info = $options;

        return $this;
    }

    public function __get($name)
    {
        $keys = $this->profiles->keys()->all();
        
        if (! in_array($name, $keys)) {
            throw new \Exception(trans('message.no-profile-info'), 1);
        }

        return $this->__call($name, []);
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->info, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->info, $method], $arguments);
    }
}