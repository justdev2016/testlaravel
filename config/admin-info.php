<?php

return [
    // Info Settings
    'data' => [
        'instance' => App\Services\Admin\Contents\Core\Components\Info\Info::class,
        'drivers' => [
            'adminLTE' => [
                'active' => true,
                'instance' => App\Services\Admin\Contents\Core\Components\Info\AdminLTE\AdminLTE::class,
                'renders' => [
                    // 'raw' =>
                ]
            ]
        ]
    ],
];