<?php

namespace App\Repositories;

use DB;
use App\Department;
use App\Traits\Admin\ACLTrait;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\Admin\Contents\Presentable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as Collect;
use Prettus\Repository\Eloquent\BaseRepository;
use App\Exceptions\Admin\SaveDepartmentException;
use App\Exceptions\Admin\DeleteDepartmentException;
use App\Services\Admin\Contents\Core\Support\Input;

class DepartmentRepository extends BaseRepository
{
    use ACLTrait, Presentable;

    /**
     * @return mixed
     */
    public function model()
    {
        return Department::class;
    }

    /**
     * @param  Department $department
     * @return self
     */
    public function setModel(Department $department)
    {
        $this->model = $department;

        return $this;
    }

// RESOURCE

    /**
     * @return mixed
     */
    public function index(Collect $request)
    {
        if ($request->get('request')->has('limit')) {
            $this->setModel(new Department);

            $data = $this->model->paginate((int)$request->getData('request.limit'));

            $newCollection = $data->getCollection()->present($request->get('present'));

            return $data->setCollection($newCollection);
        }
    }

    /**
     * @param  string $presenter
     * @return \Illuminate\Support\Collection
     */
    public function show(string $presenter) : Collect
    {
        return $this->model->present($presenter)->get();
    }

    /**
     * @param  string $presenter
     * @return \Illuminate\Support\Collection
     */
    public function edit(string $presenter) : Collect
    {
        return $this->model->present($presenter)->edit();
    }

    /**
     * @param Input|Collect $request
     * @throws DeleteDepartmentException
     */
    public function destroy(Input $request)
    {
        if (! $request->model('departments')->delete()) {
            throw new DeleteDepartmentException(trans('departments.delete-error-message'), 1);
        }
    }

    /**
     * @param  \Illuminate\Support\Collection $request
     * @throws \App\Exceptions\Admin\SaveDepartmentException
     * @return Department
     */
    public function store(Collect $request)
    {
        $department  = $this->makeModel();

        $department->name = $request->get('name');

        if (! $department->save()) {
            throw new SaveDepartmentException(trans('message.save-error-message'), 1);
        }

        return $department;
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Support\Input $request
     * @throws \App\Exceptions\Admin\SaveDepartmentException
     * @return Department
     */
    public function updateDepartment(Input $request)
    {
        $department = $request->model('departments');
        $request    = $request->request();

        $department->name = $request->get('name');

        if (! $department->save()) {
            throw new SaveDepartmentException(trans('message.save-error-message'), 1);
        }

        return $department;
    }

    /**
     * @param  array $params
     * @return void
     */
    public function syncData(array $params)
    {
        // $model, $relation, $ids
        extract($params);

        $model->{$relation}()->sync($ids);
    }

    /**
     * @param  Department  $department
     * @return void
     */
    public function cleanDepartmentCountries(Department $department)
    {
        $ids = $this->departmentCountriesPrimary($department)->toArray();

        $department->departments()->detach($ids);
    }

    /**
     * @param  Department  $department
     * @return \Illuminate\Support\Collection
     */
    public function departmentCountriesPrimary(Department $department) : Collect
    {
        return collect($department->departments()->get()->modelKeys());
    }

    /**
     * Возвращает данные для построения списка
     *
     * @return \Illuminate\Support\Collection
     */
    public function dataForSelect() : Collect
    {
        return $this->makeModel()->lists('name','id');
    }

    /**
     * @param  array $ids
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getDataById(array $ids) : Builder
    {
        return $this->makeModel()->whereIn('id', $ids);
    }

    /**
     * Возвращает PrimaryKey по ключу `id`
     *
     * @param  array $ids
     * @return \Illuminate\Support\Collection
     */
    public function getPrimaryById(array $ids) : Collect
    {
        return collect($this->getDataById($ids)->get(['id'])->modelKeys());
    }
}