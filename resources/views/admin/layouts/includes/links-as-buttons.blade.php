@foreach($parameters['actions'] as $action)
    <a
        class="btn btn-{{ ($action == 'delete') ? 'danger' : 'success' }} {{ $action }}"
        href="{{ route("admin.content.{$parameters['model']}.{$action}", $parameters['route-param'] ?? [])}}"
        role="button"
    >
        @lang('actions.' . $action)
    </a>
@endforeach

