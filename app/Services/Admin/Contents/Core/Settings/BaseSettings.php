<?php

namespace App\Services\Admin\Contents\Core\Settings;

use Illuminate\Support\Collection;

/**
 * Базовый класс для `ContentSettings`
 * @todo получение instance (optional) класса по требованию (lazy)! Через collect()->macro(`instance`) в dynamicInstance()
 */
class BaseSettings
{
    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var array */
    const METHODS = ['getData', 'content', 'hasContent'];

    public function __construct()
    {
        $this->settings = collect();
    }

    public function __call($method, $arguments)
    {
        if (starts_with($method, 'instance')) {
            return $this->dynamicInstance($method, $arguments);
        }

        if (method_exists($this->settings, $method) || in_array($method, self::METHODS)) {
            return call_user_func_array([$this->settings, $method], $arguments);
        }

        if ($this->settings->has($method)) {
            return $this->settings->get($method);
        }

        throw new \Exception(trans('message.method-not-exist'), 1);
    }

    /**
     * Получение экземпляра класса `Content`
     *
     * @param  string $content
     * @param  array $params
     * @return void
     */
    protected function dynamicInstance(string $content, array $params)
    {
        $type = strtolower(substr($content, 8));

        foreach ($this->settings->get($type) as $name => $setting) {
            $instance[$name] = new $setting;
        }

        $this->settings = collect($instance);
    }
}