<tbody>
    	<tr v-for="(content, index) in contents" :key="content.id">
        <td v-for="item in fields" v-cloak>@{{ content[item] }}</td>
        <td>
          <div class="btn-group" role="group" aria-label="..." v-cloak>
            <a :href="`{{ $page->type() }}/${content.id}/edit`" role="button" class="btn btn-default">Update</a>
            <a :href="`{{ $page->type() }}/${content.id}`" role="button" class="btn btn-default">Read</a>
            <a :href="`api/{{ $page->type() }}/${content.id}`" role="button" class="btn btn-default" @click.prevent="del(index, $event)">Delete</a>
          </div>
        </td>
    	</tr>
</tbody>