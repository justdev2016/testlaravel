import { fixTableSortable } from "../functions/fixTableSortable"

export default {
    containment: "parent",
    tolerance: "pointer",
    helper: fixTableSortable,
    revert: 50,
    items: "tr",
    axis: "y",
    placeholder: "ui-state-highlight",
    cursor: "move"
}