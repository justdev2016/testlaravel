<?php

namespace App\Services\Admin\Contents\Core\Components\Form\AdminLTE;


use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Создание пользовательского интерфейса для построения `Form`
 */
class FormManager
{
    /** @var string */
    const TYPE = 'form';

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return self
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $this->data     = new FormManagerData($data);
        $this->settings = new FormManagerSettings($settings);

        return $this;
    }

    /**
     * @param  string|null $name
     * @return string
     * @todo обработка ошибок
     */
    public function view(string $name = null) : string
    {
        return $this->settings->view($name);
    }

    /**
     * @param  string $name
     * @return string
     */
    public function viewComponent(string $name) : string
    {
        return $this->settings->settings()->getData("views.components.{$name}");
    }

    /**
     * Вернуть настройки для `form.open`
     *
     * @return array
     */
    public function form() : array
    {
        $action = $this->settings->actions()->get('update.put', $this->settings->actions()->get('create.post'));

        return [
            'type'       => $this->settings->settings('type'),
            'action'     => $action,
            'settings'   => $this->settings->settings()->getData('form.open'),
            'routeParam' => $this->data->getData('base.main.id'),
            'data_item'  => $this->data->getData('base')->has('main')
                ? $this->data->getData('base.main')->toJson()
                : null,
            'data_dep'  => $this->data->getData('base')->has('dependencies')
                ? $this->data->getData('base.dependencies')->toJson()
                : null
        ];
    }

    /**
     * Вернуть данные для `form`
     *
     * @param  string $name
     * @return mixed
     */
    public function data(string $name)
    {
        return $this->data->getData($name);
    }

    /**
     * Проверка наличия иныормации для `Form`
     *
     * @return bool
     */
    public function hasInfo() : bool
    {
        return ! $this->settings->settings()->getData('form.info')->isEmpty();
    }

    /**
     * Вернуть информацию для формы по имени
     *
     * @param  string $type
     * @return array
     */
    public function info(string $type) : array
    {
        return $this->settings->settings()->getData("form.info.{$type}")->toArray();
    }

    /**
     * Вернуть поля для `Form`
     *
     * @param  mixed|null $name
     * @return mixed
     */
    public function fields($name = null)
    {
        return ! isset($name)
            ? $this->settings->settings()->getData('form.fields')
            : $this->settings->columns('base')->getData($name);
    }

    /**
     * Вернуть значение для `Form`
     *
     * @param  string $name
     * @return mixed
     */
    public function value(string $name, $default = null)
    {
        return $this->data->getData($name, $default);
    }

    /**
     * Вернуть настройки для `Form`
     *
     * @param  mixed $index
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    public function settings($index, string $value) : Collection
    {
        return $this->fields()->get($index)->getData($value);
    }

    /**
     * Вернуть кнопки для `Form`
     *
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    public function buttons(string $name) : Collection
    {
        return $this->settings->settings()->getData($name);
    }
}