<?php

namespace App\Services\Admin\Contents\Items\Departments\Support;

use App\Contracts\Admin\ContentEntryContract;

/**
 * Точка инициализации действия для страници
 * Передача управления `Content`
 */
class Entry implements ContentEntryContract
{
    /** @var \Illuminate\Support\Collection */
    protected $config;

    /** @var string */
    protected $action;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = collect($config);
    }

    /**
     * @param  string $action
     * @param  string $type
     * @return objest
     */
    public function make(string $action, string $type)
    {
        $this->action = $this->config->getData("{$type}.{$action}");

        return $this->instance();
    }

    /**
     * @return object
     */
    private function instance()
    {
        return app($this->action);
    }
}