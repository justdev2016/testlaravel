<?php

namespace App\Traits\Admin\Contents;

trait Presentable
{
    /**
     * Present this instance using the provided Presenter class
     *
     * @param  string $presenter
     * @return Presenter
     */
    public function present($presenter)
    {
        return present($this, $presenter);
    }
}