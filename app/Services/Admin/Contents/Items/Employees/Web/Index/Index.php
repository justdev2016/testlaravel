<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Index;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Support\Input;
use App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings;
use App\Services\Admin\Contents\Core\Settings\{WebAction,WebConfigAction};

/**
 * Логика формирования данных для страницы `INDEX`
 *
 * @todo доделать multi-компонентность на странице
 */
class Index implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var array */
    protected $config;

    /**
     * @param \App\Services\Admin\Contents\Core\Support\Input $params
     */
    public function __construct(Input $params)
    {
        $this->config = [
            'settings'  => new ContentSettings('web'),
            'namespace' => __NAMESPACE__,
            'config'    => collect(config('employees.components.index')),
            'requests'  => $params,
        ];
    }

    /**
     * Инициализация настроек для выполняемого действия
     *
     * @return array
     */
    public function init()
    {
        $this->object = new WebConfigAction($this->config);
        $this->object = new IndexConfigAction($this->object);
        $this->object = new WebAction($this->object);

        return $this;
    }

    /**
     * @return array
     */
    public function action() : array
    {
        return $this->object->action();
    }
}