<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Show\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `settings`
 */
class RoutesConfig implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $routes;

    public function __construct(ActionContract $object)
    {
        $this->object = $object->action();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->routes = $this->object->getData('settings')->map(function($item, $type) {
            return collect([$type => $this->makeRoutes($type)]);
        });


    }

    /**
     * @param  string $type
     * @return array|void
     */
    private function makeRoutes(string $type)
    {
        return $this->object->getData("settings.{$type}.routes");
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        return $this->object->put('routes', $this->routes);
    }
}