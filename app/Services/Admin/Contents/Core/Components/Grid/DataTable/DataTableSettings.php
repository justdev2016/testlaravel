<?php

namespace App\Services\Admin\Contents\Core\Components\Grid\DataTable;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\Grid\BaseGrid;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Валидация и подготовка данных
 *
 * @todo реализовать функционал применение настроек в зависимости от логики страницы
 * @todo использование множественных компонентов одного типа на одной странице
 */
class DataTableSettings extends BaseGrid
{
    /** @var string */
    const DRIVER = 'dataTable';

    /** @var array (required) */
    const SETTINGS = [
        'component' => false,
        // (optional - default `vue`)
        'driver'    => 'vue',
    ];

    /** @var array (required) */
    const VIEWS = [
        'raw'        => 'index',
        'vue'        => 'vue.index',
        'javascript' => 'includes.javascript',
        'suffix'     => 'dataTable.includes',
    ];

    /** @var array Settings for front-end */
    const JAVASCRIPT = [
        'paging'          => true,
        'ordering'        => false,
        'searching'       => false,
        'filter_employee' => true,
        'scrollCollapse'  => false,
    ];

    /** @var array */
    const PAGING = [
        "lengthMenu" => [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "paging"     => true
    ];

    /** @var array */
    const ORDERING = [
        "order" => [[2, 'asc']]
    ];

    const SCROLLCOLLAPSE = [
        "scrollY"        => "550px",
        "scrollCollapse" => true,
    ];

    /** @var array Режим сортировки */
    const SORTABLE = [
        "active" => true,
        "mode"   => "auto",
    ];

    /** @todo временное решение - только для testlaravel */
    const FILTER_EMPLOYEE = [
        'filter_employee' => [
            'employee_date'    => ['from', 'to'],
            'full_name'        => ['like'],
            'phone'            => ['like'],
            'email'            => ['like'],
            'departments_list' => ['like']
        ]
    ];

    /** @var \Illuminate\Support\Collection */
    protected $columns;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $actions;

    /** @var \App\Services\Admin\Contents\Core\Components\ComponentDependence */
    protected $raw_settings;

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     */
    public function __construct(ComponentDependence $settings)
    {
        $this->columns      = collect();
        $this->settings     = collect();
        $this->raw_settings = $settings;


        $this->makeSettings();

        $this->make($settings);
    }

    // INIT

    /**
     * Global Settings
     *
     * @return void
     */
    protected function makeSettings()
    {
        $this->makeViewSettings();
        $this->makeJavaScript();
    }

    /**
     * @return void
     */
    protected function makeViewSettings()
    {
        $settings = self::SETTINGS;

        foreach (self::VIEWS as $type => $view) {
            $settings[$type] = $this->getView($type);
        }

        foreach (self::VIEW_COMPONENTS as $item) {
            $settings['components'][$item] = $this->getViewComponents(self::VIEWS['suffix'], $item);
        }

        $this->settings->put('views', collect($settings));
    }

    protected function makeJavaScript()
    {
        $javascript = [];

        foreach (self::JAVASCRIPT as $key => $value) {
            $const = 'self::' . strtoupper($key);

            if ($value && defined($const)) {
                $javascript = array_merge($javascript, constant($const));
            } else {
                $javascript[$key] = $value;
            }
        }
        $this->settings->put('javascript', collect($javascript));
        // Sorting
        if (self::JAVASCRIPT['ordering']) {
            $this->settings->put('sortable', collect(self::SORTABLE));
        }
        $local = $this->raw_settings->data->get('local');

        if (! $local->getData('config.data')) {
            $this->settings->put('sync', $local->get('sync'));
        }
        // @todo применить, если `remote` = true
        // $this->settings->put('javascript', self::SETTINGS['component'] ? collect(self::JAVASCRIPT) : collect());
    }

    /**
     * @param  \App\Services\Admin\Contents\Items\Employees\Settings\ContentSettings $settings
     * @param  \Illuminate\Support\Collection $pageSettings
     * @return self
     */
    public function make(ComponentDependence $settings)
    {
        $global = $settings->data->get('global');
        $local  = $settings->data->get('local');

        $locale   = $global->content('locale.main');

        $columns  = $local->getData('fields')->columns()->trans('text', $locale);

        $this->columns->put('base', $columns);

        $this->columns->put('actions', collect([$this->actionsColumn()])->trans('text'));

        $this->actions = $local->get('actions');

        $this->settings->put('actions', $global->only(['type']));

        return $this;
    }

    public function view(string $param = null) : string
    {
        if (! $param && $this->isComponent()) {
            $driver = $this->settings->get('driver');

            return $this->settings->content("views.{$driver}");
        }

        if ($param) {
            return $this->settings->getData("views.components.{$param}");
        }

        return $this->settings->getData('views.raw');
    }

    /**
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    public function columns(string $name = null) : Collection
    {
        return ! isset($name)
            ? $this->columns
            : $this->columns->getData($name);
    }

    /**
     * @param  string $name
     * @return mixed
     */
    public function actions(string $name = null)
    {
        return ! isset($name)
            ? $this->actions
            : $this->actions->get($name);
    }

    /**
     * @param  string|null $param
     * @return mixed
     */
    public function settings(string $param = null)
    {
        return $param ? $this->settings->getData($param) : $this->settings;
    }

    public function isComponent() : bool
    {
        return $this->settings->get('component', false);
    }

    // PRIVATE

    /**
     * @param  string $param
     * @return string
     */
    private function getView(string $param) : string
    {
        return implode('.', [
           self::BASE_VIEW, // admin.components
           self::TYPE, // grid
           self::DRIVER, // dataTable
           self::VIEWS[$param] // index
        ]);
    }

    /**
     * Получение драйвера компонента
     *
     * @return string
     */
    private function getComponentDriver() : string
    {
        return $this->settings->get('driver', 'vue');
    }

    /**
     * @return array
     */
    private function actionsColumn() : array
    {
        return [
            'field' => 'actions',
            'text'  => 'actions.actions'
        ];
    }
}