<?php

namespace App\Traits\Admin\Contents;

use Illuminate\Support\Collection;

/**
 * Трейт получения данных из констант класса
 */
trait Setter
{
    /**
     * @param  string $path
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    private function setter(string $path, string $name) : Collection
    {
        return collect($this->settings->getData("repository.{$path}.set.{$name}"));
    }
}