<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Edit;

use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Settings\EmptyCollectionAction;
use App\Services\Admin\Contents\Items\Employees\Web\Edit\Config\{
    ActionsConfig,
    FieldsConfig,
    FormInfoConfig,
    FormViewConfig,
    RepositoryConfig,
    SettingsConfig
};

/**
 * Выполнение инициализации настроек для `Web`.`Employees`.`Edit`
 */
class EditConfigAction implements ActionContract
{
    /** @var array */
    const RELATIONS_FIELDS = [];

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = new ActionsConfig(new EmptyCollectionAction);
        $this->settings = new FieldsConfig($this->settings);
        $this->settings = new RepositoryConfig($this->settings);
        $this->settings = new SettingsConfig($this->settings);
        $this->settings = new FormInfoConfig($this->settings);
        $this->settings = new FormViewConfig($this->settings);

        $this->settings = $this->settings->action();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $components = $response->getData('config')->map(function($item, $type) {
            $item = collect([
                'driver'     => $item['driver'],
                'config'     => $item['config'] ?? [],
                'actions'    => $this->settings->getData("actions.{$type}"),
                'fields'     => $this->settings->getData("fields.{$type}"),
                'relations'  => $this->settings->getData("relations"),
                'repository' => $this->settings->getData("repository"),
                'settings'   => $this->settings->getData("settings.{$type}"),
            ]);

            return $item;
        });

        $response->get('settings')->put('page', 'edit');
        $response->get('settings')->put('action', 'edit');

        $response->put('components', $components);

        return $response;
    }
}