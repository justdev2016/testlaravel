<?php

namespace App\Services\Admin\Contents\Core\Settings;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Инициализация настроек для `Web`.`Config`
 */
class WebConfigAction implements ActionContract
{
    /** @var \Illuminate\Support\Collection */
    protected $config;

    public function __construct(array $config)
    {
        $this->config = collect($config);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        return $this->config;
    }
}