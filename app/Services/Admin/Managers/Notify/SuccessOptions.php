<?php

namespace App\Services\Admin\Managers\Notify;

use Illuminate\Support\Collection;
use App\Validators\AdminNotifyValidator;
use App\Services\Admin\Managers\Notify\BaseOptions;

/**
 * Success options
 */
class SuccessOptions extends BaseOptions
{
    /** @const успешный статус сообщения: УСПЕХ */
    const STATUS_SUCCESS = 'success';

    /** @var \Illuminate\Support\Collection */
    public $notify;

    /**
     * Допустимые параметры и их типы:
     * [
     *     'message' => string => required,
     *     'status'  => string
     *     ВАЖНО! Если передавать данные, то имя ключа и тип данных
     *            можно установить произвольными
     * ]
     * @param      array  $data   The data
     */
    public function __construct(array $data = [])
    {
        parent::__construct();

        $this->notify = collect();

        if (! empty($data)) {
            $this->make($data);
        }
    }

    /**
     * Инициализация сообщения
     * Допустимые значения и их типы:
     * [
     *     message => string => required,
     *     status  => string => default:'success'
     * ]
     *
     * @param array $data
     * @return self
     */
    public function make(array $data)
    {
        $this->validate($data, AdminNotifyValidator::RULE_ANY);

        if (isset($data['message'])) {
            $this->notify->put('message', $data['message']);
        }
        $this->notify->put('status', $data['status'] ?? self::STATUS_SUCCESS);
        $this->notify->put('data', $data['data'] ?? false);

        return $this;
    }
}