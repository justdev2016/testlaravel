@include($component->viewComponent('open'), $component->form())
    {{-- @todo улучшить передачу данных + доступ к ним + дополнить данные --}}
    {{-- @todo [имена всех представлений, методы получения основных данных] --}}
    @foreach ($component->fields() as $key => $field)
        @php
            $name = $field->field('name');
        @endphp

        @if (! $component->data('base.dependencies')->has($name))
            <div class="row">
                <div class="col-md-6">
                    @include($component->view($field->field()), [
                        'data'     => $component->fields($key),
                        'settings' => $component->settings($key, $field->field()),
                        'value'    => $component->value("base.main.{$name}", '')
                    ])
                </div>
            </div><!-- /.row -->
        @else
            @php
                $setting = $component->settings($key, $field->field())->get('settings');
                $method  = $setting['method'];
            @endphp

            @if (isset($method))
                {{-- @todo оптимизировать "{$page->view('views.suffix')}.{$method}-{$name}" --}}
                @include("{$page->view('views.suffix')}.{$method}-{$name}", [
                    'settings' => $setting
                ])

                {{-- @todo оптимизировать "{$page->view('views.suffix')}.{$method}-{$name}-hidden" --}}
                @include("{$page->view('views.suffix')}.{$method}-{$name}-hidden", [
                    'data'     => $component->fields("{$name}.slug"),
                    'list'     => $component->data("base.dependencies.{$name}"),
                    'settings' => $component->settings($key, $field->field())
                ])
            @endif


            @if (! empty($component->data("base.main.{$name}")))
                <div class="row">
                    <div class="col-md-4">
                        @include($component->view($field->field()), [
                            'data'     => $component->fields($name),
                            'list'     => $component->data("base.dependencies.{$name}"),
                            'settings' => $component->settings($key, $field->field()),
                            'value'    => $component->value("base.main.{$name}.*")
                        ])
                    </div>
                </div><!-- /.row -->
            @endif
        @endif
    @endforeach

    {{-- Buttons --}}
    <div class="form-group buttons">
        <p>
            @foreach ($component->buttons('form.buttons') as $button)
                @php
                    $key = key($button->all());
                @endphp
                @include($component->viewComponent($key), ['data' => $button->getData($key)])
            @endforeach
        </p>
    </div><!-- /.buttons -->

{!! Form::close() !!}