<?php

namespace App\Services\Admin\Contents\Core\Support;

/**
 * Процесс получения и синжжронизации данных с представлением
 */
class DataLoader
{
    /** @var string */
    const GLOBAL = '@';

    /** @var string */
    const LOCAL  = '#';

    /** @var \Illuminate\Support\Collection */
    private static $config;

    /**
     * @param  array|null $config
     * @return self
     */
    public function make(array $config = null)
    {
        self::$config = collect($config['config']);

        return $this;
    }

    /**
     * @param  string $key
     * @return string
     */
    public function getMethod(string $key)
    {
        $config = self::$config->get($key);

        if ($this->isGlobal($key)) {
            return studly_case(str_replace(self::GLOBAL, '_global_', $config));
        }

        return studly_case(str_replace(self::LOCAL, '_local_', $config));
    }

    /**
     * @return  bool
     */
    public function needLoadData() : bool
    {
        return (bool)self::$config->get('data');
    }

    /**
     * @param  string $key
     * @return bool
     */
    private function isGlobal(string $key) : bool
    {
        return str_contains(self::$config->get($key), '@');
    }
}