<?php

namespace App\Providers\Admin\Components;

use Illuminate\Support\ServiceProvider;

class AdminInfoServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerCollectionMacros();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerInfoComponent();
    }

    /**
     * @return void
     */
    protected function registerInfoComponent()
    {
        /** Register Facade `Info` */
        $this->registerInfoFacade();
    }

    /**
     * @return void
     */
    protected function registerInfoFacade()
    {
        $this->app->singleton('info', function ($app) {
            // return new Menu($app['components.menu']);
        });
    }

    /**
     * @return void
     */
    private function registerCollectionMacros()
    {
        //
    }
}
