<?php

use Faker\Factory as Faker;

$factory->define(App\Employee::class, function () {
    $faker = Faker::create('ru_RU');
    $positions = [
        'маркетолог',
        'юрист',
        'бухгалтер',
        'менеджер',
        'администратор',
        'программист',
    ];
    $phones = [
        '1111111111',
        '2222222222',
        '3333333333',
        '4444444444',
        '5555555555',
        '6666666666',
        '7777777777',
        '1234554321',
        '1122334455',
    ];

    $employee_date = [
      '2011-10-25',
      '2014-10-20',
      '2001-05-10',
      '2016-10-30',
      '2002-05-15',
      '2016-11-11',
      '2003-03-05',
    ];

    $surnames = [
        'Иванович',
        'Петрович',
        'Владимирович',
        'Александрович',
        'Всеволодович',
        'Дмитриевич',
    ];

    return [
        'surname' => $surnames[array_rand($surnames)],
        'first_name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'email' => $faker->email,
        'phone' => $phones[array_rand($phones)],
        'birthday' => $faker->date($format = 'Y-m-d', $max = '2000-01-01'),
        'employee_date' => $employee_date[array_rand($employee_date)],
        'position' => $positions[array_rand($positions)],
    ];
});