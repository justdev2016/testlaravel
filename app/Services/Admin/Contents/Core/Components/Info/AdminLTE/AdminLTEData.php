<?php

namespace App\Services\Admin\Contents\Core\Components\Info\AdminLTE;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Формирование данных
 */
class AdminLTEData
{
    /** @var string */
    const TYPE = 'info';

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     */
    public function __construct(ComponentDependence $data)
    {
        $this->make($data);
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->data, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->data, $method], $arguments);
    }

    /**
     * @param  \Illuminate\Support\Collection  $base
     * @param  mixed $global
     * @return void
     * @todo валидация `settings` + подумать над параметром $actions
     */
    private function make(ComponentDependence $data)
    {
        $this->data = $data->data->map(function($item, $key) {
            if ($key === 'base') {
                $item = $item
                    ->dateFormat('small');
            }

            if ($key === 'actions') {
                $item = $item
                    ->except(['create.get'])
                    ->map(function($item) {
                        return trans("actions.{$item}");
                    });
            }

            return $item;
        });
    }

    /**
     * @param  string $name
     * @return mixed
     */
    public function data(string $name = null)
    {
        return ! isset($name)
            ? $this->data
            : $this->data->getData($name);
    }
}