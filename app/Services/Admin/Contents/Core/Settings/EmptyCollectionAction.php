<?php

namespace App\Services\Admin\Contents\Core\Settings;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Не выполняет никаких действий
 */
class EmptyCollectionAction implements ActionContract
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        return collect();
    }
}