{{-- AdminLTE App --}}
{{-- 
      Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout.
--}}
<script src="{{ asset("{$jsPath}common.js") }}"></script>
<script src="{{ asset("{$jsPath}libs.js") }}"></script>

<script>
  // Ajax Config
  $(function() {
      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN':'{{ csrf_token() }}'
          }
      });
  });
  // user locale
  var locale = "{{ $locale }}";
</script>
@stack('jsGlobals')
@stack('scripts')