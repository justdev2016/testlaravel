<?php

namespace App\Contracts\Admin;

interface ActionContract
{
    public function action();
}