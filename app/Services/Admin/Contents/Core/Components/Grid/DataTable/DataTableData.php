<?php

namespace App\Services\Admin\Contents\Core\Components\Grid\DataTable;

use Actions;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Формирование данных
 */
class DataTableData
{
    /** @var string */
    const TYPE = 'grid';

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /**
     * @param \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     */
    public function __construct(ComponentDependence $data)
    {
        $this->make($data);
    }

    public function __call($method, $arguments)
    {
        if (! method_exists($this->data, $method)) {
            throw new \Exception(trans('message.method-not-exist'), 1);
        }

        return call_user_func_array([$this->data, $method], $arguments);
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @return void
     */
    private function make(ComponentDependence $data)
    {
        $this->data = $data->data->map(function($item, $key) {
            if ($key === 'actions') {
                $item = $item
                    ->except(['create.get'])
                    ->map(function($item) {
                        return trans("actions.{$item}");
                    });
            }

            return $item;
        });
    }

    /**
     * @param  string $name
     * @return \Illuminate\Support\Collection
     */
    public function data(string $name = null) : Collection
    {
        return ! isset($name)
            ? $this->data
            : $this->data->getData($name);
    }
}