<?php

namespace App\Services\Admin\Contents\Core;

use App\Services\Admin\Support\Web\Decorator;

/**
 * Точка входа для `Web`
 */
class Web extends Decorator
{
    /**
     * @return void
     */
    public function make()
    {
        $this->action = $this->action->init('web');
    }
}