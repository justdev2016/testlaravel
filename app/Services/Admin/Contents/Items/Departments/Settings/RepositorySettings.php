<?php

namespace App\Services\Admin\Contents\Items\Departments\Settings;

use Repository;
use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Settings\BaseSettings;
use App\Repositories\{
    DepartmentRepository
};

/**
 * Настройки источников данных для всех страниц
 */
class RepositorySettings extends BaseSettings
{
    /** @var string */
    const MAIN = DepartmentRepository::class;

    /** @var array */
    const DEPENDENCIES = [
        // 'countries' => CountryRepository::class,
    ];

    /** @var \App\Repositories\DepartmentRepository */
    protected $settings;

    /** @var array */
    const METHODS = ['settings'];

    public function __construct()
    {
        $this->make();
    }

    private function make()
    {
        $settings = [
            'main'         => app(self::MAIN),
            'dependencies' => $this->makeDependencies()
        ];

        $this->settings = collect($settings);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function makeDependencies() : Collection
    {
        foreach (self::DEPENDENCIES as $name => $relation) {
            $settings[$name] = app($relation);
        }

        return collect($settings ?? []);
    }
}