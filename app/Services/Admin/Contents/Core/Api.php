<?php

namespace App\Services\Admin\Contents\Core;

use Illuminate\Support\Collection;
use App\Services\Admin\Support\Web\Decorator;

/**
 * Точка входа для `Api`
 */
class Api extends Decorator
{
    /**
     * @return void
     */
    protected function make()
    {
        $this->action = $this->action->init('api');
    }
}