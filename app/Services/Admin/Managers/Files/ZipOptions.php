<?php

namespace App\Services\Admin\Managers\Files;

use Illuminate\Support\Collection;
use Illuminate\Filesystem\Filesystem;
use App\Contracts\Managers\Files\Options;
use App\Validators\AdminFCommanderValidator;
use App\Services\Admin\Managers\Files\ArchiveInfo;
use App\Contracts\Managers\Files\Options as ZipOptionsContract;

/**
 * Archive options
 */
class ZipOptions extends BaseOptions implements ZipOptionsContract
{
    /** @var string|array */
    protected $src;

    /** @var string */
    protected $dst;

    /** @var \ZipArchive */
    protected $manager;

    /** @var \Illuminate\Filesystem\Filesystem */
    protected $filesystem;

    /** @var \Symfony\Component\HttpFoundation\File\File */
    protected $complete;

    /**
     * Допустимый формат данных
     * Допустимые значения и их типы:
     * [
     *     src    => array|object   => required,
     *     dst    => string         => required,              
     *     random => inteder        => optional              
     * ]
     * 
     * @param \Illuminate\Support\Collection $collection
     */
    public function __construct(Collection $collection = null)
    {
        parent::__construct();

        $this->complete   = collect();
        $this->manager    = new \ZipArchive;
        $this->filesystem = app(Filesystem::class);
        
        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    /**
     * Инициализация входных данных
     * 
     * @param  array $collection
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->validate($collection->toArray(), AdminFCommanderValidator::RULE_ARCHIVE_ANY);

        $this->src     = $collection->get('src');
        $this->dst     = $collection->get('dst', $this->filesystem->dirname($this->src));

        return $this;
    }
    
    /**
     * Получение информации о действии над файлами/папками
     * 
     * @param  \App\Contracts\Managers\Files\Options $options
     * @return App\Services\Admin\Managers\Info\Info
     */
    protected function info(Options $options)
    {
        return $this->info->make(new ArchiveInfo($options->complete));
    }

    /**
     * Создает zip-архив для файлов/папок
     *
     * @param boolean $info
     * @return void|\App\Services\Admin\Managers\Files\Info
     * @todo реализовать функционал
     */
    public function action(bool $info = true) : string
    {
        /*$zip = $this->manager;

        if ($zip->open($src) === TRUE) {
            $zip->extractTo($dst);
            $zip->close();
        } else {
            throw new ArchiveException(trans('messages.no-zip-error'), 1);
        }*/
    }
}