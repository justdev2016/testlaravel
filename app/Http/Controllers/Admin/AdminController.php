<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Traits\Admin\ACLTrait;

class AdminController extends Controller
{
    use ACLTrait;
}
