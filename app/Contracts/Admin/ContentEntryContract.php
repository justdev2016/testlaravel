<?php

namespace App\Contracts\Admin;

interface ContentEntryContract
{
    /**
     * @param string $action
     * @param string $type
     */
    public function make(string $action, string $type);
}