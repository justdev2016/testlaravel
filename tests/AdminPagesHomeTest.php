<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminPagesHomeTest extends TestCase
{

    public function login()
    {
        $user = \App\User::find(1);
        $this->be($user);
    }

// HOME
    public function testHomeSuccess()
    {
        $this->login();

        $this->call('GET', route('admin.pages.index'));

        $this->assertResponseOk();
    }
}