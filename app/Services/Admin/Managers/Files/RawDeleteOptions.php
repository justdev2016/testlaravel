<?php

namespace App\Services\Admin\Managers\Files;

use File;
use Illuminate\Support\Collection;
use App\Contracts\Managers\Files\Options;
use App\Validators\AdminFCommanderValidator;
// use App\Services\Admin\Managers\Files\DeleteInfo;
use App\Contracts\Managers\Files\Options as DeleteOptionsContract;

/**
 *  raw Delete actions options
 */
class RawDeleteOptions extends BaseOptions implements DeleteOptionsContract
{
    /** @var string|null */
    protected $dir;

    /** @var string|array */
    protected $data;

    /** @var string|bool */
    protected $ext;

    /** @var \Illuminate\Support\Collection */
    protected $complete;

    /**
     * Допустимый формат данных, значения и их типы:
     * [
     *     dir  => string,
     *     data => string|array => required,
     *     ext  => string
     * ]
     * 
     * @param \Illuminate\Support\Collection $collection
     */
    public function __construct(Collection $collection = null)
    {
        parent::__construct();

        $this->complete = collect();
        
        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    /**
     * Инициализация входных данных
     * 
     * @param  array $collection
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->validate($collection->toArray(), AdminFCommanderValidator::RULE_RAW_DELETE);

        $this->dir  = $collection->get('dir', null);
        $this->data = (array) $collection->get('data');
        $this->ext  = $collection->get('ext', false);

        $this->buildPath();

        return $this;
    }
    
    /**
     * Перемещений файлов/папок
     * 
     * @param bool $info
     * @return void
     */
    public function action(bool $info = true)
    {
        foreach ($this->complete->toArray() as $item) {
            if (File::exists($item)) {
                if (File::isDirectory($item)) {
                    $this->deleteFolder($item);
                }

                if (File::isFile($item)) {
                    $this->deleteFile($item);
                }
            }
        }

        if ($info) {
            return $this->info($this);
        }
    }

    /**
     * Удаление директории
     *
     * @param  string  $path
     * @throws \Exception
     * @return void
     */
    protected function deleteFolder(string $path)
    {
        if (! File::deleteDirectory($path)) {
            throw new \Exception(trans('message.no-delete-file-or-directory'), 1);
        }
    }

    /**
     * Удаление файла
     *
     * @param  string  $path
     * @throws \Exception
     * @return void
     */
    protected function deleteFile(string $path)
    {
        if (! File::delete($path)) {
            throw new \Exception(trans('message.no-delete-file-or-directory'), 1);
        }
    }

    /**
     * Получение информации о действии над файлами/папками
     * 
     * @param  \App\Contracts\Managers\Files\Options $options
     * @return App\Services\Admin\Managers\Info\Info
     */
    protected function info(Options $options)
    {
        // return $this->info->make(new MoveInfo($options->complete));
    }

    /**
     * Собирает все данные вместе, образуя путь к файлу/папке
     * 
     * @return void
     */
    protected function buildPath()
    {
        $dir = !empty($this->dir) ? str_finish(rtrim($this->dir, DIRECTORY_SEPARATOR), DIRECTORY_SEPARATOR) : '';
        foreach ($this->data as $value) {
            if ($this->ext) {
                $this->complete->push("{$dir}$value.{$this->ext}");
            }
        }
    }
}