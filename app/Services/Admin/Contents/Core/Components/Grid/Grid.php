<?php

namespace App\Services\Admin\Contents\Core\Components\Grid;

use Components;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Компонент `Grid`
 */
class Grid implements GridStrategy
{
    /** @var string */
    protected $driver;

    /**
     * @param string $driver
     */
    public function __construct(string $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return object
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $class = Components::driver('grid', $this->driver);

        return $class->make($data, $settings);
    }
}