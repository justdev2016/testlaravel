<?php

namespace App\Providers\Admin\Document;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Document\Document;
use App\Services\Admin\Document\DocumentComposite;
use App\Services\Admin\Contents\Core\Support\WidgetMapperRegistry;

class DocumentServiceProfider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSections();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'document','widgetMapper',
        ];
    }

    /**
     * @return void
     */
    private function registerSections()
    {
        /** `Document` Sections register */
        $this->registerDocumentSections();
        /** Register Facade `Document`, `WidgetMapper` */
        $this->registerFacades();
    }

    /**
     * @return void
     */
    private function registerDocumentSections()
    {
        $this->app['document.root'] = new DocumentComposite('root');

        $this->app['document.section'] = [
            'header'  => new DocumentComposite('header'),
            'footer'  => new DocumentComposite('footer'),
            'left'    => new DocumentComposite('left'),
            'content' => new DocumentComposite('content'),
        ];
    }

    /**
     * @return void
     */
    private function registerFacades()
    {
        $this->app->singleton('document', function ($app) {
            $root    = $app['document.root'];
            $config  = $app['config']['admin.document'];
            $section = $app['document.section'];

            return new Document($root, $config, $section);
        });

        $this->app->singleton('widgetMapper', function ($app) {
            $config = $app['config']['admin.map'];
            $router = $app['router'];

            return new WidgetMapperRegistry($config, $router);
        });
    }
}
