<div class="dataTables_wrapper form-inline dt-bootstrap">
  <div class="row">
    <div class="col-sm-12">
      <table id="courses" class="table table-bordered table-striped table-hover dataTable">
        <thead>
          <tr role="row">
            @include('admin.courses.includes.table.head', $data)
          </tr>
        </thead>
        <tbody>
          @foreach($data['courses'] as $key => $course)
            <tr>
              @foreach($data['table']['head'] as $column)
                @if($column == 'slug') @continue @endif
                <td class="{{ $column }}-field" data-id="{{$course->id}}">{{ $course->{$column} }}</td>
              @endforeach
              @if(hasTableActions($data['table']['actions']))
                @include('admin.layouts.includes.table.actions-column', [
                  'actions'    => $data['table']['actions'],
                  'route'      => 'admin.content.course.',
                  'type'       => 'course',
                  'routeParam' => ['slug' => $course->slug]
                ])
              @endif
            </tr>
          @endforeach
        </tbody>
        <tfoot>
          <tr>
            @include('admin.courses.includes.table.head', $data)
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
@if (isset($data['buttons']))
  @include('admin.layouts.includes.links-as-buttons', ['parameters' => $data['buttons']])
@endif
@if (hasTableActions($data['table']['actions']) && in_array('delete', $data['table']['actions']))
  @include('admin.courses.includes.modal.delete-course')
@endif