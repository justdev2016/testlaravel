<?php

namespace App\Traits\Admin\Contents;

use Illuminate\Support\Collection;

/**
 * Трейт получения данных из констант класса
 */
trait Getter
{
    /**
     * @param  string $path
     * @param  string|null $name
     * @return \Illuminate\Support\Collection
     */
    private function getter(string $path, string $name = null) : Collection
    {
        return $name ?
            collect($this->settings->getData("repository.{$path}.get.{$name}")) :
            collect($this->settings->getData("repository.{$path}.get"));
    }
}