<?php

namespace App\Providers\Services;

use Illuminate\Support\ServiceProvider;
use App\Services\Admin\Contents\Core\Defaults\{Actions,Components};

class AdminContentManagerServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCollectionMacros();
        $this->registerFacades();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'actions','components',
        ];
    }

    /**
     * @return void
     */
    protected function registerFacades()
    {
        $this->app->singleton('actions', function ($app) {
            return new Actions;
        });

        $this->app->singleton('components', function ($app) {
            return new Components($app['config']['admin.components']);
        });
    }

    protected function registerCollectionMacros()
    {
        collect()->macro('onlyWithPivot', function($baseFields, $relationFields = []) {
            return collect([$this->items])->map(function($item) use($baseFields, $relationFields) {
                return collect($item)
                    ->only($baseFields)
                    ->map(function($_item, $_key) use($relationFields) {
                        if (array_key_exists($_key, $relationFields)) {
                            $relation = $relationFields[$_key];

                            return collect($_item)->map(function($val) use($relation) {
                                return array_only($val, $relation);
                            })->all();
                        }

                        return $_item;
                    });
            })->first();
        });

        collect()->macro('field', function($name = null) {
            $item = $this->values()->first();

            if (! $name) {
                return $this->keys()[0];
            }

            return collect($item)->get($name);
        });

        /*collect()->macro('hasContent', function($name) {
            $param  = explode('.', $name);
            $result = $this->items;

            foreach ($param as $value) {
                if (is_array($result)) {
                    $result = collect($result);
                }

                if ($value === last($param)) {
                    break;
                }

                $result = $result->get($value);
            }

            return $result->has(last($param));
        });*/

        collect()->macro('r_search', function($name) {
            $search = false;

            $this->each(function($item, $key) use ($name, &$search) {
                $current = $key;

                if($name === $key OR (is_array($item) && collect($item)->r_search($name) !== false)) {
                    $search = true;

                    return false;
                }
            });

            return $search;
        });

        collect()->macro('key', function() {
            if (is_array($this->items)) {
                return key($this->items);
            }

            return $this->keys()->first();
        });

        collect()->macro('active', function() {
            return $this->filter(function($item) {
                return $item['active'] ?? $item;
            });
        });

        collect()->macro('instance', function() {
            return $this->map(function($class) {
                return new $class;
            });
        });

        collect()->macro('columns', function() {
            return $this->map(function($item, $key) {
                if (is_array($item)) {
                    return collect($item)
                        ->flip()
                        ->map(function($item, $key) {
                            return [
                                'field' => $key,
                                'text'  => $key
                            ];
                        });
                }

                return [
                    'field' => $item,
                    'text'  => $item
                ];
            });
        });

        collect()->macro('route', function($field) {
            return $this->map(function($item, $key) use ($field) {
                if ($key === $field) {
                    $item = route($item);
                }

                return $item;
            });
        });

        collect()->macro('trans', function($field, $file = null) {
            return $this->map(function($item, $key) use ($field, $file) {
                if (empty($file)) {
                    if ($key === $field) {
                        $item = trans($item);
                    } elseif (isset($item[$field])) {
                        $item[$field] = trans($item[$field]);
                    }
                } else {
                    if ($item instanceof \Illuminate\Support\Collection) {
                        return $item->trans($field, $file);
                    } else {
                        $item[$field] = trans("{$file}.{$item[$field]}");
                    }
                }

                return $item;
            });
        });

        collect()->macro('addClass', function($name, $after = true) {
            if (! $this->has('class')) {
                return $this->put('class', $name);
            }

            return $this->map(function($item, $key) use ($name, $after) {
                if ($key === 'class') {
                    $newClasses   = (array)$name;
                    $oldClasses[] = $item;

                    if ($after) {
                        $classes = array_merge($oldClasses, $newClasses);
                    } else {
                        $classes = array_merge($newClasses, $oldClasses);
                    }

                    $item = implode(' ', $classes);
                }

                return $item;
            });
        });

        collect()->macro('dateFormat', function($type, $fields = []) {
            return $this->map(function($item, $key) use ($type, $fields) {
                $fields = array_merge(['created_at', 'updated_at'], $fields);

                if (in_array($key, $fields)) {
                    $item = formatDateTime($item, $type);
                }

                return $item;
            });
        });
    }
}
