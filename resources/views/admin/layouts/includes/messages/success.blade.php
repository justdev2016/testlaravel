<div class="callout callout-success @if(isset($static) && $static) static-success @else success @endif">
    <h4>Success!</h4>
    <p>{{ session('notify')['message'] }}</p>
</div>