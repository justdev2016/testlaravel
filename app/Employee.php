<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Admin\Contents\Presentable;

class Employee extends Model
{
    use Presentable;

    /**
     * Список отделов
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departments()
    {
        return $this->belongsToMany(Department::class, 'departments_employees')->withTimestamps();
    }
}
