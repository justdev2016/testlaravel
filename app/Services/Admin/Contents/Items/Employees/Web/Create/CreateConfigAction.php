<?php

namespace App\Services\Admin\Contents\Items\Employees\Web\Create;

use Actions;
use App\Contracts\Admin\ActionContract;
use App\Services\Admin\Contents\Core\Settings\EmptyCollectionAction;
use App\Services\Admin\Contents\Items\Employees\Web\Create\Config\{
    ActionsConfig,
    FieldsConfig,
    FormInfoConfig,
    FormViewConfig,
    RepositoryConfig,
    SettingsConfig
};

/**
 * Выполнение инициализации настроек для `Web`.`Employees`.`Create`
 * @todo DRY - support methods
 */
class CreateConfigAction implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $this->settings = new ActionsConfig(new EmptyCollectionAction);
        $this->settings = new FieldsConfig($this->settings);
        $this->settings = new RepositoryConfig($this->settings);
        $this->settings = new SettingsConfig($this->settings);
        $this->settings = new FormInfoConfig($this->settings);
        $this->settings = new FormViewConfig($this->settings);

        $this->settings = $this->settings->action();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action()
    {
        $response = $this->object->action();

        $components = $response->getData('config')->map(function($item, $type) {
            $item = collect([
                'driver'     => $item['driver'],
                'config'     => $item['config'] ?? [],
                'actions'    => $this->settings->getData("actions.{$type}"),
                'fields'     => $this->settings->getData("fields.{$type}"),
                'repository' => $this->settings->getData('repository'),
                'settings'   => $this->settings->getData("settings.{$type}"),
            ]);

            return $item;
        });

        $response->get('settings')->put('page', 'create');
        $response->get('settings')->put('action', 'create');

        $response->put('components', $components);

        return $response;
    }
}