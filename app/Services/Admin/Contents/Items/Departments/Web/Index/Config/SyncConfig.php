<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Index\Config;

use Actions;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `sync`
 */
class SyncConfig implements ActionContract
{
    /** @var array */
    const SYNC = [
        'grid' => 'admin.api.departments.index'
    ];

    /** @var int */
    const LIMIT = 20;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $sync;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;

        $this->sync = collect();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        foreach (self::SYNC as $type => $data) {
            $this->sync->put($type, route($data, ['limit' => self::LIMIT]));
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        $response = $this->object->action();

        return $response->put('sync', $this->sync);
    }

}