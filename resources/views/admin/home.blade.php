@extends('admin.layouts.app')

@section('htmlheader_title')
	@lang('message.home')
@endsection

@section('main-content')
	<div class="box-header with-border">
		<h3 class="box-title">@lang('message.home')</h3>
	</div><!-- /.box-header -->
	<div class="box-body">
        @if ($page->isEmpty())
            @include('admin.layouts.includes.messages.info', ['info' => [trans('message.no-data')], 'static' => true])
        @endif
	</div><!-- /.box-body -->
@endsection

{{-- @push('link')
@endpush --}}

{{-- @push('scripts')
@endpush --}}