<?php

namespace App\Services\Admin\Contents\Items\Departments\Web\Edit\Config;

use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Настройки для `settings`
 */
class FormInfoConfig implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    private $info;

    public function __construct(ActionContract $object)
    {
        $this->object = $object->action();

        $this->make();
    }

    /**
     * @return void
     */
    private function make()
    {
        $settings = $this->object->getData('settings')->map(function($item, $type) {
            $item['info'] = collect($this->makeInfo($type));

            return $item;
        });

        $this->object->put('settings', $settings);
    }

    /**
     * @param  string $type
     * @return array|void
     */
    private function makeInfo(string $type)
    {
        if ($this->isRequired($type)) {
            $info['warning'] = [
                'warning' => [
                    '<strong class="required">*</strong> - ' . trans('message.required-message')
                ],
                'static' => true
            ];

            return $info;
        }
    }

    /**
     * @param string $type
     * @return bool
     */
    private function isRequired(string $type) : bool
    {
        return $this->object->getData("settings.{$type}.fields")->r_search('required');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function action() : Collection
    {
        return $this->object;
    }
}