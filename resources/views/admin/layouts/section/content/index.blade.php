{{-- @include('admin.layouts.includes.contentheader') --}}
<!-- Main content -->
<section class="content">
    <div class="box main-box">
        @include($content->view('messages'))

        <!-- Your Page Content Here -->
        @yield('main-content')
    </div><!-- ./box -->
</section><!-- /.content -->