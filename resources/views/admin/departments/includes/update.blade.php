@include('admin.layouts.includes.form.open', ['params' => $form['header-course']])

  {{-- Title --}}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        {!! Form::label('title', trans('courses.title')) !!}
        {!! Form::text('title', (isset($data['courseData'])) ? $data['courseData']['title'] : old('title'), ['id' => 'title', 'class' => 'form-control']) !!}
      </div><!-- /.form-group -->
    </div>
  </div><!-- /.row -->

  {{-- Slug --}}
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label for="slug">@lang('courses.slug')<strong class="required">*</strong></label>
        {!! Form::text('slug', (isset($data['courseData'])) ? $data['courseData']['slug'] : old('slug'), ['id' => 'slug', 'class' => 'form-control']) !!}
      </div><!-- /.form-group -->
    </div>
  </div><!-- /.row -->

  {{-- Countries --}}
  <div class="row">
    <div class="form-group">
      <div class="col-md-7 country-block">
        <h2>
          @lang('countries.countries') 
          <button type="button" class="btn btn-success" id="create-country">
            <i class="fa fa-plus" aria-hidden="true"></i>
          </button>
        </h2>
        @foreach(collect($data['courseCountries'])->pluck('slug')->all() as $country)
          @if(!in_array($country, $data['allowCountries']))
            @continue
          @endif
          <div class="input-group col-md-7">
            <h3>
              @lang('countries.country')<strong class="required">*</strong>:
              <button type="button" class="btn btn-danger delete-country">
                <i class="fa fa-minus" aria-hidden="true"></i>
              </button>
            </h3>
              {!!
                Form::select(
                  "countries[]",
                  $data['countries'],
                  $country,
                  [
                    'placeholder' => trans('countries.select-message'),
                    'class'       => "form-control course-country"
                  ])
              !!}
          </div><!-- /.input group -->
        @endforeach
      </div><!-- /.col-md -->
    </div><!-- /.form-group -->
  </div><!-- /.row -->

  {{-- Buttons --}}
  <div class="form-group buttons">
    <p>
    {!! Form::submit(trans('message.save'), ['class' => 'btn btn-success', 'id' => 'course-update-button']) !!}
    {!! Form::button(trans('message.cancel'), ['class' => 'btn btn-danger', 'id' => 'course-cancel']) !!}
    </p>
  </div>
{!! Form::close() !!}