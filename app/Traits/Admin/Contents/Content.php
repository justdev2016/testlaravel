<?php

namespace App\Traits\Admin\Contents;

use Repository;
use Illuminate\Support\Collection;

/**
 * Трейт выполнение действий по настройкам
 */
trait Content
{
    /**
     * @param  \Illuminate\Support\Collection $action
     * @param  array $data
     * @param  string|null $repoName
     * @return mixed
     * @todo проверить на возможные ошибки + подуматьнад целесообразностью использования `extract|$$`
     */
    private function content(Collection $action, array $data = null, string $repoName = null)
    {
        if (! empty($data)) {
            extract($data);
        }
        $repository = $repoName
            ? $this->settings->content("settings.repository.dependencies.{$repoName}")
            : $this->settings->content('settings.repository.main');
        $method     = $action->get('method');
        $params     = $action->get('params');

        return $repository->{$method}($$params ?? $params);
    }
}