<?php

namespace App\Services\Admin\Contents\Items\Departments\Api\Index;

use Notify;
use App\Contracts\Admin\ActionContract;
use App\Traits\Admin\Contents\{Getter,Content};
use App\Services\Admin\Contents\Items\Departments\Web\Index\IndexPresenter;

/**
 * Выполнение действия для `Ajax`.`UpdateReOrder`
 */
class IndexAction implements ActionContract
{
    use Getter, Content;

    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для ответа
     *
     * @return \Notify
     */
    public function action()
    {
        $this->settings = $this->object->action();

        $request    = $this->settings->get('requests')->request();
        $mainGetter = $this->getter('main','index');

        $request = [
            'request' => collect($request->all()),
            'present' => IndexPresenter::class,
        ];

        // получение данных модели
        $employee = $this->content($mainGetter, ['request' => collect($request)]);

        $notify = Notify::success(['data' => $employee]);

        return $notify;
    }
}