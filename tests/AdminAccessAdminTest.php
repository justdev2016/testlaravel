<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminAccessAdminTest extends TestCase
{
    /**
     * Имитация авторизованного пользователя
     */
    public function login()
    {
        $user = \App\User::find(1);
        $this->be($user);
    }

    public function testDashboardAfterLoginSuccess()
    {
        $this->login();

        $this->visit(route('admin.pages.index'))
            ->see(trans('message.no-data'));
    }

    public function testDashboardWithoutLogin()
    {
        $this->call('GET', route('admin.pages.index'));
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('login');
    }

    public function testDashboardAfterLoginForbinded()
    {
        $this->login();

        $this->call('GET', route('admin.pages.index'));
        $this->assertResponseOk();
    }
}
