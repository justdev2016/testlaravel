<?php

namespace App\Services\Admin\Support;

/**
 * Component - компонент
 * - Объявляет интерфейс для компонуемых объектов
 * - Предоставляет подходящую реализацию операций по умолчанию, общую для всех классов
 * - Объявляет интерфейс для доступа к потомкам и управлению ими
 * - Определяет интерфейс доступа к родителю компонента в рекурсивной структуре
 *   и при необходимости реализует его. Описанная возможность необязательна
 */
abstract class Component
{
    /** @var array */
    protected $item;

    /**
     * @param mixed $item
     */
    public function __construct($item)
    {
        $this->item = $item;
    }

    public abstract function add(Component $component);

    public abstract function remove(Component $component);

    public abstract function render();
}