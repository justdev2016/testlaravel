<?php

namespace App\Services\Admin\Contents\Core\Settings;

use SPA;
use Actions;
use DataLoader;
use Illuminate\Support\Collection;
use App\Contracts\Admin\ActionContract;

/**
 * Выполнение действия для `Web`
 */
class WebAction implements ActionContract
{
    /** @var \App\Contracts\Admin\ActionContract */
    protected $object;

    public function __construct(ActionContract $object)
    {
        $this->object = $object;
    }

    /**
     * Формирование данных для страницы
     *
     * @return array
     */
    public function action() : array
    {
        $response = $this->object->action();

        foreach ($response->getData('config')->keys() as $component) {
            DataLoader::make($response->getData('config')->get($component));

            $loadClass = $response->get('namespace') . '\\' . DataLoader::getMethod('data');
            $syncClass = $response->get('namespace') . '\\' . DataLoader::getMethod('sync');

            if (SPA::has() || ! DataLoader::needLoadData()) {
                $data[] = (new $syncClass(
                    $response->get('requests'),
                    $response->get('components'),
                    $response->get('settings')
                ))->action($component);
            } else {
                $data[] = (new $loadClass(
                    $response->get('requests'),
                    $response->get('components'),
                    $response->get('settings')
                ))->action($component);
            }
        }

        $result = [
            'data'     => $data ?? [],
            'settings' => $response->get('settings')
        ];

        return $result;
    }
}