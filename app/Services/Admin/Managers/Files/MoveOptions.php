<?php

namespace App\Services\Admin\Managers\Files;

use Illuminate\Support\Collection;
use App\Contracts\Managers\Files\Options;
use App\Validators\AdminFCommanderValidator;
use App\Services\Admin\Managers\Files\MoveInfo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Contracts\Managers\Files\Options as MoveOptionsContract;

/**
 * Move actions options
 */
class MoveOptions extends BaseOptions implements MoveOptionsContract
{
    /** @var array */
    protected $src;

    /** @var string */
    protected $dst;

    /** @var array|bool */
    protected $random;

    /** @var \Symfony\Component\HttpFoundation\File\File */
    protected $complete;

    /**
     * Допустимый формат данных
     * Допустимые значения и их типы:
     * [
     *     src    => array|object   => required,
     *     dst    => string         => required,              
     *     random => inteder        => optional              
     * ]
     * 
     * @param \Illuminate\Support\Collection $collection
     */
    public function __construct(Collection $collection = null)
    {
        parent::__construct();

        $this->complete = collect();
        
        if (! empty($collection)) {
            $this->make($collection);
        }
    }

    /**
     * Инициализация входных данных
     * 
     * @param  array $collection
     * @return $this
     */
    public function make(Collection $collection)
    {
        $this->validate($collection->toArray(), AdminFCommanderValidator::RULE_MOVE);

        $this->src    = $collection->get('src');
        $this->dst    = $collection->get('dst');
        $this->random = $collection->get('random', false);

        return $this;
    }
    
    /**
     * Перемещений файлов/папок
     * 
     * @param  bool $info
     * @return void
     */
    public function action(bool $info = true)
    {
        if ($this->isMultiple()) {
            // @todo реализовать функционал
        }
        
        if ($this->isUploaded()) {
            $this->moveUploaded();
        }

        if ($info) {
            return $this->info($this);
        }
    }

    /**
     * @return void
     */
    protected function moveUploaded()
    {
        $fileName = $this->isRandom() ? $this->generateFileName() : $this->getOriginalFileName();

        $this->complete->push($this->src->move($this->dst, $fileName));
    }

    /**
     * Получение информации о действии над файлами/папками
     * 
     * @param  \App\Contracts\Managers\Files\Options $options
     * @return App\Services\Admin\Managers\Info\Info
     */
    protected function info(Options $options)
    {
        return $this->info->make(new MoveInfo($options->complete));
    }

    /**
     * @return boolean
     */
    protected function isRandom() : bool
    {
        return $this->random;
    }

    /**
     * @return boolean
     */
    protected function isMultiple() : bool
    {
        return is_array($this->src);
    }

    /**
     * @return boolean
     */
    protected function isUploaded() : bool
    {
        return $this->src instanceof UploadedFile;
    }

    /**
     * @return string
     */
    protected function getOriginalFileName() : string
    {
        if ($this->isUploaded()) {
             $name = $this->src->getClientOriginalName();
        }
        
        return $name;
    }

    /**
     * @return string
     */
    protected function generateFileName() : string
    {
        return str_random($this->random) . ".{$this->getOriginalExtension()}";
    }

    /**
     * @return string
     */
    protected function getOriginalExtension() : string
    {
        return $this->src->getClientOriginalExtension();
    }
}