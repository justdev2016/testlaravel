<?php

namespace App\Services\Admin\Contents\Core\Components\Grid\DataTable;

use Illuminate\Support\Collection;
use App\Services\Admin\Contents\Core\Components\Grid\GridStrategy;
use App\Services\Admin\Contents\Core\Components\ComponentDependence;

/**
 * Создание пользовательского интерфейса для построения Grid
 */
class DataTable implements GridStrategy
{
    /** @var string */
    const TYPE = 'grid';

    /** @var \Illuminate\Support\Collection */
    protected $settings;

    /** @var \Illuminate\Support\Collection */
    protected $data;

    /**
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $data
     * @param  \App\Services\Admin\Contents\Core\Components\ComponentDependence $settings
     * @return self
     * @todo валидация входных параметров!
     */
    public function make(ComponentDependence $data, ComponentDependence $settings)
    {
        $this->data     = new DataTableData($data);
        $this->settings = new DataTableSettings($settings);

        return $this;
    }

    /**
     * @param  string $param
     * @return string
     */
    public function view($name = null) : string
    {
        return $this->settings->view($name);
    }

    /**
     * Вернуть данные для формирования столбцов
     *
     * @param  string|null $name
     * @return \Illuminate\Support\Collection
     */
    public function columns(string $name = null) : Collection
    {
        return $this->settings->columns($name);
    }

    /**
     * Вернуть данные для формирования столбцов
     *
     * @param  string|null $name
     * @return mixed
     */
    public function actions(string $name = null)
    {
        return $this->settings->actions($name);
    }

    /**
     * Вернуть данные для заполнения таблицы данными
     *
     * @return mixed
     */
    public function data(string $name = null)
    {
        return $this->data->data($name);
    }

    /**
     * Вернуть настройки компоненты
     *
     * @param  string $param
     * @return mixed
     */
    public function settings(string $param = null)
    {
        return $this->settings->settings($param);
    }
}